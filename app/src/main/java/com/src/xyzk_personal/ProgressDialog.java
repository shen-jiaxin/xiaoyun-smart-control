package com.src.xyzk_personal;

import com.src.tsdl_personal.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProgressDialog extends Dialog{
	private static ProgressDialog progressDialog;
	private static TextView tvMessage;

	private ProgressDialog(Context context, int themeResId) {
		super(context, themeResId);
	}

	public static ProgressDialog createDialog(Context context,String message){
		progressDialog = new ProgressDialog(context,R.style.ProgressDialog);
		progressDialog.setContentView(R.layout.dialog_progress);
		tvMessage=(TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
		if (message == null || message.length() == 0) {
			tvMessage.setVisibility(View.GONE);
		}else{
			tvMessage.setText(message);
		}
		progressDialog.setTitle("");
		progressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		progressDialog.show();
		return progressDialog;
	}

	/**
	 * 开启动画
	 */
	public void onWindowFocusChanged(boolean hasFocus){
		if (progressDialog == null) return;
		ImageView imageView = (ImageView) progressDialog.findViewById(R.id.loadingImageView);
		AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
		animationDrawable.start();
	}

	/**
	 * 显示信息
	 */
	public void setMessage(String message){
		if (message!= null&&message.length()>0){
			tvMessage.setVisibility(View.VISIBLE);
			tvMessage.setText(message);
		}
	}

}

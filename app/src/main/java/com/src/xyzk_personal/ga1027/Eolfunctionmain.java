package com.src.xyzk_personal.ga1027;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;

import com.src.xyzk_personal.config.Common;

import android.util.Log;

public class Eolfunctionmain {
	private final static String TAG = "Eolfunctionmain";

	//比较判断是否存在该配置代码
	public static boolean CompareCartype(String cartye)
	{
		String path = Common.Dir + "cartype.ini";
		Ini initype;
		try{
			initype = new Ini(new File(path));
			Profile.Section sec = initype.get("MODE");
			int num = Integer.valueOf(sec.get("NUM")); //配置数量
			for(int i = 0; i < num; i ++)
			{
				String name = sec.get("T" + (i + 1));
				if(cartye.equals(name))  //找到有一致的返回true
				{
					return true;
				}
			}
		}catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}
	//根据stepname获取执行相关的信息
	public static ArrayList<String> GetStepInformation(Ini Pini)
	{
		if(Pini == null) return null;
		ArrayList<String> liststep = new ArrayList<String>();
		//获取STEP信息
		Profile.Section sec = Pini.get("STEP");
		//先判断是否是AUTO状态
		int auto = Integer.valueOf(sec.get("AUTO"));
		if(auto == 1) //自动执行,则不能点击
		{
			Common.cartype.putMap("AUTO", 1);
		}
		else
		{
			Common.cartype.putMap("AUTO", 0);
		}
		int step_num = Integer.valueOf(sec.get("NUM"));
		if(Common.Debug) Log.i(TAG,"Step num = " + step_num);
		for(int i = 0; i < step_num; i ++)
		{
			//判断OPT可选,过滤SHW项目,直接显示REQ项目
			String step_name = sec.get("T" + i);
			if(step_name == "") continue;
			if(step_name.length() < 3) continue;
			String v_mode = step_name.substring(0, 3);
			String v_name = step_name.substring(4,step_name.length());
			if(v_mode.equals("OPT"))
			{
				if(Common.cartype.getMap(v_name) > 0)
					liststep.add(v_name);

			}
			else if(v_mode.equals("REQ"))
			{
				liststep.add(v_name);
			}
			else if(v_mode.equals("SHW"))
			{
			}
		}
		return liststep;
	}
	public static ArrayList<String> GetStepInformationRepair()
	{
		Ini inifile;
		ArrayList<String> listfunc = new ArrayList<String>();  //函数

		//ArrayList<String> stationlist = new ArrayList<String>(); //工位
		try {
			String configfile = Common.Dir + Common.GuestName + "/config.ini";
			if(Common.Lang.equals("en"))
				configfile = Common.Dir + Common.GuestName + "/config_en.ini";
			inifile = new Ini(new File(configfile));
			Profile.Section sec = inifile.get("STATION");
			//数量
			int num = Integer.valueOf(sec.get("NUM"));
			String id = null;
			for(int i = 0; i < (num - 1); i ++)
			{
				id = "T" + i;
				String stationname = sec.get(id);
				//stationlist.add(stationname);
				//挨个遍历工位
				Ini inistep;
				inistep = new Ini(new File(Common.Dir + Common.GuestName + "/" + Common.cartype.car + "/" + "STEP_" + i + ".ini"));
				Profile.Section secstep = inistep.get("STEP");
				int step_num = Integer.valueOf(secstep.get("NUM"));
				if(Common.Debug) Log.i(TAG,"Step num = " + step_num);
				for(int k = 0; k < step_num; k ++)
				{
					//判断OPT可选,过滤SHW项目,直接显示REQ项目
					String step_name = secstep.get("T" + k);
					if(step_name == "") continue;
					if(step_name.length() < 3) continue;
					String v_mode = step_name.substring(0, 3);
					String v_name = step_name.substring(4,step_name.length());
					if(v_mode.equals("OPT"))
					{
						if(Common.cartype.getMap(v_name) > 0)
							listfunc.add(stationname + "->" + v_name);

					}
					else if(v_mode.equals("REQ"))
					{
						listfunc.add(stationname + "->" + v_name);
					}
					else if(v_mode.equals("SHW"))
					{
					}
				}
			}
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return listfunc;
	}
	public static boolean BCMSafekey(byte[] seed,byte [] key,int pin)
	{
		int i = 0;
		int v_pin = pin;
		for(i = 0; i < 4; i ++)
		{
			v_pin ^= ((v_pin << 5) + seed[i] + (v_pin >> 4));
		}
		for(i = 0; i < 4; i ++)
		{
			key[i] = (byte)((v_pin >> (i * 8)) & 0xFF);
		}
		return true;
	}
	//EMS安全认证
	public static boolean EMSSafekey(byte [] seed, byte[] key)
	{
		int v_seed = (seed[0] & 0xFF) * 0x1000000 + (seed[1] & 0xFF) * 0x10000
				+ (seed[2] & 0xFF) * 0x100 + (seed[3] & 0xFF);
		int v_key = 0;
		v_key = ((((v_seed >> 4) ^ v_seed) << 2) ^ v_seed);
		key[0] = (byte) ((v_key >> 24) & 0xFF);
		key[0] = (byte) ((v_key >> 16) & 0xFF);
		key[0] = (byte) ((v_key >> 8) & 0xFF);
		key[0] = (byte) ((v_key >> 0) & 0xFF);
		return true;
	}
	//ICM安全认证
	public static boolean ICMSafekey(byte [] seed, byte[] key)
	{
		int v_seed = (seed[0] & 0xFF) * 0x1000000 + (seed[1] & 0xFF) * 0x10000
				+ (seed[2] & 0xFF) * 0x100 + (seed[3] & 0xFF);
		int v_key = 0;
		v_key = (v_seed + 0x0FBDCADE) ^ (v_seed + 0x0BEFDFEC);
		key[0] = (byte) ((v_key >> 24) & 0xFF);
		key[0] = (byte) ((v_key >> 16) & 0xFF);
		key[0] = (byte) ((v_key >> 8) & 0xFF);
		key[0] = (byte) ((v_key >> 0) & 0xFF);
		return true;
	}
	//bosch安全算法
	public static void Bosch_CalcKey(byte[] seed,byte[] key)
	{
		int v_seed = (seed[0]&0xFF) * 0x1000000 + (seed[1]&0xFF) * 0x10000 + (seed[2]&0xFF) * 0x100 + (seed[3]&0xFF);
		//int [] Mask  = {0x19A04B50,0x19A04806,0x19A04147,0x19A04445};
		//int [] Mask  = {0x09C53711,0x09C53071,0x09C53147,0x09C53445};
		//int [] Mask  = {0x03735c71,0x31323334,0x61626364,0x19293949};
		int [] Mask  = {0x35c71037,0x73747576,0x01562758,0x16011983};
		int v_key = 0;
		int v_count = seed[1] & 0x0F;
		int v_selectmask = ((seed[3]&0xFF) >> 4) & 0x03;
		int v_dir = ((seed[3]&0xFF) >> 3) & 0x01;
		int v_pattern = seed[3] & 0x07;
		int L1,L2;
		int i = 0;
		while(true)
		{
			v_pattern = v_seed & 0x07; //变化的
			if(i < v_count)
			{
				if(v_pattern == 5)
				{
					v_key = v_seed ^ Mask[v_selectmask];
					break;
				}
				else //循环移位
				{
					if(v_dir == 1) //右移
					{
						L1 = (v_seed & 0xFFFFFFFF) / 2;
						L2 = (int)((v_seed & 0x01) << 31);
						v_seed = (L1 + L2) & 0xFFFFFFFF;
					}
					else	//左移
					{
						L1 = (v_seed * 2) & 0xFFFFFFFF;
						L2 = (v_seed >> 31) & 0x01;
						v_seed = (L1 + L2) & 0xFFFFFFFF;
					}
				}
			}
			else
			{
				v_key = v_seed ^ Mask[v_selectmask];
				break;
			}
			i ++;
		}
		//获取结果
		key[0] = (byte) ((v_key >> 24) & 0xFF);
		key[1] = (byte) ((v_key >> 16) & 0xFF);
		key[2] = (byte) ((v_key >> 8) & 0xFF);
		key[3] = (byte) ((v_key >> 0) & 0xFF);
	}
}

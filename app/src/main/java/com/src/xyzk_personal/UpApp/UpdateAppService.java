package com.src.xyzk_personal.UpApp;

import static com.src.xyzk_personal.config.WifiAdmin.TAG;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.src.tsdl_personal.R;
import com.src.xyzk_personal.config.Common;
import com.src.xyzk_personal.config.HttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class UpdateAppService extends Service {

    private ThreadUpdate m_thread = null;

    private boolean m_threadstart = false;

    private boolean m_threadstatus = false;


    private final Timer UpdateTimer = new Timer();

    private Timer StatusTimer = new Timer();

    private TimerTask UpdateTask;

    private TimerTask StatusTask;
    Handler UpdateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //启动线程
            StartDiagnose();
            super.handleMessage(msg);
        }
    };

    //获取软件版本号
    private String getVersionName(){
        String versionName = "";
        try {
            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
        return versionName;
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 首次创建服务时，系统将调用此方法来执行一次性设置程序（在调用 onStartCommand() 或 onBind() 之前）。
     * 如果服务已在运行，则不会调用此方法。该方法只被调用一次
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }


    /**
     * 每次通过startService()方法启动Service时都会被回调。
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //检测是否有新版本的定时器任务
        //初始化计时器任务
        UpdateTask = new TimerTask() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                Message message = new Message();
                message.what = 1;
                UpdateHandler.sendMessage(message);
            }
        };
        System.out.println("启动服务");
        //启动定时器
        UpdateTimer.schedule(UpdateTask, 0,60000); //一分钟


        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 服务销毁时的回调
     */
    @Override
    public void onDestroy() {
        System.out.println("onDestroy invoke");
        //定时任务销毁
        UpdateTimer.cancel();
        StatusTimer.cancel();

        super.onDestroy();
    }

    //启动线程 查询是否存在软件版本更新
    public void StartDiagnose()
    {
        m_thread = new ThreadUpdate();
        if(Common.Debug) Log.i(TAG,"     ߳ ...");
        m_threadstart = true;
        System.out.println("启动查询线程");
        m_thread.start();
    }
    //查询是否存在版本更新
    public class ThreadUpdate extends Thread {

        @Override
        public void run() {
            //拼接项目名称和序列号
            String web = "projectName=" + Common.ProjectName + "&"
                    + "terminalSerial=" + Common.BoxName;
//            System.out.println(Common.BoxName);
            String res = null;
            for (int i = 0; i < 3; i++) {
                res = HttpRequest.sendGet(Common.web_API_SelectUpdate, web); //调用查询版本接口
                System.out.println(res);
                if (res.indexOf("200") > 0) {
                    break;
                }
            }
            if (!res.isEmpty() && res.indexOf("200") > 0) {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    JSONObject result = jsonObject.getJSONObject("data");
                    //获取返回的版本以及路径以及版本信息
                    Common.appVersion = result.getString("appVersion");
//                    System.out.println("---版本---"+Common.appVersion);
                    String filePath = result.getString("filePath");
                    Common.appNote = result.getString("appNote");
                    String fileName = result.getString("fileName");
                    Common.FileName = fileName;
                    Common.resource = "resource=" + filePath;
                    System.out.println(Common.appVersion);
                    SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                    //3.生成一个保存编辑变量
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("appNote",Common.appNote);
                    editor.putString("Version",Common.appVersion);
                    editor.commit();
                } catch (JSONException e) {
                   e.printStackTrace();
                }
            }
        }
    }

//    /**
//     * 上传修改终端状态线程
//     */
//
//    public void StartStatusDiagnose()
//    {
//        ThreadStatus threadStatus = new ThreadStatus();
//        if(Common.Debug) Log.i(TAG,"     ߳ ...");
//        m_threadstatus = true;
//        threadStatus.start();
//    }

//    public class ThreadStatus extends Thread{
//
//        @Override
//        public void run(){
//            // 编写上送数据代码
//            //获取ip地址
//            String ip = getWifiApIpAddress();
//
//            //拼接json  todo
//            JSONObject json = new JSONObject();
//            try {
//                json.put("terminalSerial",Common.BoxName);  // 序列号
////                json.put("terminalSerial","9");
//                json.put("terminalIp",ip);
//                json.put("terminalVersions",getVersionName());
//                json.put("terminalStation",Common.terminalStation); //工位号
//                json.put("terminalContent","");//预留字段
//            } catch (JSONException e) {
//                throw new RuntimeException(e);
//            }
//            for (int k=0;k<3;k++) {
//                String res = HttpRequest.sendPut(Common.web_API_AppStatus, json.toString());
//                System.out.println(res);
//                if (res.indexOf("200")>0){
//                    break;
//                }
//            }
//        }
//    }

//    //获取ip
//    public String getWifiApIpAddress() {
//        try {
//            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
//                    .hasMoreElements();) {
//                NetworkInterface intf = en.nextElement();
//                if (intf.getName().contains("wlan")) {
//                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
//                            .hasMoreElements();) {
//                        InetAddress inetAddress = enumIpAddr.nextElement();
//                        if (!inetAddress.isLoopbackAddress()
//                                && (inetAddress.getAddress().length == 4)) {
//                            Log.d(TAG, inetAddress.getHostAddress());
//                            return inetAddress.getHostAddress();
//                        }
//                    }
//                }
//            }
//        } catch (SocketException ex) {
//            Log.e(TAG, ex.toString());
//        }
//        return null;
//    }
}

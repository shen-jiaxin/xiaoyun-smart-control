package com.src.xyzk_personal;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.src.tsdl_personal.R;
import com.src.xyzk.bluetooth.service.BluetoothAdapterService;
import com.src.xyzk_personal.EOL.DbAdapter;
import com.src.xyzk_personal.EOL.DbAdapter.TableData;
import com.src.xyzk_personal.EOL.DbAdapter.TableMain;
import com.src.xyzk_personal.EOL.DesUtil;
import com.src.xyzk_personal.EOL.EolFunction;
import com.src.xyzk_personal.Flash.SelectPartNumberDialog;
import com.src.xyzk_personal.config.Common;
import com.src.xyzk_personal.config.Commonfunc;
import com.src.xyzk_personal.config.EcuModule;
import com.src.xyzk_personal.config.HttpRequest;
import com.src.xyzk_personal.config.Message;
import com.src.xyzk_personal.config.ServerSocket;
import com.src.xyzk_personal.ga1027.Eolfunctionmain;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressLint("SimpleDateFormat")
public class EolNewFunctionActivity extends Activity {
	private static String TAG = "EolNewFunctionActivity";
	Context context = EolNewFunctionActivity.this;
	//锁定屏幕
	private PowerManager powerManager = null;
	private WakeLock wakeLock = null;
	//函数功能标题
	private TextView m_func_title = null;
	//车型
	private TextView m_func_cartype = null;
	//功能选择
	//ListView列表
	private ListView m_listshow = null;
	private ArrayList<Map<String, String>> m_listshowArray = null;
	private ListShowAdapter m_listshow_adapter = null;
	private ArrayList<Map<String, String>> m_listshowhide = null; //隐藏的列表
	//car list
	private ListView m_listmodule = null;
	private ArrayList<String> m_listmodule_data = null;
	private ListColorModuleAdapter m_listmodule_adapter = null;
	private List<EcuModule> modules = new ArrayList<EcuModule>();
	EcuModule module = null;

	//提示文本
	private TextView m_showtip = null;
	private MsgWaitDialog waitdialog = null;

	private ThreadDiagnose m_thread = null; //执行线程
	private boolean m_threadstart = false;
	boolean m_upfile_isok = false;

	SharedPreferences m_pref = null;
	//boolean learnKey;
	//上传MES数据
//	private ArrayList<String> m_meslist = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.eolnewfunction_activity);
		//锁定屏幕初始化
		powerManager = (PowerManager) this.getSystemService(Service.POWER_SERVICE);
		wakeLock = this.powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Lock");
		//是否需计算锁的数量
		wakeLock.setReferenceCounted(false);
		//获取Bundle消息
		Bundle bund = getIntent().getExtras();
		m_func_title = (TextView)findViewById(R.id.func_show_func);
		m_func_title.setText(bund.getString("TITLE"));
		//
		m_func_cartype = (TextView)findViewById(R.id.func_show_cartype);
		m_func_cartype.setText(Common.cartype.car + "-" + Common.cartype.name);
		//列表初始化
		m_listshow = (ListView)findViewById(R.id.func_show_list);
		m_listshowArray = new ArrayList<Map<String,String>>();
		m_listshowhide = new ArrayList<Map<String,String>>();
		m_listshow_adapter = new ListShowAdapter(m_listshowArray);
		m_listshow.setAdapter(m_listshow_adapter);
		//初始化CarList
		m_listmodule = (ListView)findViewById(R.id.func_show_module);
		m_listmodule_data = new ArrayList<String>();
		m_listmodule_adapter = new ListColorModuleAdapter(m_listmodule_data);
		m_listmodule.setAdapter(m_listmodule_adapter);
		m_listmodule.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				m_listmodule_adapter.setSelected(position);
				m_listmodule_adapter.notifyDataSetChanged();
			}
		});
		//提示信息框
		m_showtip = (TextView)findViewById(R.id.func_show_text);


		Common.cartype.Result=0;
		m_pref = getSharedPreferences("config", MODE_PRIVATE);
		//开始测试
		StartDiagnose(bund.getString("DIR"),bund.getString("FUNC_NAME"));
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//请求常亮
		wakeLock.acquire();

	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//取消屏幕常亮
		wakeLock.release();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		switch(keyCode)
		{
			case KeyEvent.KEYCODE_BACK:	//返回键
				if(m_threadstart == true)
				{
					//最后提示
					final MsgWaitDialog waitdialog = new MsgWaitDialog(EolNewFunctionActivity.this);
					waitdialog.setText(getString(R.string.msg_show_context16) + "?");
					waitdialog.setWaitTime(0);
					waitdialog.ShowCancel();
					waitdialog.show();
					waitdialog.setOnOKButton(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							waitdialog.dismiss();
							//执行功能
							//Toast.makeText(context, R.string.main_tip_text6, Toast.LENGTH_SHORT).show();\
							StopDiagnose();
							finish();
						}
					});
					return true;
				}
				else
				{
					finish();
				}
			default:
				break;
		}
		return super.onKeyDown(keyCode, event);
	}
	//显示提示信息
	private void ShowFuncTip(String context)
	{
		m_handler.obtainMessage(Message.MSG_Func_ShowTip, context).sendToTarget();
	}
	//显示检测结果
	private void ShowFuncTip(int Presult,String context)
	{
		m_handler.obtainMessage(Message.MSG_Func_ShowTip,Presult,0, context).sendToTarget();
	}
	private void ShowFuncTip(int Rid)
	{
		ShowFuncTip(getString(Rid));
	}

	private final Handler m_handler = new Handler()
	{
		public void handleMessage(android.os.Message msg)
		{
			if(m_threadstart == false) return;
			switch (msg.what) {
				case Message.MSG_Dialog_WaitTime:
					if(waitdialog != null)
					{
						waitdialog.dismiss();
						waitdialog = null;
					}
					waitdialog = new MsgWaitDialog(EolNewFunctionActivity.this);
					waitdialog.setWaitTime(msg.arg1);
					if(msg.arg2 == 1) //ON
						waitdialog.setText(R.string.msg_show_keyon);
					else if(msg.arg2 == 2) //OFF
						waitdialog.setText(R.string.msg_show_keyoff);
					else if(msg.arg2 == 3) //直接显示文本
						waitdialog.setText((String)msg.obj);
					else if(msg.arg2 == 4) //OFF
						waitdialog.setText(R.string.msg_show_context6);
					else if(msg.arg2 == 5) //OFF
						waitdialog.setText(R.string.msg_show_context7);
					else if(msg.arg2 == 6) //WAIT
						waitdialog.setText(R.string.msg_show_context10);
					else if(msg.arg2 == 7) //START
						waitdialog.setText(R.string.msg_show_keystart);
					else
						waitdialog.setText((String) msg.obj);
					waitdialog.show();
					//Common.wait = false;
					break;
				case Message.MSG_Dialog_WaitIimeAutoClose:
					if(Common.Debug) Log.i(TAG,"Show AutoClose");
					if(msg.arg1 >= 0)
					{
						if(waitdialog != null)
						{
							waitdialog.dismiss();
							waitdialog = null;
						}
						waitdialog = new MsgWaitDialog(EolNewFunctionActivity.this);
						waitdialog.setWaitTime(msg.arg1);
					}
					if(msg.arg2 == 1) //ON
						waitdialog.setText(R.string.msg_show_context1);
					else if(msg.arg2 == 2)
						waitdialog.setText(R.string.msg_show_context2);
					else if(msg.arg2 == 3)
						waitdialog.setText(R.string.msg_show_context3);
					else if(msg.arg2 == 4)
						waitdialog.setText(R.string.msg_show_context4);
					else if(msg.arg2 == 5)
						waitdialog.setText(R.string.msg_show_context5);
					else if(msg.arg2 == 6)
						waitdialog.setText(R.string.msg_show_context9);
					else if(msg.arg2 == 7)
						waitdialog.setText(R.string.msg_show_context11);
					else if(msg.arg2 == 8)
						waitdialog.setText(R.string.msg_show_context12);
					else if(msg.arg2 == 9)
						waitdialog.setText(R.string.msg_show_context13);
					else
						waitdialog.setText((String) msg.obj);
					if(msg.arg1 >= 0)
					{
						waitdialog.show();
						waitdialog.SetAutoClose();
					}
					break;
				case Message.MSG_Dialog_Show_Text: //显示提示信息
					if(waitdialog != null)
					{
						waitdialog.setText((String) msg.obj);
					}
					break;
				case Message.MSG_Dialog_close:
					if(waitdialog != null)
					{
						if(waitdialog.IsShowing())
							waitdialog.dismiss();
						Common.wait = false;
						waitdialog = null;
					}
					break;
				case Message.MSG_Start_Diagnose:
					if(m_thread != null)
					{
						//结束线程
						if(Common.Debug) Log.i(TAG,"Close Thread");
						m_thread.close();
						try {
							Thread.sleep(300); //延时
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						m_thread = null;
					}
					if(Common.Debug) Log.i(TAG,"Start Thread");
					break;
				case Message.MSG_Initial_Module:
					//清除列表
					m_listshowArray.clear();
					m_listshowhide.clear();
					m_listshow_adapter.notifyDataSetChanged();
					m_threadstart = true;
					break;
				case Message.MSG_ShowArray:	//显示
					m_listshowArray.add((HashMap<String, String>)msg.obj);
					m_listshow_adapter.notifyDataSetChanged();
					break;
				case Message.MSG_ShowArray_module:
					m_listmodule_adapter.notifyDataSetChanged();
					break;
				case Message.MSG_ShowArrayHide: //隐藏的列表
					m_listshowhide.add((HashMap<String, String>)msg.obj);
					break;
				case Message.MSG_Func_ShowTip:	//显示提示信息
					m_showtip.setText((String) msg.obj);
					if(msg.arg1 == 2) //失败
						m_showtip.setBackgroundColor(0xFFFF0000);
					else if(msg.arg1 == 1) //成功
						m_showtip.setBackgroundColor(0xFF00FF00);
					m_showtip.invalidate();
					break;
				case Message.MSG_Dialog_SHow_YESNO:  //显示提示信息对话框
					final MsgWaitDialog waitdialog = new MsgWaitDialog(EolNewFunctionActivity.this);
					waitdialog.setText((String)msg.obj);
					waitdialog.setWaitTime(0);
					waitdialog.ShowYesOrNo();
					waitdialog.ShowCancel();
					waitdialog.setOnCancelButton(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Common.dialog_result = false;	//点击了取消
							waitdialog.dismiss();
							Common.wait = false;
						}
					});
					waitdialog.setOnOKButton(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Common.dialog_result = true;	//点击了取消
							waitdialog.dismiss();
							Common.wait = false;
						}
					});
					waitdialog.show();
					break;
				case Message.MSG_Select_Partnumber: //选择零件号
					final SelectPartNumberDialog dialogconfig = new SelectPartNumberDialog(EolNewFunctionActivity.this);
					for(int i = 0; i < Common.SelectPartNumberList.size(); i ++){
						dialogconfig.AddSelectString(Common.SelectPartNumberList.get(i));
					}
					dialogconfig.SetDefaultSelect(0);
					dialogconfig.setOnOKButton(new OnClickListener() {
					   @Override
					   public void onClick(View v) {
						   Common.PartNumber = dialogconfig.GetSelectString();
						   dialogconfig.dismiss();
					   }
					});
						dialogconfig.show();
					break;
				default:
					break;
			}
		};
	};

	public class ListShowAdapter extends BaseAdapter
	{
		private ArrayList<Map<String, String>> listdata = null;
		private FuncListShow listshow = null;
		private View view = null;
		private LayoutInflater inflater = null;
		private int Selected = -1;
		public ListShowAdapter(ArrayList<Map<String, String>> Pdata) {
			// TODO Auto-generated constructor stub
			listdata = Pdata;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if(listdata != null)
				return listdata.size();
			else
				return 0;
		}
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			if(listdata != null && listdata.size() > position)
				return listdata.get(position);
			else
				return null;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		public void setSelected(int pos)
		{
			Selected = pos;
		}
		public int getSelected()
		{
			return Selected;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.eolnewfunction_showlistitem, null, false);
			listshow = (FuncListShow) view.getTag();
			if(listshow == null)
			{
				listshow = new FuncListShow();
				listshow.id = (TextView)view.findViewById(R.id.eolnewfunction_listshow_id);
				listshow.context = (TextView)view.findViewById(R.id.eolnewfunction_listshow_context);
				listshow.result = (TextView)view.findViewById(R.id.eolnewfunction_listshow_result);
				listshow.ctand=(TextView)view.findViewById(R.id.eolnewfunction_listshow_ctand);
				listshow.time = (TextView)view.findViewById(R.id.eolnewfunction_listshow_time);
				view.setTag(listshow);
			}
			if(listdata.get(position).get("SHOW_TIME").equals("N.OK"))
			{
				view.setBackgroundColor(Color.RED);
			}
			else if(position == Selected)
			{
				//listlayout.text.setBackgroundColor(Color.GREEN);
				view.setBackgroundColor(Color.GREEN);
				listshow.id.setSelected(true);
				listshow.id.setPressed(true);
			}
			else if(position % 2 == 1)
			{
				listshow.id.setSelected(false);
				listshow.id.setPressed(false);
				view.setBackgroundColor(0xFFC0C0C0);
			}
			else
			{
				listshow.id.setSelected(false);
				listshow.id.setPressed(false);
				view.setBackgroundColor(Color.WHITE);
			}
			listshow.id.setText(listdata.get(position).get("SHOW_ID"));
			listshow.context.setText(listdata.get(position).get("SHOW_CONTEXT"));
			listshow.result.setText(listdata.get(position).get("SHOW_RESULT"));
			listshow.ctand.setText(listdata.get(position).get("SHOW_CTAND"));
			listshow.time.setText(listdata.get(position).get("SHOW_TIME"));
			return view;
		}
	}
	class FuncListShow
	{
		TextView id;
		TextView context;
		TextView result;
		TextView ctand;
		TextView time;
	}
	public class ListColorModuleAdapter extends BaseAdapter
	{
		private ArrayList<String> listdata = null;
		private ListModule listlayout = null;
		private View view = null;
		private LayoutInflater inflater = null;
		private int Selected = -1;
		public ListColorModuleAdapter(ArrayList<String> Pdata) {
			// TODO Auto-generated constructor stub
			listdata = Pdata;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if(listdata != null)
				return listdata.size();
			else
				return 0;
		}
		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			if(listdata != null && listdata.size() > position)
				return listdata.get(position);
			else
				return null;
		}
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		public void setSelected(int pos)
		{
			Selected = pos;
		}
		public int getSelected()
		{
			return Selected;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.listcoloritemmodule, null, false);
			listlayout = (ListModule) view.getTag();
			if(listlayout == null)
			{
				listlayout = new ListModule();
				listlayout.text = (TextView)view.findViewById(R.id.eolfunction_liststep_module);
				view.setTag(listlayout);
			}
			if(position == Selected)
			{
				//listlayout.text.setBackgroundColor(Color.GREEN);
				view.setBackgroundColor(Color.CYAN);
				listlayout.text.setSelected(true);
				//listlayout.text.setPressed(true);
			}
			else
			{
				listlayout.text.setSelected(false);
				//listlayout.text.setPressed(false);
				view.setBackgroundColor(listlayout.color);
			}
			listlayout.text.setText(listdata.get(position));
			return view;
		}
	}
	class ListModule
	{
		TextView text;
		int color = Color.WHITE;
	}

	//--------------------------------车型相关诊断--------------------------------------------

	//启动线程
	private void StartDiagnose(String Pstep,String Pfunc)
	{
		//启动线程
		m_thread = new ThreadDiagnose(Pstep, Pfunc);
		if(Common.Debug) Log.i(TAG,"启动线程...");
		m_threadstart = true;
		m_thread.start();
	}
	//结束线程
	private void StopDiagnose()
	{
		if(m_thread != null)
		{
			//结束线程
			if(Common.Debug) Log.i(TAG,"Close Thread");
			m_thread.close();
			try {
				Thread.sleep(500); //延时
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			m_thread = null;
			m_threadstart = false;
		}
	}

	//-----------------------执行线程-------------------------------
	public class ThreadDiagnose extends Thread
	{
		String stepname = null;	//执行的函数名称
		String func = null;	//执行功能
		Ini t_inistep = null;
		Ini t_inilang = null;
		Ini t_dtc = null;
		Ini t_dtc_disable = null;
		Profile.Section t_lang = null;
		//ArrayList<String> t_print = new ArrayList<String>();
		ArrayList<String> t_downline = new ArrayList<String>();
		int showId=0;
		ArrayList<HashMap<String, String>> updateData = new ArrayList<HashMap<String,String>>();
		boolean  t_isok = true; //初始
		//ArrayList<String> clearOrReadDtc = new ArrayList<String>();
		//boolean readVin;
		//初始化连接service//蓝牙服务
		BluetoothAdapterService t_com = BluetoothAdapterService.getInstance();
		//boolean t_havedtc = false;	//系统是否存在故障码
		public boolean exit = false;
		public ThreadDiagnose(String Pstep,String Pfunc) {
			// TODO Auto-generated constructor stub
			stepname = Pstep;
			func = Pfunc;
			//t_com = new ComSocket();
		}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if(exit == true) return;
			if(Common.Debug) Log.i(TAG,"通讯盒初始化...");
			//t_com.SetStartRecord(new String(Common.cartype.vin));
			//t_com.PrintLog(getString(R.string.main_title));
			Sleep(100);
			if(ConnectToBox() != 0) return;
			//执行功能模块
			//初始化清除列表
			m_handler.obtainMessage(Message.MSG_Initial_Module).sendToTarget();
			//初始化库文件
			t_inistep = Common.cartype.istep;
			t_inilang = Common.cartype.ilang;
			t_lang = Common.cartype.ilang.get("LANG");
			if(t_inistep == null) return;
			if(t_inilang == null) return;
			SimpleDateFormat  sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //日期格式
			Common.cartype.start_time = sDateFormat.format(new java.util.Date());  //当前时间
			Common.cartype.end_time = sDateFormat.format(new java.util.Date());  //当前时间
			//load module
			if(Common.cartype.OptMode == 0)
			{
				ArrayList<String> list = Eolfunctionmain.GetStepInformation(t_inistep);
				for(int x = 0; x < list.size(); x ++)
				{
					m_listmodule_data.add(list.get(x).toString());
				}
			}
			else
			{
				m_listmodule_data.add(func);
			}
			m_handler.obtainMessage(Message.MSG_ShowArray_module).sendToTarget();

			if(Common.cartype.OptMode == 0)
			{
				AutoFunction();
			}
			else
			{
				ManuFunction(func);
			}

			close();
			ShowFuncTip(R.string.func_show_saveresult);
			if(modules.size() > 0)
			{
				Common.cartype.Result = 1;
				for(EcuModule mod : modules)
				{
					if(mod.getresult() > 1)
					{
						Common.cartype.Result = mod.getresult();
						break;
					}
				}
			}
			// 上传数据至服务器
			if(SendAddResult(m_func_title.getText().toString(),m_listshowArray) == false)
				ShowFuncTip(2,getString(R.string.msg_show_context17));
			//根据上传数据将其存储进数据库中
			SaveDataToDb(m_listshowArray);
			//存储结果
			Sleep(100);
			if(Common.cartype.Result == 2) //失败
				ShowFuncTip(2,getString(R.string.msg_show_context18));
			else
				ShowFuncTip(1,getString(R.string.msg_show_context19));
			Sleep(100);
			m_threadstart = false;
			super.run();
		}
		void close()
		{
			exit = true;
			//t_com.close();
		}
		void SaveDataToDb(ArrayList<Map<String, String>> list)
		{
			String v_print = "";
			for(EcuModule mod : modules)
			{
				v_print += mod.toString();
			}
			DbAdapter db = new DbAdapter(context);
			//---------------------第一步存主数据库---------------------
			db.open();
			//查询测试次数
			int oldtestnum = db.QueryMainTestNum(new String(Common.cartype.vin));
			if(Common.Debug) Log.i(TAG,"已经存储次数为:" + oldtestnum);
			ContentValues cv = new ContentValues();
			cv.put(TableMain.car,Common.cartype.system);
			cv.put(TableMain.esk,Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, 16));
			cv.put(TableMain.vin, new String(Common.cartype.vin));
			cv.put(TableMain.pin,Commonfunc.bytesToHexStringP(Common.cartype.pin, 0, 2));
			cv.put(TableMain.cartype,Common.cartype.car);
			cv.put(TableMain.carcolor,Common.cartype.name);
			cv.put(TableMain.station, m_func_title.getText().toString());
			cv.put(TableMain.carname, Common.cartype.name);
			cv.put(TableMain.timestart, Common.cartype.start_time);
			cv.put(TableMain.timeend, Common.cartype.end_time);
			cv.put(TableMain.printdata, v_print);
			cv.put(TableMain.device, Common.cartype.box_serialnum);
			cv.put(TableMain.appversion,getString(R.string.main_title));
			cv.put(TableMain.testnum, oldtestnum + 1);
			cv.put(TableMain.logfileName,t_com.GetLogfileName());
			cv.put(TableMain.logpath,t_com.getFilePaths());
			if(Common.cartype.Result <= 1)
				cv.put(TableMain.result, "OK");
			else
				cv.put(TableMain.result, "NG");
			if(Common.Debug) Log.i(TAG,cv.toString());
			db.addmainTable(cv,Common.updata_isok);
			//---------------------第二步，存储详细数据---------------------
			int datanum = db.CreateTableData(new String(Common.cartype.vin));
			db.StartAddMost();
			if(Common.Debug) Log.i(TAG,"查询到的测试次数:" + datanum);
			if(list != null)
			{
				for(int i = 0; i < list.size(); i ++)
				{
					ContentValues cdata = new ContentValues();
					cdata.put(TableData.testnum, oldtestnum + 1);
					cdata.put(TableData.testcode, list.get(i).get("SHOW_ID"));
					cdata.put(TableData.context, list.get(i).get("SHOW_CONTEXT"));
					cdata.put(TableData.testdata, list.get(i).get("SHOW_RESULT"));
					cdata.put(TableData.testctand,list.get(i).get("SHOW_CTAND"));
					cdata.put(TableData.result, list.get(i).get("SHOW_TIME"));
					//if(Common.Debug) Log.i(TAG,cv.toString());
					db.addDatatotable(new String(Common.cartype.vin), cdata,false);
				}
			}
			db.StopAddMost();
			db.close();
			//m_handler.obtainMessage(MSG_Connect_MainSQL, cv).sendToTarget();
		}
		//自动执行
		private void AutoFunction()
		{
			//获取step数据
			Profile.Section sec = t_inistep.get("STEP");
			int num = Integer.valueOf(sec.get("NUM"));
			for(int i = 0; i < num; i ++)
			{
				if(exit) return;
				Sleep(Common.waittime);
				//判断OPT可选,过滤SHW项目,直接显示REQ项目
				String step_name = sec.get("T" + i);
				String v_mode = step_name.substring(0, 3);
				String v_name = step_name.substring(4,step_name.length());
				if(Common.Debug) Log.i(TAG,"v_mode = " + v_mode + "; v_name = " + v_name + "; i = " + i);
				if(v_mode.equals("OPT"))	//处理可选项目
				{
					int mode_num = Common.cartype.getMap(v_name);
					if(mode_num > 0)
					{
						//执行单个模块
						module = new EcuModule(v_name);
						ExecutionModule(v_name);
						String str = module.toString();
						modules.add(module);
					}
				}
				else if(v_mode.equals("REQ"))	//处理比选项目
				{
					//执行单个模块
					int mode_num = Common.cartype.getMap(v_name);
					module = new EcuModule(v_name);
					if(mode_num > 0)
						ExecutionModule(v_name);
					else
						ExecutionModule(v_name);
					modules.add(module);
				}
				else if(v_mode.equals("SHW")) //处理弹出显示窗口
				{
					int v_add1 = step_name.indexOf("<");
					int v_add2 = step_name.indexOf(">");
					if(Common.Debug) Log.i(TAG,"v_add1 = " + v_add1 + "; v_add2 = " + v_add2);
					String v_data1 = step_name.substring(4, v_add1);
					int v_time = Integer.valueOf(step_name.substring(v_add1 + 1, v_add2));
					if(v_data1.equals("ON") == true)
					{
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, v_time, 1).sendToTarget();
						while(Common.wait) {Sleep(Common.waittime);};
					}
					else if(v_data1.equals("OFF") == true)
					{
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, v_time, 2).sendToTarget();
						while(Common.wait){Sleep(Common.waittime);};
					}
					else if(v_data1.equals("START") == true)
					{
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, v_time, 7).sendToTarget();
						while(Common.wait){Sleep(Common.waittime);};
					}
					else if(v_data1.equals("WAIT") == true)
					{
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, v_time, 6).sendToTarget();
						while(Common.wait){Sleep(Common.waittime);};
					}
				}
			}
		}
		//手动执行
		private void ManuFunction(String name)
		{
			//获取step数据
			//----------------------------------------------
			//根据用户选择执行
			//清除列表
			m_handler.obtainMessage(Message.MSG_Initial_Module).sendToTarget();
			//特殊处理名称问题
			if(Common.Debug) Log.i(TAG,"v_name = " + name);
			//执行单个模块
			int mode_num = Common.cartype.getMap(name);
			if(mode_num > 0)
				ExecutionModule(name);
			else
				ExecutionModule(name);
		}
		//执行单个模块功能
		private void ExecutionModule(String modename)
		{
			//显示模块名称
			if(Common.cartype.station != 1)
			{
				Profile.Section sec = null;
				sec = t_inistep.get(modename); //获取模块内容
				if(sec == null) return;
				//显示检测内容
				String v_showtip = "";
				if(Common.cartype.getMap("AUTO") > 0)
					v_showtip = getString(R.string.main_tip_text9) + m_func_title.getText().toString() + "->" + modename + "...";
				else
					v_showtip = getString(R.string.main_tip_text9) + m_func_title.getText().toString() + "...";
				ShowFuncTip(v_showtip);
				t_com.PrintLog(v_showtip);
				//第一步 显示模块标题
				//打印增加
				//t_print.add(sec.get("MODULE_NAME"));
				t_isok = true; //复位
				//显示
				int module_id = Integer.valueOf(sec.get("MODULE_ID"));
				ShowProcessList(module_id,modename,"ECU","MES",Common.Rnull);
				if(Common.cartype.station <= 6) //
				{

					if (Common.cartype.car.equals("EP12")) {
						if(Common.cartype.station == 0)  //EOL
						{
							if(modename.equals("BDCM"))
							{
								fun_EP12_F0_BDCM1(sec);
							} else if(modename.equals("ClearDTC") || modename.equals("ReadDTC")||modename.equals("ReadConfig"))
							{
								fun_EP12_F0_DTC1(sec);
							}
							//else if(modename.equals("EHB")||modename.equals("EPBi"))
							//{
							//	fun_EP12_F2_EHB1(sec);
							//}
							else
							{
								fun_EP12_F0_Stand1(sec);
							}
						}
						else if(Common.cartype.station == 2) //四轮
						{
							if(modename.equals("EPS") || modename.equals("ESC") || modename.equals("ESC1"))
							{
								fun_EP12_F3_Stand1(sec);
							}
						}
						else if(Common.cartype.station == 3  || Common.cartype.station == 4) //出厂
						{
							if(modename.equals("ClearDTC") || modename.equals("ReadDTC")||modename.equals("ReadConfig"))
							{
								fun_EP12_F0_DTC1(sec);
							}else if(modename.equals("OBD"))
							{
								fun_EP12_F4_OBD1(sec);
							}
							//else if(modename.equals("EHB")||modename.equals("EPBi"))
							//{
							//	fun_EP12_F2_EHB1(sec);
							//}
							else
							{
								fun_EP12_F0_Stand1(sec);
							}
						}
					}

				}

				if(t_isok == false) //执行失败
				{
					Common.cartype.Result = 2; //不合格
				}
				else
				{

				}
				//t_print.add(t_lang.get(module_id + "") + " - 合格");
			}
			else //刷写工位
			{
				Profile.Section sec = null;
				sec = t_inistep.get("Flash"); //获取模块内容
				if(sec == null) return;
				////去掉尾部的1
				//modename = modename.substring(0, modename.length());
				//显示检测内容
				String v_showtip = "";
				if(Common.cartype.getMap("AUTO") > 0)
					v_showtip = getString(R.string.main_tip_text9) + m_func_title.getText().toString() + "->" + modename + "...";
				else
					v_showtip = getString(R.string.main_tip_text9) + m_func_title.getText().toString() + "...";
				ShowFuncTip(v_showtip);
				t_com.PrintLog(v_showtip);
				//第一步 显示模块标题
				t_isok = true; //复位
				//显示
				int module_id = Integer.valueOf(sec.get("MODULE_ID"));
				ShowProcessList(module_id,modename +"","ECU","MES",Common.Rnull);
				if((modename.equals("BDCM")&&Common.cartype.OptMode==0)||Common.cartype.OptMode==1){
					Profile.Section sec_on = null;
					sec_on = t_inistep.get("SWITCH_OFF"); //获取模块内容
					if(sec_on == null) return;
					fun_EP12_F1_Switch1(sec_on);
				}
//				if(modename.equals("BMS") || modename.equals("MCU") || modename.equals("VCU1") || modename.equals("OBC")|| modename.equals("VCU"))
				if(modename.equals("BDCM"))
				{ //整车下高压
					Profile.Section sec_on = null;
					sec_on = t_inistep.get("SWITCH_OFF"); //获取模块内容
					if(sec_on == null) return;
					fun_EP12_F1_Switch1(sec_on);
				}
				fun_EP12_F1_EcuFlash(sec,modename); //刷写
				if((modename.equals("VCU1")&&Common.cartype.OptMode==0)||Common.cartype.OptMode==1){
					Profile.Section sec_on = null;
					sec_on = t_inistep.get("SWITCH_ON"); //获取模块内容
					if(sec_on == null) return;
					fun_EP12_F1_Switch1(sec_on);
				}
//				if(modename.equals("BMS") || modename.equals("MCU") || modename.equals("VCU1") || modename.equals("OBC")|| modename.equals("VCU"))
				if(modename.equals("OBCe"))
				{ //整车上高压
					Profile.Section sec_on = null;
					sec_on = t_inistep.get("SWITCH_ON"); //获取模块内容
					if(sec_on == null) return;
					fun_EP12_F1_Switch1(sec_on);
				}
				if(t_isok == false) //执行失败
				{
					Common.cartype.Result = 2; //不合格
				}
				else
				{
				}
			}

		}

		//显示一行数据
		//@SuppressLint("SimpleDateFormat")
		//isok: 0--"---";1--"OK";2--"N.OK"
		private void ShowProcessList(int id,String content,String result,String ctand,int isok)
		{
			//SimpleDateFormat  sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //日期格式
			//String date = sDateFormat.format(new java.util.Date());  //当前时间
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("SHOW_ID", id + "");
			map.put("SHOW_CONTEXT", content);
			map.put("SHOW_RESULT", result);
			map.put("SHOW_CTAND",ctand); //TODO
			if(isok == 1)
			{
				map.put("SHOW_TIME", "OK");
				if(module != null)
				{
					if(module.getresult() < 1)
						module.setresult(isok);
				}
			}
			else if(isok == 2)
			{
				map.put("SHOW_TIME", "N.OK");
				Common.cartype.Result = isok; //结果不OK
				if(module != null)
				{
					if(module.getresult() < 2)
						module.setresult(isok);
					//添加内容
					if((content.indexOf("零件号") >= 0) || (content.indexOf("供应商代码") >= 0) || (content.indexOf("硬件版本") >= 0) || (content.indexOf("软件版本") >= 0) || (content.indexOf("序列号") >= 0) || (content.indexOf("配置码") >= 0) ||
					(content.indexOf("part") >= 0) || (content.indexOf("supplier") >= 0) || (content.indexOf("hardware ver") >= 0) || (content.indexOf("software ver") >= 0) || (content.indexOf("erial num") >= 0) || (content.indexOf("configuration") >= 0))
					{
						String xtr = getString(R.string.msg_show_context64) + content + "-" + getString(R.string.msg_show_context65) +",";
						String ecu_val = result;
						String mes_val = ctand;
						if(ecu_val.length() > 0)
							xtr += "ECU:" + ecu_val + ",";
						else
							xtr += "ECU:null,";
						if(mes_val.length() > 0)
							xtr += "MES:" + mes_val + ",";
						else
							xtr += "MES:null";
						module.addcontext(xtr);
					}
					else
						module.addcontext(content + ":" + result);
				}
			}
			else
				map.put("SHOW_TIME", "---");
			m_handler.obtainMessage(Message.MSG_ShowArray, map).sendToTarget();
		}
		private void ShowProcessListHide(int id,String content,String result,int isok)
		{
			//SimpleDateFormat  sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //日期格式
			//String date = sDateFormat.format(new java.util.Date());  //当前时间
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("SHOW_ID", id + "");
			map.put("SHOW_CONTEXT", content);
			map.put("SHOW_RESULT", result);
			if(isok == 1)
				map.put("SHOW_TIME", "OK");
			else if(isok == 2)
			{
				map.put("SHOW_TIME", "N.OK");
				Common.cartype.Result = isok; //结果不OK
			}
			else
				map.put("SHOW_TIME", "-");
			m_handler.obtainMessage(Message.MSG_ShowArrayHide, map).sendToTarget();
		}
		//第一步，先连接上box,
		//返回值：  -1，直接退出,0 -- 连接成功
		private int ConnectToBox()
		{
			int i;
			//开始记录日志
			t_com.exit = false;
			//t_com.SetStartRecord(new String(Common.cartype.vin));
			//获取序列号
			if(Common.Debug) Log.i(TAG,"获取序列号...");
			byte[] serialnum = new byte[12];
			byte[] softv = new byte[2];
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.GetSerialNum(serialnum, serialnum.length,softv)) break;
				Sleep(50);
				if(exit) return -1;
			}
			if(i >= Common.cmd_freq)
			{
				ShowFuncTip(R.string.func_show_tip3);
			}
			//显示序列号
			ShowFuncTip(getString(R.string.func_show_tip5) + "V" + (softv[0]&0xFF) + "." +
					Commonfunc.byteToString(softv[1]) + "\n" +
					getString(R.string.func_show_tip4)	+ new String(serialnum));
			Common.cartype.box_serialnum = new String(serialnum);
			//判断序列号
			if((softv[0]&0xFF) == 0 && (softv[1]&0xFF) == 0) return -1;
			//设置BOX
			byte [] cmd_res = {0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
			if(SetCanInit(1, 500000, cmd_res) == false) return -1;
			Sleep(100);
			return 0;
		}

		//上传数据至服务器并保存进数据库
		private boolean SendAddResult(String Pname,List<Map<String, String>> listData){
			String v_print = "";
			//上传web服务器
			JSONObject jsonmain = new JSONObject();
			try{
				jsonmain.put("resultVin", new String(Common.cartype.vin));
				jsonmain.put("resultCar",Common.cartype.system);
				jsonmain.put("resultCartype", Common.cartype.car);
				jsonmain.put("resultStation",Pname);
				jsonmain.put("resultCarname",Common.cartype.car);
				//jsonmain.put("resultCardate",Common.cartype.date);
				jsonmain.put("resultCardate",Common.cartype.start_time);
				jsonmain.put("resultCartime",Common.cartype.start_time);
				jsonmain.put("resultDevice",Common.BoxName);
				//打印机编号做返修不打印的特殊处理
				if(Common.cartype.OptMode == 0) {
					jsonmain.put("resultPrintnum",""+ Common.Printnumber);
				}else {
					jsonmain.put("resultPrintnum","99");
				}
				jsonmain.put("resultTestnum", "1");
				if(Common.cartype.Result <= 1)
					jsonmain.put("resultResult","OK");
				else
					jsonmain.put("resultResult","NG");
				//int v_size = modules.size();
				for(EcuModule mod : modules)
				{
					v_print += mod.toString();
				}
				jsonmain.put("resultPrint",v_print);
				jsonmain.put("resultPin",Commonfunc.bytesToHexStringP(Common.cartype.pin, 0, 2));
				jsonmain.put("resultEsk",Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, 16));
				jsonmain.put("resultColor",Common.cartype.name);
				jsonmain.put("resultAppver",getString(R.string.main_title));
				jsonmain.put("resultUp2mes","0");
				jsonmain.put("resultUser","test");
				jsonmain.put("fileName",t_com.GetLogfileName());
				jsonmain.put("resultLog","");
				JSONArray array=new JSONArray();
				for(int k = 0; k < listData.size(); k ++)
				{
					JSONObject jsondetail = new JSONObject();
					jsondetail.put("mVin",new String(Common.cartype.vin));
					jsondetail.put("mTestcode",listData.get(k).get("SHOW_ID"));
					jsondetail.put("mContext",listData.get(k).get("SHOW_CONTEXT"));
					jsondetail.put("mTestdata",listData.get(k).get("SHOW_RESULT"));
					jsondetail.put("mStand",listData.get(k).get("SHOW_CTAND"));
					jsondetail.put("mResult",listData.get(k).get("SHOW_TIME"));
					array.put(k,jsondetail);
				}
				jsonmain.put("tableResultDetails", array);
			}catch (Exception e){
				e.printStackTrace();
			}
			Long v_upfileid = (long) 0;

			// 上传
			t_com.PrintLog("UpAddr:"+ Common.Web_Result_Add);
			t_com.PrintLog(jsonmain.toString());
			String res = HttpRequest.sendPostToMyMes(Common.Web_Result_Add, jsonmain.toString().replace(">",""));

			if(res.indexOf("200") > 0) //ok
			{
				try {
					JSONObject resjson = new JSONObject(res);
					JSONObject son = resjson.getJSONObject("data");
					if(son != null)
						v_upfileid = son.getLong("resultId");
					ShowFuncTip(getString(R.string.msg_show_context20));
					Common.updata_isok = true;
				}catch (Exception e){
					e.printStackTrace();
				}
			}else {
				Common.updata_isok=false;
				t_com.PrintLog(res);
			}
			//上传日志文件
			if(v_upfileid > 0)
			{
				ShowFuncTip(getString(R.string.msg_show_context21));
				if(uploadFiles(Common.Web_Result_Uploadfile,t_com.getFilePaths(),v_upfileid) == true)
					ShowFuncTip(getString(R.string.msg_show_context22));
				else
					ShowFuncTip(getString(R.string.msg_show_context23));
			}
			return true;
		}

		/**
		 * 文件上传线程
		 * @param uploadUrl
		 * @param filePaths
		 * @param id
		 */
		public boolean uploadFiles(String uploadUrl, String filePaths,Long id)
		{
			m_upfile_isok = false;
			MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
			String fname = filePaths;
			File tempfile = new File(fname);
			//根据文件的后缀名，获得文件类型
			builder.setType(MultipartBody.FORM)
					.addFormDataPart("resultId", String.valueOf(id))// 其他参数信息
					.addFormDataPart( //给Builder添加上传的文件
							"files",  //请求的名字
							tempfile.getName(), //文件的文字，服务器端用来解析的
							RequestBody.create(MediaType.parse("multipart/form-data"), tempfile)//创建RequestBody，把上传的文件放入
					);
			MultipartBody requestBody = builder.build();
			Request request = new Request.Builder()
					.url(uploadUrl)
					.post(requestBody)
					.build();
			OkHttpClient client = new OkHttpClient.Builder()
					.connectTimeout(10000, TimeUnit.SECONDS)
					.readTimeout(10000, TimeUnit.SECONDS)
					.writeTimeout(10000, TimeUnit.SECONDS).build();
			client.newCall(request).enqueue(new Callback() {
				@Override
				public void onFailure(Call call, final IOException e) {
					Log.e("TAG", "返回内容===失败>:" + e.toString());
					t_com.PrintLog( e.toString());
				}

				@Override
				public void onResponse(Call call, Response response) throws IOException {
					String result = response.body().string();
					Log.e("TAG", "返回内容===成功>:" + result);
					t_com.PrintLog(result);
					m_upfile_isok = true;
				}
			});
			return m_upfile_isok;
		}
		//上传数据并打印保存
		@SuppressWarnings("resource")
		private boolean SendDataToDbAndPrint(String Pname,int Printnum)
		{
			ArrayList<String> list = new ArrayList<String>();
			list.add(new String(Common.cartype.vin));
			list.add(Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, 16));
			list.add(Pname);
			list.add(Common.cartype.car);
			list.add(Common.cartype.name);
			list.add(Common.cartype.start_time);
			list.add(Common.cartype.box_serialnum);
			//这里改成app version
			list.add(getString(R.string.main_title));
			//增加的这里打印PIN码
			list.add(Commonfunc.bytesToHexStringP(Common.cartype.pin, 0, 2));
			//打印结果
			if(Printnum == 0)
				list.add(0 + "");
			else
				list.add(Common.Printnumber + "");
			list.add(Common.cartype.Result + "");
			//增加用户名
			list.add("test001");
			String v_print = "";
			int v_size = modules.size();
			for(EcuModule mod : modules)
			{
				v_print += mod.toString();
			}
			return true;
		}
		//设置can初始化
		private boolean SetCanInit(int mode,long baud,byte [] Pauto)
		{
			int i = 0;
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetCanBaud(baud)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置波特率成功： " + baud);
			Sleep(50);
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetCanMode(mode)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置CanMode成功：" + mode);
			Sleep(50);
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetCommunTimer(10,2000,3000,5)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置CAN 时序成功!");
			Sleep(50);
			//设置CAN自动回复帧
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetCanAutoFrame(Pauto, 8)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置CAN 时序成功!");
			Sleep(50);
			return true;
		}
		private boolean SetCanInitTimer(int P4)
		{
			int i;
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetCommunTimer(10,2000,3000,P4)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置CAN 时序成功!");
			return true;
		}
		private boolean SetStandCanAutoFrame(int Pid,byte [] Pdata)
		{
			int i;
			for(i = 0; i < Common.cmd_freq; i ++)
			{
				if(t_com.SetStandCanAutoFrame(Pid, Pdata, Pdata.length)) break;
				Sleep(50);
			}
			if(i >= Common.cmd_freq) return false;
			if(Common.Debug) Log.i(TAG,"设置CAN 时序成功!");
			return true;
		}
		///////////////////////////////////////功能模块-start////////////////////////////////////////////////
		//执行K IMMO防盗匹配
		private void fun_S30_F4_BDCM1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_name == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //safekey
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"---",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF18C:  //控制器序列号,校验格式，只能是数字字母
								v_show = new String(v_data);
								v_get = "";
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xD001:  //key num
								v_show = "" + v_data[0];
								v_get = "2";
								break;
							case 0x5505:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.flid, 0, 4);
								break;
							case 0x5506:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.frid, 0, 4);
								break;
							case 0x5507:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.blid, 0, 4);
								break;
							case 0x5508:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.brid, 0, 4);
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnull);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"---",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"---",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F4_Stand1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			String partnumber_cache = "";  //特殊处理
			int error = 0;
			float v_testvalue = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_name == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(i == 1)
					{
						byte [] cmddef = new byte[]{0x02,0x10,0x01};
						error = t_com.CanOne2OneUDS(v_req, v_res, cmddef,cmddef.length, Readbuf, 5003, Common.cmdmaxtime, 3);
						Sleep(100);
					}
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					//特殊处理多次发送命令
					if(cmd[1] == 0x28 || cmd[1] == 0x85) //广播
					{
						error = t_com.CanSendOneOnly(0x7DF, cmd, cmd.length);
					}
					else if((cmd[1] == 0x22 && (cmd[2]&0xFF) == 0xF1 && cmd[3] == 0x70))
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 3);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"---",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
							continue;
						}
						else if(((cmd[1]&0xFF) + 0x40) != (Readbuf[1]&0xFF)) //异常
						{
							if(cmd[1] == 0x27) //安全认证
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
								break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"---",Common.Rnok);
								continue;
							}
						}

					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (cmd[2]&0xFF) * 0x100 + (cmd[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(Pstep.getName()) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), partnumber_cache,"supplierId");
								break;
							case 0xF18C:  //控制器序列号,校验格式，只能是数字字母
								v_show = new String(v_data);
								if("IHU".equals(Pstep.getName()) || "TBOX".equals(Pstep.getName()) || "VCU".equals(Pstep.getName()) || "ICU".equals(Pstep.getName()) || "BMS".equals(Pstep.getName()))
									v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "serialNumber");
								else
									v_get = "format";
								break;
							case 0xF191:
							case 0xF193:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "hardwareVersion");
								break;
							case 0xF1C0:
							case 0xF195:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								if("PDCS".equals(Pstep.getName()))
								{
									v_testvalue = (float) (((v_data[2]&0xFF) * 0x100 + (v_data[3]&0xFF)) * 0.00333);
									v_show = ((v_data[2]&0xFF) * 0x100 + (v_data[3]&0xFF)) * 0.00333 + " %";
								}
								else
								{
									v_testvalue = (v_data[0]&0xFF);
									v_show = (v_data[0]&0xFF) + " %";
								}
								break;
							case 0xF100:  //国标码ASC码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF2C2:
							case 0xF200:  //驱动电机编码ASC码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr("MCU", "ecode");
								break;
							case 0x460B: //iccid
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "iccid_code");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0x5514: //BTM
							case 0x5511:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(v_show.length() > 10)
									v_get = "OK";
								else
									v_get = "NOK";
								break;
							case 0x5515:
							case 0x5512:
							case 0x5513:
							case 0xF198:
							case 0x460D:   //imei
								v_show = new String(v_data);
								v_get = "OK";
								break;
						}
						v_show = v_show.replace(" ", "");
						v_show = v_show.trim();
						if(v_code == 0xF191 && Pstep.getName().equals("MFCP"))
						{
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
						}
						else if(v_code == 0xF160)
						{
							if(v_testvalue >= 95) //ok
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_code == 0xF187 && Pstep.getName().equals("IHU"))
						{
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
						}
						else if(v_code == 0xF187 && Pstep.getName().equals("ACU"))
						{
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
						}
						else if(v_get.equals("OK"))
						{
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
						}
						else if(v_get.equals("NOK"))
						{
							ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.equals("format")) //格式校验
						{
							if(Commonfunc.CheckSerialformat(v_data) == true)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show, "[Mes:空]",Common.Rnok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x71) //动作
					{
						ShowProcessList(step_id + i,step_name,"action","",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}

		private void fun_EP12_F4_OBD1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = 0x7DF;
			int v_res = 0x7E8;
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			int error = 0;
			SetCanInitTimer(15);
			byte [] v_autocmd = {0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
			SetStandCanAutoFrame(0x7E0,v_autocmd);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_name == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------

					//特殊处理多次发送命令
					if(false) //广播
					{
						error = t_com.CanSendOneOnly(0x7DF, cmd, cmd.length);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x49) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF);
						switch(v_code)
						{
							case 0x02:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;

						}
						if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
			SetStandCanAutoFrame(0,v_autocmd);
		}

		private void fun_S30_F4_OBD1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = 0x7DF;
			int v_res = 0x7E8;
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			int error = 0;
			SetCanInitTimer(15);
			byte [] v_autocmd = {0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
			SetStandCanAutoFrame(0x7E0,v_autocmd);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_name == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------

					//特殊处理多次发送命令
					if(false) //广播
					{
						error = t_com.CanSendOneOnly(0x7DF, cmd, cmd.length);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x49) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF);
						switch(v_code)
						{
							case 0x02:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;

						}
						if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
			SetStandCanAutoFrame(0,v_autocmd);
		}
		//四轮工位标定程序
		private void fun_S30_F3_Stand1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			int error = 0;
			int esc_g = 3;
			int esc_a = 3;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x07) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,"解析配置码:" + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"获取配置码","",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x31 && (cmd[2]&0xFF) == 0x03) //查询
					{
						Sleep(1000);
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
						else if(((cmd[1]&0xFF) + 0x40) != (Readbuf[1]&0xFF)) //异常
						{
							if(cmd[1] == 0x27) //安全认证
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								continue;
							}
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF18C:
								v_show = new String(v_data);
								v_get = "";
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if((Readbuf[1] == 0x71) && (Readbuf[2] == 0x03) && (Readbuf[3] == 0x55)) //动作
					{
						if(Readbuf[5] == 0x00)
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x01) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else if(Readbuf[5] == 0x02)
						{
							ShowProcessList(step_id + i,step_name,"数据检测错误","",Common.Rok);
						}
						else if(Readbuf[5] == 0x03)
						{
							ShowProcessList(step_id + i,step_name,"0 位转角错误","",Common.Rok);
						}
						else if(Readbuf[5] == 0x04)
						{
							ShowProcessList(step_id + i,step_name,"EEPROM 错误","",Common.Rok);
						}
						else
							ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
					}
					else if((Readbuf[1] == 0x71) && (Readbuf[2] == 0x03) && ((Readbuf[3]&0xFF) == 0xAC)) //G-sensor calibration
					{
						if(Readbuf[5] == 0x20) //
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x00) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_g > 0)
							{
								i -= 2;
								esc_g --;
								ShowFuncTip("G-Sensor标定重试,第:" + (3 - esc_g) + "次...");
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x03) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						if(Readbuf[5] == 0x02) //Running
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x03) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_a > 0)
							{
								i -= 2;
								esc_a --;
								ShowFuncTip("EPB Assembly Check重试,第:" + (3 - esc_a) + "次...");
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x51 && Readbuf[2] == 0x01)
					{
						Sleep(2000);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_EP12_F3_Stand1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			int error = 0;
			int esc_g = 3;
			int esc_a = 3;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x07) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context25) + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context26),"",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x31 && (cmd[2]&0xFF) == 0x03) //查询
					{
						Sleep(1000);
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
						else if(((cmd[1]&0xFF) + 0x40) != (Readbuf[1]&0xFF)) //异常
						{
							if(cmd[1] == 0x27) //安全认证
							{
								ShowProcessList(step_id + i,step_name,"Error:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,"Error:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								continue;
							}
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF18C:
								v_show = new String(v_data);
								v_get = "";
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if((Readbuf[1] == 0x71) && (Readbuf[2] == 0x03) && (Readbuf[3] == 0x55)) //动作
					{
						if(Readbuf[5] == 0x00)
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x01) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else if(Readbuf[5] == 0x02)
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context29),"",Common.Rok);
						}
						else if(Readbuf[5] == 0x03)
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context30),"",Common.Rok);
						}
						else if(Readbuf[5] == 0x04)
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context31),"",Common.Rok);
						}
						else
							ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
					}
					else if((Readbuf[1] == 0x71) && (Readbuf[2] == 0x03) && ((Readbuf[3]&0xFF) == 0xAC)) //G-sensor calibration
					{
						if(Readbuf[5] == 0x20) //
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x00) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_g > 0)
							{
								i -= 2;
								esc_g --;
								ShowFuncTip(getString(R.string.msg_show_context32) + (3 - esc_g) + getString(R.string.msg_show_context33));
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x03) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						if(Readbuf[5] == 0x02) //Running
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x03) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_a > 0)
							{
								i -= 2;
								esc_a --;
								ShowFuncTip(getString(R.string.msg_show_context34) + (3 - esc_a) + getString(R.string.msg_show_context33));
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x51 && Readbuf[2] == 0x01)
					{
						Sleep(2000);
					}
					else
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}
		private void fun_EP12_F2_CBS1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			byte [] cmdclr = new byte[]{0x04,0x14,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF};
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if (i==7){
						for (int k = 5;k>0;k--){
						String value="正在执行推杆零位匹配，请等待"+k+"s";
						ShowFuncTip(value);
						error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
						Sleep(1000);
						}
					} else if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x07) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,"解析配置码:" + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"获取配置码","",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					//特殊处理多次发送命令
					if (i==9){
						String v_tip = "深踩制动踏板5次,完成后点击[确认]继续";
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
						for (int k = 1;k<100;k++){
							String value="正在执行负载匹配，已进行"+k+"s";
							ShowFuncTip(value);
							error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
							Sleep(1000);
							if (Readbuf[1]==0x7F)
								continue;
							else break;
						}
					} else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(i == 13 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(i == 31 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val > 26))
						{
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
							Sleep(2000);
						}
						else
						{
							//ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm",Common.Rnok);
							//break;
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0C) //第二路
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0F) //电压
					{
						int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 8));
						if(v_val <= 15 && v_val > 10)
							ShowProcessList(step_id + i,step_name,v_val + " V","",Common.Rok);
						else
						{
							//ShowProcessList(step_id + i,step_name,v_val + " V",Common.Rnok);
							//break;
							ShowFuncTip(step_name + ":" + v_val + " V");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x12) //温度
					{
						float v_val =(float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
						float v_val1 =(float) (((Readbuf[8]&0xFF)*256+(Readbuf[9]&0xFF))/Math.pow(2, 6));
						if((v_val <= 70) && (v_val1 <= 70) )
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_code == 0x2007)
						{
							int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
							if(v_val >= 35)
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rnok);
						}
						else if(v_code == 0x2001)
						{
							float v_val = (float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 9));
							if(v_val <= 23.825)
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
							case 0xF200:
								v_show = new String(v_data);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x01)
					{
						Sleep(600);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[3]&0xFF) == 0xAC) //G-sensor calibration
					{
						if((Readbuf[5]&0xFF) == 0x01) //
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x02) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else if((Readbuf[5]&0xFF) == 0x71) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context57),"",Common.Rnok);
							break;
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F2_CBS1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			byte [] cmdclr = new byte[]{0x04,0x14,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF};
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(i == 1 || i == 22) //提示踩踏板5次
					{
						String v_tip = "测试条件:\n1、整车钥匙处于ON档；   2、供电电压正常10V-15V;\n3、深踩制动踏板2次后,完全松开制动踏板;      4、档位处于N/P档;\n5、车辆静止;     6、松开手刹.";
						if(i == 1)
						{
							error = t_com.CanSendOneOnly(v_req, cmdclr, cmdclr.length);
							Sleep(100);
							t_com.CanOne2OneUDS(v_req, v_res, cmdclr,cmdclr.length, Readbuf, 5003, Common.cmdmaxtime, 3);
							Sleep(100);
						}
						else if(i == 22)
						{
							v_tip = "深踩制动踏板5次,完成后点击[确认]继续";
						}

						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(i == 27 || i == 29) //27wait 20s;29wait 25s
					{
						Common.wait = true;
						int waittime = 20;
						if(i == 29)
							waittime = 25;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, waittime, 0,"等待...").sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(i == 31 || i == 34)
					{
						String v_tip = "踩制动踏板到底.并保持.";
						if(i == 31) //踩制动踏板到底
						{

						}
						else if(i == 34) //松开制动踏板
						{
							v_tip = "松开制动踏板.";
						}
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x07) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,"解析配置码:" + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"获取配置码","",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(i == 13 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(i == 31 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val > 26))
						{
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
							Sleep(2000);
						}
						else
						{
							//ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm",Common.Rnok);
							//break;
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0C) //第二路
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0F) //电压
					{
						int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 8));
						if(v_val <= 15 && v_val > 10)
							ShowProcessList(step_id + i,step_name,v_val + " V","",Common.Rok);
						else
						{
							//ShowProcessList(step_id + i,step_name,v_val + " V",Common.Rnok);
							//break;
							ShowFuncTip(step_name + ":" + v_val + " V");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x12) //温度
					{
						float v_val =(float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
						float v_val1 =(float) (((Readbuf[8]&0xFF)*256+(Readbuf[9]&0xFF))/Math.pow(2, 6));
						if((v_val <= 70) && (v_val1 <= 70) )
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else if(v_code == 0x2007)
						{
							int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
							if(v_val >= 35)
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rnok);
						}
						else if(v_code == 0x2001)
						{
							float v_val = (float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 9));
							if(v_val <= 23.825)
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
							case 0xF200:
								v_show = new String(v_data);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x01)
					{
						Sleep(600);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[3]&0xFF) == 0xAC) //G-sensor calibration
					{
						if((Readbuf[5]&0xFF) == 0x01) //
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x02) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else if((Readbuf[5]&0xFF) == 0x71) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context57),"",Common.Rnok);
							break;
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_EP12_F2_EHB1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			byte [] cmdclr = new byte[]{0x04,0x14,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF};
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
//					int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
					if (cmd[1]==0x31&&cmd[2]==0x03&&cmd[4]==0x01){
						for (int k = 5;k>0;k--){
							String value="正在执行推杆零位匹配，请等待"+k+"s";
							ShowFuncTip(value);
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					} else if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x07) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,"解析配置码:" + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"获取配置码","",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					//特殊处理多次发送命令
					if (cmd[1]==0x31&&cmd[2]==0x03&&cmd[4]==0x02){
						String v_tip = "深踩制动踏板5次,完成后点击[确认]继续";
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
						for (int k = 1;k<100;k++){
							String value="正在执行负载匹配，已进行"+k+"s";
							ShowFuncTip(value);
							error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
							Sleep(1000);
							if (Readbuf[1]==0x7F)
								continue;
							else break;
						}
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(i == 13 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(i == 31 && Readbuf[1] == 0x62 && Readbuf[3] == 0x0B)
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val > 26))
						{
							ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm","",Common.Rok);
							Sleep(2000);
						}
						else
						{
							//ShowProcessList(step_id + i,step_name,"第一路:" + v_val + "mm",Common.Rnok);
							//break;
							ShowFuncTip("第一路:" + v_val + "mm");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0C) //第二路
					{
						float v_val =(float) (((Readbuf[4])*256+(Readbuf[5]))/Math.pow(2, 7));
						if((v_val <= 20.03))
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"第二路:" + v_val + "mm","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x0F) //电压
					{
						int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 8));
						if(v_val <= 15 && v_val > 10)
							ShowProcessList(step_id + i,step_name,v_val + " V","",Common.Rok);
						else
						{
							//ShowProcessList(step_id + i,step_name,v_val + " V",Common.Rnok);
							//break;
							ShowFuncTip(step_name + ":" + v_val + " V");
							Sleep(1000);
							i --;
						}
					}
					else if(Readbuf[1] == 0x62 && Readbuf[3] == 0x12) //温度
					{
						float v_val =(float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
						float v_val1 =(float) (((Readbuf[8]&0xFF)*256+(Readbuf[9]&0xFF))/Math.pow(2, 6));
						if((v_val <= 70) && (v_val1 <= 70) )
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rok);
						else
						{
							ShowProcessList(step_id + i,step_name,"功率区:" + v_val + "℃;数字区:" + v_val1 + "℃","",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else if(v_code == 0x2007)
						{
							int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
							if(v_val >= 35)
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rnok);
						}
						else if(v_code == 0x2001)
						{
							float v_val = (float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 9));
							if(v_val <= 23.825)
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
							case 0xF200:
								v_show = new String(v_data);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x01)
					{
						Sleep(600);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[3]&0xFF) == 0xAC) //G-sensor calibration
					{
						if((Readbuf[5]&0xFF) == 0x01) //
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x02) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else if((Readbuf[5]&0xFF) == 0x71) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context57),"",Common.Rnok);
							break;
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F2_CBS2(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			int v_g1 = 3;
			int v_g2 = 3;
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				if(step_t == null) break;
				step_name = t_lang.get((step_id + i) + "");
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(i == 1 || i == 22) //提示踩踏板5次
					{
						String v_tip = "测试条件:\n1、整车钥匙处于ON档；   2、供电电压正常10V-15V;\n3、深踩制动踏板2次后,完全松开制动踏板;      4、档位处于N/P档;\n5、车辆静止;     6、松开手刹.";
						if(i == 1)
						{

						}
						else if(i == 22)
						{
							v_tip = "深踩制动踏板5次,完成后点击[确认]继续";
						}

						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(i == 11 || i == 13 || i == 16) //wait 5s;13wait 32s
					{
						Common.wait = true;
						int waittime = 5;
						if(i == 13)
							waittime = 32;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, waittime, 0,"等待...").sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_code == 0x2007)
						{
							int v_val =(int) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 6));
							if(v_val >= 35)
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " A","",Common.Rnok);
						}
						else if(v_code == 0x2001)
						{
							float v_val = (float) (((Readbuf[4]&0xFF)*256+(Readbuf[5]&0xFF))/Math.pow(2, 9));
							if(v_val <= 23.825)
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_val + " mm","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x01)
					{
						Sleep(600);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x03 && (Readbuf[4]&0xFF) == 0x01)
					{
						if((Readbuf[5]&0xFF) == 0x00) //
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x01) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(v_g1 > 0)
							{
								i -= 2;
								ShowFuncTip("重试第:" + (4 - v_g1) + "次...");
								v_g1 --;
								Sleep(500);
								continue;
							}
							ShowProcessList(step_id + i,step_name,"Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2]&0xFF) == 0x03 && (Readbuf[4]&0xFF) == 0x02)
					{
						if((Readbuf[5]&0xFF) == 0x00) //
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x01) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(v_g2 > 0)
							{
								i -= 2;
								ShowFuncTip("重试第:" + (4 - v_g2) + "次...");
								v_g2 --;
								Sleep(500);
								continue;
							}
							ShowProcessList(step_id + i,step_name,"Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_EP12_F0_BDCM1(Profile.Section Pstep)
		{
			if(Pstep == null) return;
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			String partnumber_cache = "";
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			byte [] cmdkey = new byte[]{0x04,0x31,0x03, (byte) 0xAB,0x02};
			int cmdkeylen = cmdkey.length;
			int key_retry = 3; //钥匙学习重复次数
			int key_num = 0; //钥匙学习顺序
			int key_select = 2; //学2把钥匙
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					//切换ID
					if(i == 56) //tbox
					{
						if((Common.cartype.getMap("TBOX") > 0)||(Common.cartype.getMap("TBOX1") > 0))
						{
							v_req = Common.cartype.getModuleIdReq("TBOX1");
							v_res = Common.cartype.getModuleIdRes("TBOX1");
						}
						else
						{
							i += 5;continue;
						}
					}else if(i==29) {
						//todo 根据工位决定是否弹提示框
						if (Common.cartype.OptMode == 1) {
							//提示操作
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0, getString(R.string.msg_show_context35)).sendToTarget();
							while (Common.wait) //发链路
							{
								//error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
							if (Common.dialog_result == false) {
								i += 3;
								continue;
							} else {
								v_req = Common.cartype.getModuleIdReq("VCU1");
								v_res = Common.cartype.getModuleIdRes("VCU1");
							}
						} else {
							i += 3;
							continue;
						}
					}else if (i==33){
						v_req = Common.cartype.getModuleIdReq("BDCM");
						v_res = Common.cartype.getModuleIdRes("BDCM");
					}
					if(i == 1)
					{
						byte [] cmddef = new byte[]{0x02,0x10,0x01};
						error = t_com.CanOne2OneUDS(v_req, v_res, cmddef,cmddef.length, Readbuf, 5003, Common.cmdmaxtime, 3);
						Sleep(100);
					}
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					} else if(cmd[1] == 0x2E && cmd[3] == 0x70) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context25) + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context26),"",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0xF1) //写PIN
					{
						System.arraycopy(Common.cartype.pin, 0, cmd, 4, cmd[0] - 3);

					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x0F) //写ESK
					{
						System.arraycopy(Common.cartype.esk, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x05) //写LF
					{
						if((Common.cartype.flid[0] | Common.cartype.flid[1] | Common.cartype.flid[2] | Common.cartype.flid[3]) == 0) continue;
						System.arraycopy(Common.cartype.flid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x06) //写RF
					{
						if((Common.cartype.frid[0] | Common.cartype.frid[1] | Common.cartype.frid[2] | Common.cartype.frid[3]) == 0) continue;
						System.arraycopy(Common.cartype.frid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x08) //写RB
					{
						if((Common.cartype.brid[0] | Common.cartype.brid[1] | Common.cartype.brid[2] | Common.cartype.brid[3]) == 0) continue;
						System.arraycopy(Common.cartype.brid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(i == 8)  //学习钥匙
					{
						//根据钥匙数量提示放钥匙s
						String v_tip = "";
						if(key_num == 1) //第一把钥匙
							v_tip = getString(R.string.msg_show_context36) + ",";
						else if(key_num == 10)
						{
							v_tip = getString(R.string.msg_show_context37) + ",";
							key_retry = 3;
						}
						else
							v_tip = getString(R.string.msg_show_context38) + ",";
						if(key_retry >= 0) //还在重复次数以内
						{
							if(key_num == 10) //特殊等待
							{
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,v_tip).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
								key_num = 2;
								v_tip = getString(R.string.msg_show_context38) + ",";
							}
							//提示操作
							Common.wait = true;
							key_retry --;
							v_tip += "the" + (3 - key_retry) + "times";
							m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,v_tip).sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
						}
					}

					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);

						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if(i == 8) //学钥匙
						{
							if(key_retry < 0) //重复次数过多
							{
								ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
						}
						else if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[3] == 0x13)
					{
						if(Readbuf[4] == 0)
						{
							i ++;continue;
						}
					}
					else if(i == 5) //钥匙状态
					{
						String v_tip = "未学习";
						if(Readbuf[4] == 0x01)
						{
							v_tip = getString(R.string.msg_show_context39);
							//特殊处理已经学习一把钥匙的情况
							error = t_com.CanOne2OneUDS(v_req, v_res, cmdkey,cmdkeylen, Readbuf, 5003, Common.cmdmaxtime, 3);
							if (Readbuf[2] == 0x03){
								//需要更换钥匙
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,getString(R.string.msg_show_context40)).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
							}else {//不需要更换钥匙
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,getString(R.string.msg_show_context41)).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
							}
							key_num=10;
							i+=2;
						}
						else if(Readbuf[4] == 0x02)
						{
							if (Common.cartype.OptMode==1){
								v_tip = getString(R.string.msg_show_context42);
								//提示操作
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,getString(R.string.msg_show_context43)).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
								if(Common.dialog_result == false)
								{
									i += 5;
								}
								else
								{
									key_num = 1;
								}
							}else {
								i += 5;
								v_tip = getString(R.string.msg_show_context42);
							}
						}
						else
						{
							key_num = 1;
							i += 2; continue;
						}
						ShowProcessList(step_id + i,step_name,v_tip,"",Common.Rok);
					}
					else if(i == 10) //最后检查钥匙数量
					{
						if(Readbuf[4] == 0x02)
							ShowProcessList(step_id + i,step_name,"2 keys","",Common.Rok);
						else if(Readbuf[4] == 0x01)
						{
							ShowProcessList(step_id + i,step_name,"1 key","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"Study" + (Readbuf[4]&0xFF) + "Pcs","",Common.Rnok);
							//break;
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum("BDCM") > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr("BDCM", partnumber_cache,"partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum("BDCM") > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr("BDCM", partnumber_cache,"hardwareVersion");
								break;
							case 0xF1D0:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum("BDCM") > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr("BDCM", partnumber_cache,"softwareAss");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18C:  //控制器序列号
								v_show = new String(v_data);
								if("IHU".equals(Pstep.getName()) || "TBOX".equals(Pstep.getName()) || "VCU".equals(Pstep.getName()) || "ICU".equals(Pstep.getName()) || "BMS".equals(Pstep.getName()))
									v_get = Common.cartype.getModuleStr(Pstep.getName(), "serialNumber");
								else
									v_get = "format";
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF1F1:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get =  Commonfunc.bytesToHexStringP(Common.cartype.pin, 0, Common.cartype.pin.length);
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0x900F:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, 16);
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.equals("format")) //格式校验
						{
							if(Commonfunc.CheckSerialformat(v_data) == true)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							v_show=v_show.trim();
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show ,"[Mes:null]",Common.Rnok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,"No Dtc","",Common.Rok);
					}
					else if(Readbuf[1] == 0x6F) //手动测试功能
					{
						int v_code = (Readbuf[3]&0xFF);
						String v_tip = "";
						switch(v_code)
						{
							case 0x07: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = getString(R.string.msg_show_context46);
								else
									v_tip = getString(R.string.msg_show_context47);
								break;
							case 0x08: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:位置灯是否开启?";
								else
									v_tip = "手动确认:位置灯是否关闭?";
								break;
							case 0x18: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:制动灯是否开启?";
								else
									v_tip = "手动确认:制动灯是否关闭?";
								break;
							case 0x17: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:倒车灯是否开启?";
								else
									v_tip = "手动确认:倒车灯是否关闭?";
								break;
							case 0x06: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:左侧转向灯是否开启?";
								else
									v_tip = "手动确认:左侧转向灯是否关闭?";
								break;
							case 0x03: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:近光灯是否开启?";
								else
									v_tip = "手动确认:近光灯是否关闭?";
								break;
							case 0x04: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:远光灯是否开启?";
								else
									v_tip = "手动确认:远光灯是否关闭?";
								break;
							case 0x13: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:左侧日间行车灯是否开启?";
								else
									v_tip = "手动确认:左侧日间行车灯是否关闭?";
								break;
							case 0x14: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:右侧日间行车灯是否开启?";
								else
									v_tip = "手动确认:右侧日间行车灯是否关闭?";
								break;
							case 0x12: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:危险灯是否开启?";
								else
									v_tip = "手动确认:危险灯是否关闭?";
								break;
							case 0x11: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:后雾灯是否开启?";
								else
									v_tip = "手动确认:后雾灯是否关闭?";
								break;
							case 0x28: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:高位制动灯是否开启?";
								else
									v_tip = "手动确认:高位制动灯是否关闭?";
								break;
							case 0x01: //手动确认状态
								v_tip = "手动确认:中控锁是否有动作?";
								break;
							case 0x05: //手动确认状态
								v_tip = "手动确认:高、低是否有动作?";
								break;
							case 0x67:
								if(Readbuf[5] == 0x01)
									v_tip = "后视镜打开...";
								else
									v_tip = "后视镜折叠...";
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, 3, 0,v_tip).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
								v_tip = "";
								break;
							default:
								v_tip = "";
								break;
						}
						if(v_tip.length() > 2)
						{
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 0, 0,v_tip).sendToTarget();
							while(Common.wait) //发链路
							{

								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
							if(Common.dialog_result == false) //失败
							{
								ShowProcessList(step_id + i,step_name, "","",Common.Rnok);
							}
						}

					}
					else if(Readbuf[1] == 0x71) //动作
					{
						String v_show = "";
						int v_code = (Readbuf[3]&0xFF) * 0x100 + (Readbuf[4]&0xFF);
						switch(v_code)
						{
							case 0xAB04: //删除钥匙
								if(Readbuf[2] == 0x03) //开始
								{
									key_num = 1;
								}
								ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
								break;
							case 0xAB02: //学习钥匙
								if(Readbuf[2] == 0x03) //开始
								{
									if(key_num == 1) //学完第一把
									{
										key_num = 10;

										Common.wait = true;
										m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,getString(R.string.msg_show_context44)).sendToTarget();
										while(Common.wait) //发链路
										{
											error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
											Sleep(1000);
										}
										if(Common.dialog_result == true) //是
										{
											i -= 2;
											continue;
										}
										else
										{
											key_select = 1;
										}
									}
									else if(key_num == 2)
									{
										Common.wait = true;
										m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, 3, 0,getString(R.string.msg_show_context45)).sendToTarget();
										while(Common.wait) //发链路
										{
											error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
											Sleep(1000);
										}
									}
								}
								ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
								break;
							case 0x55EA:  //复位BDCM
							case 0xAB78:
							case 0xAB76:
							case 0xAB75:
								Sleep(800);
								ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
								break;
							case 0xAB05: //BDCM生成SK
								if((Readbuf[5]&0xFF) != 0x80)
								{
									ShowProcessList(step_id + i,step_name, v_show,"",Common.Rnok);
								}
								break;
							default:
								break;
						}

					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
								v_show = Commonfunc.bytesToHexStringP(v_data, 2, 2);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}

		private void fun_S30_F0_BDCM1(Profile.Section Pstep)
		{
			if(Pstep == null) return;
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			int key_retry = 3; //钥匙学习重复次数
			int key_num = 0; //钥匙学习顺序
			int key_select = 2; //学2把钥匙
			int error = 0;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					//切换ID
					if(i == 56 || i == 57 || i == 64) //切回来
					{
						v_req = Common.cartype.getModuleIdReq(Pstep.getName());
						v_res = Common.cartype.getModuleIdRes(Pstep.getName());
					}
					else if(i == 67) //tbox
					{
						if(Common.cartype.getMap("TBOX") <= 0)
						{
							i += 5;continue;
						}
						else
						{
							v_req = Common.cartype.getModuleIdReq("TBOX");
							v_res = Common.cartype.getModuleIdRes("TBOX");
						}
					}
					else if(i == 73) //btm
					{
						if(Common.cartype.getMap("BTM") <= 0)
						{
							i += 5;continue;
						}
						else
						{
							v_req = Common.cartype.getModuleIdReq("BTM");
							v_res = Common.cartype.getModuleIdRes("BTM");
						}
					}
					if(i == 5)
					{
						//i = 36;
					}
					if(i == 1)
					{
						byte [] cmddef = new byte[]{0x02,0x10,0x01};
						error = t_com.CanOne2OneUDS(v_req, v_res, cmddef,cmddef.length, Readbuf, 5003, Common.cmdmaxtime, 3);
						Sleep(100);
					}
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E && cmd[3] == 0x00) //写配置
					{
						String config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
						if(config.length() == (cmd[0] - 3) * 2)
						{
							byte [] configb = new byte[cmd[0] - 3];
							int v_cmdlen = Commonfunc.StringToBytes(config,configb, cmd[0] - 3);
							if(v_cmdlen == (cmd[0] - 3))
								System.arraycopy(configb, 0, cmd, 4, cmd[0] - 3);
							else
							{
								ShowProcessList(step_id + i,step_name,"Read Config:" + config,"",Common.Rnok);
								break;
							}
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"Config","",Common.Rnok);
							break;
						}
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x9D) //写时间
					{
						System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x90) //写VIN
					{
						System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0xF1) //写PIN
					{
						System.arraycopy(Common.cartype.pin, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x0F) //写ESK
					{
						System.arraycopy(Common.cartype.esk, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x05) //写LF
					{
						if((Common.cartype.flid[0] | Common.cartype.flid[1] | Common.cartype.flid[2] | Common.cartype.flid[3]) == 0) continue;
						System.arraycopy(Common.cartype.flid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x06) //写RF
					{
						if((Common.cartype.frid[0] | Common.cartype.frid[1] | Common.cartype.frid[2] | Common.cartype.frid[3]) == 0) continue;
						System.arraycopy(Common.cartype.frid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x07) //写LB
					{
						if((Common.cartype.blid[0] | Common.cartype.blid[1] | Common.cartype.blid[2] | Common.cartype.blid[3]) == 0) continue;
						System.arraycopy(Common.cartype.blid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(cmd[1] == 0x2E && (cmd[3]&0xFF) == 0x08) //写RB
					{
						if((Common.cartype.brid[0] | Common.cartype.brid[1] | Common.cartype.brid[2] | Common.cartype.brid[3]) == 0) continue;
						System.arraycopy(Common.cartype.brid, 0, cmd, 4, cmd[0] - 3);
					}
					else if(i == 35)  //学习钥匙
					{
						//根据钥匙数量提示放钥匙s
						String v_tip = "";
						if(key_num == 1) //第一把钥匙
							v_tip = getString(R.string.msg_show_context36) + ",";
						else if(key_num == 10)
						{
							v_tip =getString(R.string.msg_show_context37) + ".";
							key_retry = 3;
						}
						else
							v_tip = getString(R.string.msg_show_context38) + ",";
						if(key_retry >= 0) //还在重复次数以内
						{
							if(key_num == 10) //特殊等待
							{
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,v_tip).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
								key_num = 2;
								v_tip = getString(R.string.msg_show_context38) + ",";
							}
							//提示操作
							Common.wait = true;
							key_retry --;
							v_tip += "the" + (3 - key_retry) + "times retry";
							m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,v_tip).sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
						}
					}
					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if(i == 35) //学钥匙
						{
							if(key_retry < 0) //重复次数过多
							{
								ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
						}
						else if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[3] == 0x13)
					{
						if(Readbuf[4] == 0)
						{
							i ++;continue;
						}
					}
					else if(i == 32) //钥匙状态
					{
						String v_tip = "未学习";
						if(Readbuf[4] == 0x01)
						{
							v_tip = "已学习1把";
							//提示操作
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,"已学习1把钥匙,是否擦除后重新学习?\nเรียนรู้ 1 คีย์, คุณต้องการเรียนรู้ใหม่หลังจากลบ?").sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
							if(Common.dialog_result == false) //否
							{
								i += 5;
								key_select = 1; //只学一把钥匙
							}
							else  //是
							{
								key_num = 1;
							}
						}
						else if(Readbuf[4] == 0x02)
						{
							v_tip = "已学习2把";
							//提示操作
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,"已学习2把钥匙,是否擦除后重新学习?\nเรียนรู้ 2 คีย์, คุณต้องการเรียนรู้ใหม่หลังจากลบ?").sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
							if(Common.dialog_result == false)
							{
								i += 5;
							}
							else
							{
								key_num = 1;
							}
						}
						else
						{
							key_num = 1;
							i += 2; continue;
						}
						ShowProcessList(step_id + i,step_name,v_tip,"",Common.Rok);
					}
					else if(i == 37) //最后检查钥匙数量
					{
						if(Readbuf[4] == 0x02)
							ShowProcessList(step_id + i,step_name,"2 keys","",Common.Rok);
						else if(key_select == 1 && Readbuf[4] == 0x01)
						{
							ShowProcessList(step_id + i,step_name,"1 key","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"studyid" + (Readbuf[4]&0xFF) + "把","",Common.Rnok);
							//break;
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "hardwareVersion");
								break;
							case 0xF1C0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "softwareVersion");
								break;
							case 0xF18C:  //控制器序列号
								v_show = new String(v_data);
								//v_get = Common.cartype.getModuleStr(Pstep.getName(), "serialNumber");
								v_get = "";
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
							case 0xF100:  //国标码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0x5505:  //tpms
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.flid, 0, 4);
								break;
							case 0x5506:  //tpms
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.frid, 0, 4);
								break;
							case 0x5507:  //tpms
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.blid, 0, 4);
								break;
							case 0x5508:  //tpms
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.brid, 0, 4);
								break;
							case 0x900F:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, 16);
								break;
						}
						if(v_get.equals("date"))
						{
							if(v_data[0] == 0x20)
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							v_show = v_show.trim();
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show , "[Mes:空]",Common.Rnok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x6F) //手动测试功能
					{
						int v_code = (Readbuf[3]&0xFF);
						String v_tip = "";
						switch(v_code)
						{
							case 0x07: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:转向灯是否开启?";
								else
									v_tip = "手动确认:转向灯是否关闭?";
								break;
							case 0x14: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:日间行车灯是否开启?";
								else
									v_tip = "手动确认:日间行车灯是否关闭?";
								break;
							case 0x08: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:后位置灯是否开启?";
								else
									v_tip = "手动确认:后位置灯是否关闭?";
								break;
							case 0x18: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:制动灯是否开启?";
								else
									v_tip = "手动确认:制动灯是否关闭?";
								break;
							case 0x17: //手动确认状态
								if(Readbuf[5] == 0x01)
									v_tip = "手动确认:倒车灯是否开启?";
								else
									v_tip = "手动确认:倒车灯是否关闭?";
								break;
							case 0x01: //手动确认状态
								v_tip = "手动确认:中控锁是否有动作?";
								break;
							case 0x67:
								if(Readbuf[5] == 0x01)
									v_tip = "后视镜打开...";
								else
									v_tip = "后视镜折叠...";
								Common.wait = true;
								m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, 3, 0,v_tip).sendToTarget();
								while(Common.wait) //发链路
								{
									error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
									Sleep(1000);
								}
								v_tip = "";
								break;
							default:
								v_tip = "";
								break;
						}
						if(v_tip.length() > 2)
						{
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 0, 0,v_tip).sendToTarget();
							while(Common.wait) //发链路
							{

								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
							if(Common.dialog_result == false) //失败
							{
								ShowProcessList(step_id + i,step_name, "","",Common.Rnok);
							}
						}

					}
					else if(Readbuf[1] == 0x71) //动作
					{
						String v_show = "";
						int v_code = (Readbuf[3]&0xFF) * 0x100 + (Readbuf[4]&0xFF);
						switch(v_code)
						{
							case 0xAB04: //删除钥匙
								if(Readbuf[2] == 0x03) //开始
								{
									key_num = 1;
								}
								break;
							case 0xAB02: //学习钥匙
								if(Readbuf[2] == 0x03) //开始
								{
									if(key_num == 1) //学完第一把
									{
										key_num = 10;

										Common.wait = true;
										m_handler.obtainMessage(Message.MSG_Dialog_SHow_YESNO, 3, 0,"完成第一把钥匙学习,是否继续学习第二把钥匙?.").sendToTarget();
										while(Common.wait) //发链路
										{
											error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
											Sleep(1000);
										}
										if(Common.dialog_result == true) //是
										{
											i -= 2;
											continue;
										}
										else
										{
											key_select = 1;
										}
									}
									else if(key_num == 2)
									{
										Common.wait = true;
										m_handler.obtainMessage(Message.MSG_Dialog_WaitIimeAutoClose, 3, 0,"完成第二把钥匙学习.").sendToTarget();
										while(Common.wait) //发链路
										{
											error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
											Sleep(1000);
										}
									}
								}
								break;

							default:
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
								v_show = Commonfunc.bytesToHexStringP(v_data, 2, 2);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}
		private void fun_EP12_F0_Stand1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			String partnumber_cache = "";  //特殊处理
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,(byte) 0x80};
			byte [] cmdextd= new byte[] {0x02,0x10,0x03};
			byte [] cmdendtc = new byte[] {0x02,(byte) 0x85,0x02};
			byte [] cmdendtc2 = new byte[] {0x03,0x28,0x03,0x03};
			byte [] cmddisdtc = new byte[] {0x02,(byte) 0x85,0x01};
			byte [] cmddisdtc2 = new byte[] {0x03,0x28,0x00,0x03};
			int error = 0;
			int v_plg_ok = 1;
			int esc_a = 3;
			SetCanInitTimer(20);
			byte [] v_autocmd = {0x30,0x00,0x0A,0x00,0x00,0x00,0x00,0x00};
			//SetStandCanAutoFrame(0,v_autocmd);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E) //写
					{
						int v_code = (cmd[2]&0xFF) * 0x100 + (cmd[3]&0xFF);
						switch(v_code)
						{
							case 0xF190://写VIN
								System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0xF19D:
								System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0x900F:
								System.arraycopy(Common.cartype.esk, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0xF101:
							case 0xF170:
							case 0xF110:
							case 0xF019:
							case 0xF107:
							case 0xC100:
							case 0xF050:
							case 0xF176:
							case 0xC110:
							case 0x0007:
								String v_config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								if(v_config == null)
								{
									ShowProcessList(step_id + i,step_name,"ConfigCode","",Common.Rnok);
									continue;
								}
								if(v_config.length() != ((cmd[0]&0xFF) - 3) * 2)
								{
									ShowProcessList(step_id + i,step_name,"ConfigCode length error","",Common.Rnok);
									continue;
								}
								byte [] v_configbyte = new byte[v_config.length() / 2];
								int v_configlen = Commonfunc.StringToBytes(v_config,v_configbyte,v_configbyte.length);
								if(v_configlen == ((cmd[0]&0xFF) - 3)) //ok
									System.arraycopy(v_configbyte, 0, cmd, 4, ((cmd[0]&0xFF) - 3));
								else
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context25) + v_config,"",Common.Rnok);
									continue;
								}
								break;
							case 0xF100:
								String v_eccode = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								//if(v_eccode.length() != ((cmd[0]&0xFF) - 3))
								if(v_eccode.length() < 20 || v_eccode.length() > 24)
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context48),"",Common.Rnok);
									continue;
								}
								cmd[0] = (byte) (v_eccode.length() + 3);
								byte [] v_codebyte = v_eccode.getBytes();
								System.arraycopy(v_codebyte, 0, cmd, 4, (cmd[0]&0xFF) - 3);
								break;
							case 0xF175:
								String v_configcode = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								if(v_configcode == null)
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context49),"",Common.Rnok);
									continue;
								}
								if(v_configcode.length() != ((cmd[0]&0xFF) - 3) * 2)
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context50),"",Common.Rnok);
									continue;
								}
								byte [] v_conbyte = new byte[v_configcode.length() / 2];
								int v_conlen = Commonfunc.StringToBytes(v_configcode,v_conbyte,v_conbyte.length);
								if(v_conlen == ((cmd[0]&0xFF) - 3)) //ok
									System.arraycopy(v_conbyte, 0, cmd, 4, ((cmd[0]&0xFF) - 3));
								else
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context51) + v_configcode,"",Common.Rnok);
									continue;
								}
								break;
							case 0xF200:
							case 0xF1C2: //电机编码是ASC码
								v_eccode = Common.cartype.getModuleStr("MCU", "ecode");
								if(v_eccode.length() != ((cmd[0]&0xFF) - 3))
								{
									ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context52),"",Common.Rnok);
									return;
								}
								v_codebyte = v_eccode.getBytes();
								System.arraycopy(v_codebyte, 0, cmd, 4, (cmd[0]&0xFF) - 3);
								break;
						}
					}
					else if(cmd[1] == 0x31 && cmd[2] == 0x01 && (cmd[3]&0xFF) == 0xF0 && cmd[4] == 0x01) //踩踏板
					{
						String v_tip = getString(R.string.msg_show_context53);
						Common.wait = true;
						m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 0, 0,v_tip).sendToTarget();
						Sleep(100);
						while(Common.wait) //发链路
						{
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(cmd[1] == 0x31 && cmd[2] == 0x03 && (cmd[3]&0xFF) == 0xF0 && cmd[4] == 0x01)
					{
						for (int k = 5;k>0;k--){
							String value= getString(R.string.msg_show_context54)+k+"s...";
							ShowFuncTip(value);
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(cmd[1] == 0x31 && cmd[2] == 0x03 && (cmd[3]&0xFF) == 0xF0 && cmd[4] == 0x02)
					{
						for (int k = 60;k>0;k--){
							String value=getString(R.string.msg_show_context55) +k+"s...";
							ShowFuncTip(value);
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}
					else if(cmd[1] == 0x31 && cmd[2] == 0x03 && (cmd[3]&0xFF) == 0xF2 && cmd[4] == 0x02)
					{
						for (int k = 7;k>0;k--){
							String value= getString(R.string.msg_show_context56) +k+"s...";
							ShowFuncTip(value);
							error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							Sleep(1000);
						}
					}

					//特殊处理多次发送命令
					if((cmd[1] == 0x22 && (cmd[2]&0xFF) == 0xF1 && cmd[3] == 0x70))
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 3);
					} else if (Pstep.getName().equals("VCU1")&&(i==20||i==21)) {  //车管所读VIN码
						v_req= 0x7DF;
						v_res= 0x7E8;
						v_autocmd = new byte[]{0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
						SetStandCanAutoFrame(0x7E0,v_autocmd);
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					} else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						//break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						//return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
						else if(((cmd[1]&0xFF) + 0x40) != (Readbuf[1]&0xFF)) //异常
						{
							if(cmd[1] == 0x27) //安全认证
							{
								ShowProcessList(step_id + i,step_name,"Error:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,"Error:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								continue;
							}
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[2] == 0x64 && Readbuf[3] == 0x15) //lock
					{
						if(Readbuf[4] == 0x01)
							ShowProcessList(step_id + i,step_name,"unlock","",Common.Rok);
						else if(Readbuf[4] == 0x00)
							ShowProcessList(step_id + i,step_name,"lock","",Common.Rok);
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:   //partnumber
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(Pstep.getName()) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								if("PDCS".equals(module))
									v_get = "OK";
								else
									v_get = Common.cartype.getModuleStr(Pstep.getName(), partnumber_cache,"supplierId");
								break;
							case 0xF191:
							case 0xF193:
								v_show = new String(v_data);
								if("PDCS".equals(module))
									v_get = "OK";
								else
									v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "hardwareVersion");
								break;
							case 0xF1D0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "softwareAss");
								break;
							case 0xF1C0:
							case 0xF195:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), partnumber_cache,"softwareVersion");
								break;
							case 0xF18C:  //控制器序列号,校验格式，只能是数字字母
								v_show = new String(v_data);
								if("IHU".equals(Pstep.getName()))
									v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "serialNumber");
								else
									v_get = "format";
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0x9010:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "01";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF176:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								int v_bai = 0;
								if("PDCS".equals(Pstep.getName()))
								{
									v_bai = (int)(((v_data[2]&0xFF) * 0x100 + (v_data[3]&0xFF)) * 0.00333);
								}
								else
								{
									v_bai = (v_data[0]&0xFF);
								}
								v_show =v_bai + " %";
								if(v_bai > 95)
									v_get = "OK";
								else
									v_get = "100%";
								break;
							case 0xF100:  //国标码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF1C2:  //电机编码MCU
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr("MCU", "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0x5515:  //BTM
							case 0x5514:
							case 0x5511:
							case 0x5512:
							case 0x5513:
								v_show = new String(v_data);
								v_get = "OK";
								break;
							case 0xD001:
								v_show = (v_data[0]&0x0F) + "Pcs";
								v_get = "OK";
								break;
							case 0x900F:   //ESK
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Commonfunc.bytesToHexStringP(Common.cartype.esk, 0, Common.cartype.esk.length);
								break;
						}
						v_show = v_show.replace(" ","");
						v_show = v_show.trim();
						if(v_get.equals("OK"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_get.equals("date"))
						{
							if((v_data[0] == 0x20) && (v_data[2] < 0x13) && (v_data[3] < 0x32))
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.equals("format")) //格式校验
						{
							if(Commonfunc.CheckSerialformat(v_data) == true)
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							v_show = v_show.replace(" ", "");
							v_show = v_show.trim();
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else {
							ShowProcessList(step_id + i, step_name, v_show, "[Mes:null]", Common.Rnok);
						}
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(1500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
						if(Pstep.getName().equals("GW"))
						{
							t_com.CanSendOneOnly(0x7DF, cmddisdtc2, cmddisdtc2.length);
							Sleep(50);
							t_com.CanSendOneOnly(0x7DF, cmddisdtc, cmddisdtc.length);
							Sleep(50);
						}
						else if(Pstep.getName().equals("IHU"))
						{
							Sleep(3000);
						}
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
							case 0xF200:
								v_show = new String(v_data);
								break;
							case 0xF170:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								Sleep(500);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && ((Readbuf[3]&0xFF) == 0xFA) && ((Readbuf[4]&0xFF) == 0x10))
					{
						if((Readbuf[5]&0xFF) == 0x05 || (Readbuf[5]&0xFF) == 0x03 || (Readbuf[5]&0xFF) == 0x06) //wait
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x04) //ok
						{
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name, "Error:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							//break;
							v_plg_ok = 0;
						}
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && ((Readbuf[3]&0xFF) == 0xFA) && ((Readbuf[4]&0xFF) == 0x15))
					{
						if((Readbuf[5]&0xFF) == 0x02) //wait
						{
							i --;
							Sleep(1000);
						}
						else
						{
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						}
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x01) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(1000);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x03) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						if(Readbuf[5] == 0x02) //Running
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x03) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_a > 0)
							{
								i -= 2;
								esc_a --;
								ShowFuncTip("EPB Assembly Check,retry,the:" + (3 - esc_a) + "S...");
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F0_Stand1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pstep.getName());
			int v_res = Common.cartype.getModuleIdRes(Pstep.getName());
			String step_t = null;
			String step_name = null;
			String partnumber_cache = "";  //特殊处理
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,(byte) 0x80};
			byte [] cmdextd= new byte[] {0x02,0x10,0x03};
			byte [] cmdendtc = new byte[] {0x02,(byte) 0x85,0x02};
			byte [] cmdendtc2 = new byte[] {0x03,0x28,0x03,0x03};
			byte [] cmddisdtc = new byte[] {0x02,(byte) 0x85,0x01};
			byte [] cmddisdtc2 = new byte[] {0x03,0x28,0x00,0x03};
			int error = 0;
			int v_plg_ok = 1;
			int esc_a = 3;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					if(Pstep.getName().equals("PLG"))
					{
						if(i == 13) //切换到BDCM
						{
							v_req = Common.cartype.getModuleIdReq("BDCM");
							v_res = Common.cartype.getModuleIdRes("BDCM");
						}
						else if(i == 17) //切换回来
						{
							v_req = Common.cartype.getModuleIdReq(Pstep.getName());
							v_res = Common.cartype.getModuleIdRes(Pstep.getName());
							//for(int ss =0;ss < 4;ss++)
							//{
							//	error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
							//	Sleep(1000);
							//}
						}
						else if(i == 8) //提示:1、确认后背门是否关闭;2、确保后背门处无人操作,自动开启后背门.
						{
							//提示操作
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,"提示:\n1、确认后背门是否关闭;\n2、确保后背门处无人操作,自动开启后背门.").sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
						}
						else if(i == 19)
						{
							if(v_plg_ok == 0) break;
						}
					}
					else if(Pstep.getName().equals("ESC"))
					{
						if(i == 1) //松手刹
						{
							//提示操作
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Dialog_WaitTime, 3, 0,"重要提示:\n请释放EPB开关!!!").sendToTarget();
							while(Common.wait) //发链路
							{
								error = t_com.CanSendOneOnly(v_req, cmdlink, cmdlink.length);
								Sleep(1000);
							}
						}
					}
					else if(Pstep.getName().equals("GW"))
					{
						if(i == 7)
						{
							t_com.CanSendOneOnly(0x7DF, cmdextd, cmdextd.length);
							Sleep(50);
							t_com.CanSendOneOnly(0x7DF, cmdendtc, cmdendtc.length);
							Sleep(50);
							t_com.CanSendOneOnly(0x7DF, cmdendtc2, cmdendtc2.length);
							Sleep(50);
						}
					}
					if(i == 1)
					{
						byte [] cmddef = new byte[]{0x02,0x10,0x01};
						error = t_com.CanOne2OneUDS(v_req, v_res, cmddef,cmddef.length, Readbuf, 5003, Common.cmdmaxtime, 3);
						Sleep(100);
					}
					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E) //写
					{
						int v_code = (cmd[2]&0xFF) * 0x100 + (cmd[3]&0xFF);
						switch(v_code)
						{
							case 0xF190://写VIN
								System.arraycopy(Common.cartype.vin, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0xF19D:
								System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0x900F:
								System.arraycopy(Common.cartype.esk, 0, cmd, 4, cmd[0] - 3);
								break;
							case 0xF101:
							case 0xF170:
							case 0xF110:
							case 0xF019:
							case 0xF107:
							case 0xC100:
							case 0xF050:
							case 0xC110:
							case 0x0007:
								String v_config = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								if(v_config == null)
								{
									ShowProcessList(step_id + i,step_name,"获取配置码","",Common.Rnok);
									continue;
								}
								if(v_config.length() != ((cmd[0]&0xFF) - 3) * 2)
								{
									ShowProcessList(step_id + i,step_name,"配置码长度错误","",Common.Rnok);
									continue;
								}
								byte [] v_configbyte = new byte[v_config.length() / 2];
								int v_configlen = Commonfunc.StringToBytes(v_config,v_configbyte,v_configbyte.length);
								if(v_configlen == ((cmd[0]&0xFF) - 3)) //ok
									System.arraycopy(v_configbyte, 0, cmd, 4, ((cmd[0]&0xFF) - 3));
								else
								{
									ShowProcessList(step_id + i,step_name,"解析配置码:" + v_config,"",Common.Rnok);
									continue;
								}
								break;
							case 0xF100:
								String v_eccode = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								//if(v_eccode.length() != ((cmd[0]&0xFF) - 3))
								if(v_eccode.length() < 20 || v_eccode.length() > 24)
								{
									ShowProcessList(step_id + i,step_name,"获取国标码","",Common.Rnok);
									continue;
								}
								cmd[0] = (byte) (v_eccode.length() + 3);
								byte [] v_codebyte = v_eccode.getBytes();
								System.arraycopy(v_codebyte, 0, cmd, 4, (cmd[0]&0xFF) - 3);
								break;
							case 0xF175:
								String v_configcode = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								if(v_configcode == null)
								{
									ShowProcessList(step_id + i,step_name,"获取电池配置码","",Common.Rnok);
									continue;
								}
								if(v_configcode.length() != ((cmd[0]&0xFF) - 3) * 2)
								{
									ShowProcessList(step_id + i,step_name,"电池配置码长度错误","",Common.Rnok);
									continue;
								}
								byte [] v_conbyte = new byte[v_configcode.length() / 2];
								int v_conlen = Commonfunc.StringToBytes(v_configcode,v_conbyte,v_conbyte.length);
								if(v_conlen == ((cmd[0]&0xFF) - 3)) //ok
									System.arraycopy(v_conbyte, 0, cmd, 4, ((cmd[0]&0xFF) - 3));
								else
								{
									ShowProcessList(step_id + i,step_name,"解析电池配置码:" + v_configcode,"",Common.Rnok);
									continue;
								}
								break;
							case 0xF200:
							case 0xF1C2: //电机编码是ASC码
								v_eccode = Common.cartype.getModuleStr("MCU", "ecode");
								if(v_eccode.length() != ((cmd[0]&0xFF) - 3))
								{
									ShowProcessList(step_id + i,step_name,"获取电机编码","",Common.Rnok);
									return;
								}
								v_codebyte = v_eccode.getBytes();
								System.arraycopy(v_codebyte, 0, cmd, 4, (cmd[0]&0xFF) - 3);
								break;

						}
					}

					//特殊处理多次发送命令
					if((cmd[1] == 0x22 && (cmd[2]&0xFF) == 0xF1 && cmd[3] == 0x70))
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 3);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					if(Pstep.getName().equals("PLG") && i > 12 && i < 16)
					{
						Sleep(50);
					}
					else
						Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
						else if(((cmd[1]&0xFF) + 0x40) != (Readbuf[1]&0xFF)) //异常
						{
							if(cmd[1] == 0x27) //安全认证
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,"异常回复:" + Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
								continue;
							}
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[2] == 0x64 && Readbuf[3] == 0x15) //lock
					{
						if(Readbuf[4] == 0x01)
							ShowProcessList(step_id + i,step_name,"解锁","",Common.Rok);
						else if(Readbuf[4] == 0x00)
							ShowProcessList(step_id + i,step_name,"上锁","",Common.Rok);
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:   //partnumber
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(Pstep.getName()) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), partnumber_cache,"supplierId");
								break;
							case 0xF191:
							case 0xF193:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "hardwareVersion");
								break;
							case 0xF1C0:
							case 0xF195:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), partnumber_cache,"softwareVersion");
								break;
							case 0xF18C:  //控制器序列号,校验格式，只能是数字字母
								v_show = new String(v_data);
								if("IHU".equals(Pstep.getName()))
									v_get = Common.cartype.getModuleStr(Pstep.getName(),partnumber_cache, "serialNumber");
								else
									v_get = "format";
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0x9010:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "01";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								if("PDCS".equals(Pstep.getName()))
								{
									v_show = ((v_data[2]&0xFF) * 0x100 + (v_data[3]&0xFF)) * 0.00333 + " %";
								}
								else
								{
									v_show = (v_data[0]&0xFF) + " %";
								}
								break;
							case 0xF100:  //国标码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "eolConfigCode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(Pstep.getName(), "ecode");
								break;
							case 0x5515:  //BTM
							case 0x5514:
							case 0x5511:
							case 0x5512:
							case 0x5513:
								v_show = new String(v_data);
								v_get = "OK";
								break;
						}
						v_show = v_show.replace(" ", "");
						v_show = v_show.trim();
						if(v_code == 0xF187 && Pstep.getName().equals("IHU"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_code == 0xF191 && Pstep.getName().equals("MFCP"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_get.equals("OK"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_get.equals("date"))
						{
							if((v_data[0] == 0x20) && (v_data[2] < 0x13) && (v_data[3] < 0x32))
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.equals("format")) //格式校验
						{
							if(Commonfunc.CheckSerialformat(v_data) == true)
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show ,"[Mes:空]",Common.Rnok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x51) //reset
					{
						Sleep(1500);
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
						if(Pstep.getName().equals("GW"))
						{
							t_com.CanSendOneOnly(0x7DF, cmddisdtc2, cmddisdtc2.length);
							Sleep(50);
							t_com.CanSendOneOnly(0x7DF, cmddisdtc, cmddisdtc.length);
							Sleep(50);
						}
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pstep.getName(),dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x6E) //写
					{
						byte [] v_data = new byte[cmd[0] - 3];
						System.arraycopy(cmd, 4, v_data, 0, cmd[0] - 3);
						String v_show = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								break;
							case 0xF100:
							case 0xF200:
								v_show = new String(v_data);
								break;
							case 0xF170:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								Sleep(500);
								break;
							default:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								break;
						}
						ShowProcessList(step_id + i,step_name, v_show,"",Common.Rok);
						Sleep(500);
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && ((Readbuf[3]&0xFF) == 0xFA) && ((Readbuf[4]&0xFF) == 0x10))
					{
						if((Readbuf[5]&0xFF) == 0x05 || (Readbuf[5]&0xFF) == 0x03 || (Readbuf[5]&0xFF) == 0x06) //wait
						{
							i --;
							Sleep(1000);
						}
						else if((Readbuf[5]&0xFF) == 0x04) //ok
						{
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name, "错误代码:" + (Readbuf[5]&0xFF),"",Common.Rnok);
							//break;
							v_plg_ok = 0;
						}
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && ((Readbuf[3]&0xFF) == 0xFA) && ((Readbuf[4]&0xFF) == 0x15))
					{
						if((Readbuf[5]&0xFF) == 0x02) //wait
						{
							i --;
							Sleep(1000);
						}
						else
						{
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						}
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x01) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(1000);
					}
					else if(Readbuf[1] == 0x71 && (Readbuf[2] == 0x03) && (Readbuf[3]&0xFF) == 0x31) //EPB Assembly Check
					{
						if(Readbuf[5] == 0x02) //Running
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x03) //完成
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context28),"",Common.Rok);
						}
						else
						{
							if(esc_a > 0)
							{
								i -= 2;
								esc_a --;
								ShowFuncTip("EPB Assembly Check重试,第:" + (3 - esc_a) + "次...");
								Sleep(500);
							}
							else
								ShowProcessList(step_id + i,step_name,"Error:" + Readbuf[5],"",Common.Rnok);
						}
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}

		private void fun_EP12_F0_DTC1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = 0;
			int v_res = 0;
			String step_t = null;
			String step_name = null;
			String v_module = null;
			String partnumber_cache = "";  //特殊处理
			byte [] Readbuf = new byte[512];
			int error = 0;
			int v_len = 0;
			SetCanInitTimer(10);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_name == null)
				{
					ShowProcessList(step_id + i,v_module,getString(R.string.msg_show_context66),"",Common.Rnok);
					continue;
				}
				//获取模块名字
				v_len = step_name.indexOf("-");
				v_module = step_name.substring(0,v_len);
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					//确认模块
					v_len = Common.cartype.getMap(v_module);
					if(v_len <= 0) continue;
					//得到CAN ID
					//v_module = v_module + v_len;
					v_req = Common.cartype.getModuleIdReq(v_module);
					v_res = Common.cartype.getModuleIdRes(v_module);
					Sleep(10);

					//特殊处理多次发送命令
					if("TBOX".equals(v_module))
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 2);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						AddModulePrintList(v_module,getString(R.string.msg_show_context24));
						continue;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						AddModulePrintList(v_module,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1));
						continue;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							AddModulePrintList(v_module,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1));
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,v_module,getString(R.string.msg_show_context58),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(v_module,dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,v_module,dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
								AddModulePrintList(v_module,dtclist.get(z).get("CONTEXT"));
							}
						}
						else
							ShowProcessList(step_id + i,v_module,getString(R.string.msg_show_context27),"",Common.Rok);
					} else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:   //partnumber
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(v_module) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(v_module,partnumber_cache, "partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module, partnumber_cache,"supplierId");
								break;
							case 0xF191:
							case 0xF193:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module,partnumber_cache, "hardwareVersion");
								break;
							case 0xF1D0:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module,partnumber_cache, "softwareAss");
								break;
							case 0xF1C0:
							case 0xF195:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module, partnumber_cache,"softwareVersion");
								break;
							case 0xF18C:  //控制器序列号,校验格式，只能是数字字母
								v_show = new String(v_data);
								if("IHU".equals(v_module))
									v_get = Common.cartype.getModuleStr(v_module,partnumber_cache, "serialNumber");
								else
									v_get = "format";
								break;
							case 0xF18B:  //生产日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(v_module, "manufactureDate");
								if(v_get.length() < 2)
									v_get = "date";
								break;
							case 0xF19D:  //装配日期
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "date";
								break;
							case 0x9010:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = "01";
								break;
							case 0xF170:  //配置码
							case 0xF110:
							case 0xF107:
							case 0x0007:
							case 0xC100:
							case 0xF101:
							case 0xF050:
							case 0xC110:
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(v_module, "eolConfigCode");
								break;
							case 0xF160:  //N95配置码
								if("PDCS".equals(v_module))
								{
									v_show = ((v_data[2]&0xFF) * 0x100 + (v_data[3]&0xFF)) * 0.00333 + " %";
								}
								else
								{
									v_show = (v_data[0]&0xFF) + " %";
								}
								break;
							case 0xF100:  //国标码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module, "ecode");
								break;
							case 0xF175:  //电池配置码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(v_module, "eolConfigCode");
								break;
							case 0xF200:  //驱动电机编码
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(v_module, "ecode");
								break;
							case 0xF1C2:  //电机编码MCU
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr("MCU", "ecode");
								break;
							case 0xF019:  //读EOL-Coding码
								v_show = Commonfunc.bytesToHexStringP(v_data, 0, v_data.length);
								v_get = Common.cartype.getModuleStr(v_module, "ecode");
								break;
							case 0x5515:  //BTM
							case 0x5514:
							case 0x5511:
							case 0x5512:
							case 0x5513:
								v_show = new String(v_data);
								v_get = "OK";
								break;
						}
						v_show = v_show.replace(" ", "");
						v_show = v_show.trim();
						if(v_code == 0xF187 && v_module.equals("IHU"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_code == 0xF191 && v_module.equals("MFCP"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_get.equals("OK"))
						{
							ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
						}
						else if(v_get.equals("date"))
						{
							if((v_data[0] == 0x20) && (v_data[2] < 0x13) && (v_data[3] < 0x32))
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.equals("format")) //格式校验
						{
							if(Commonfunc.CheckSerialformat(v_data) == true)
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
						}
						else if(v_get.length() > 0)
						{
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else
								ShowProcessList(step_id + i,step_name,v_show ,v_get,Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name,v_show ,"[Mes:null]",Common.Rnok);
					}
				}
			}
		}
		private void fun_S30_F0_DTC1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = 0;
			int v_res = 0;
			String step_t = null;
			String step_name = null;
			String v_module = null;
			byte [] Readbuf = new byte[512];
			int error = 0;
			int v_len = 0;
			SetCanInitTimer(10);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				//获取模块名字
				v_len = step_name.indexOf("-");
				v_module = step_name.substring(0,v_len);
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					//确认模块
					v_len = Common.cartype.getMap(v_module);
					if(v_len <= 0) continue;
					//得到CAN ID
					//v_module = v_module + v_len;
					v_req = Common.cartype.getModuleIdReq(v_module);
					v_res = Common.cartype.getModuleIdRes(v_module);
					Sleep(10);

					//特殊处理多次发送命令
					if("TBOX".equals(v_module))
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 2);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						AddModulePrintList(v_module,getString(R.string.msg_show_context24));
						continue;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						AddModulePrintList(v_module,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1));
						continue;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							AddModulePrintList(v_module,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1));
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,v_module,getString(R.string.msg_show_context58),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(v_module,dtc,dtclist,3,(byte)0x08);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,v_module,dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
								AddModulePrintList(v_module,dtclist.get(z).get("CONTEXT"));
							}
						}
						else
							ShowProcessList(step_id + i,v_module,getString(R.string.msg_show_context27),"",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F1_EcuFlash(Profile.Section Pstep,String Pname)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pname);
			int v_res = Common.cartype.getModuleIdRes(Pname);
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdextd = new byte[] {0x02,0x10,0x03};
			byte [] cmdendtc = new byte[] {0x02,(byte) 0x85,0x02};
			byte [] cmdendtc2 = new byte[] {0x03,0x28,0x03,0x03};
			byte [] cmddisdtc = new byte[] {0x02,(byte) 0x85,0x01};
			byte [] cmddisdtc2 = new byte[] {0x03,0x28,0x00,0x03};
			String partnumber_cache = "";  //特殊处理
			int v_maxlen = 0;
			short v_crc = (short) 0xFFFF;
			if(Pname.equals("WPC"))
				SetCanInitTimer(5);
			else if(Pname.equals("EHB"))
				SetCanInitTimer(1);
			else
				SetCanInitTimer(0);
			//先解析刷写数据
			String v_app_file = "";
			String v_dri_file = "";
			byte [] appbuf = null;
			//int v_applen = 0;
			byte [] dribuf = null;
			//int v_drilen = 0;
			byte [] v_dri_adr = new byte[256];
			byte [] v_dri_len = new byte[4];
			int [] v_dri_block = new int[10];
			int v_drilength = 0;
			byte [] v_app_adr = new byte[512];
			byte [] v_app_len = new byte[4];
			int [] v_app_block = new int[100];
			int v_applength = 0;
			int v_down_block = 0; //已经下载的块数
			int v_down_length = 0;
			int v_app_down = 0;
			//driver
			int v_dri_down_block = 0;
			int v_dri_down_length = 0;
			int v_dri_down = 0;
			String mcu_padrnumber = "";  //MCU特殊处理
			int use_old_check = 0;

			//byte [] cmdlink = new byte[]{0x02,0x3E,0x00};
			int error = 0;
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					ShowFuncTip(step_name + "...");
					//---------------------发送前处理---------------------
					//解析下载文件
					if(i == 5)
					{
						//先解析driver文件
						ShowFuncTip("解析Driver文件...");
						String v_drivername = Common.cartype.getModuleStr(Pname, partnumber_cache,"driverFileName");
						//if(Pname.equals("GW"))
						//{
						//	v_drivername = "Flash_Drv.s19";
						//}
						v_dri_file = Common.Dir + "HOZON/S30/ECU/" + Pname + "/" +  v_drivername;
						if((v_drivername != null) && (v_drivername.length() > 5))
						{
							if(".s19".equals(v_drivername.substring(v_drivername.length() - 4)) || ".S19".equals(v_drivername.substring(v_drivername.length() - 4)))
								try {
									v_drilength = Commonfunc.GetS19FileLength(v_dri_file,v_dri_adr);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i,"打开Driver文件",v_drivername,"",Common.Rnok);
									break;
								}
							else //hex
							{
								try {
									v_drilength = Commonfunc.GetHexFileLength(v_dri_file, v_dri_adr);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i,"打开Driver文件",v_drivername,"",Common.Rnok);
									break;
								}
							}
							//
							dribuf = new byte[v_drilength + 10];
							if(".s19".equals(v_drivername.substring(v_drivername.length() - 4)) || ".S19".equals(v_drivername.substring(v_drivername.length() - 4)))
								try {
									v_drilength = Commonfunc.ReadS19File(v_dri_file, dribuf,0,v_dri_block);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i,"解析Driver文件",v_drivername,"",Common.Rnok);
									break;
								}
							else  //hex
							{
								try {
									v_drilength = Commonfunc.ReadHexFile(v_dri_file, dribuf, v_dri_block);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i,"解析Driver文件",v_drivername,"",Common.Rnok);
									break;
								}
							}
							if(v_drilength <= 10)
							{
								ShowProcessList(step_id + i,"Driver数据解析",v_drivername,"",Common.Rnok);
								break;
							}
						}
						if(Pname.equals("BDCM"))
						{
							if(v_drilength <= 10)
							{
								ShowProcessList(step_id + i,"Driver数据解析",v_drivername,"",Common.Rnok);
								break;
							}
						}
						ShowFuncTip("解析app文件...");
						String v_appname = Common.cartype.getModuleStr(Pname,partnumber_cache, "appFileName");
						v_app_file = Common.Dir + "HOZON/S30/ECU/" + Pname + "/" +  v_appname;
						if((v_appname != null) && (v_appname.length() > 5))
						{
							if(".s19".equals(v_appname.substring(v_appname.length() - 4)) || ".S19".equals(v_appname.substring(v_appname.length() - 4)))
								try {
									v_applength = Commonfunc.GetS19FileLength(v_app_file,v_app_adr);
									if(Pname.equals("CLM")) //需要刷标定文件
									{
										String v_cal_name = Common.cartype.getModuleStr(Pname, partnumber_cache,"calFileName");
										String v_cal_file = Common.Dir + "HOZON/S30/ECU/" + Pname + "/" +  v_cal_name;
										byte [] v_cal_adr = new byte[128];
										int cal_len = Commonfunc.GetS19FileLength(v_cal_file,v_cal_adr);
										if(cal_len > 0)
										{
											v_applength += cal_len;
											System.arraycopy(v_cal_adr, 1, v_app_adr, 1 + (v_app_adr[0]&0xFF) * 4, (v_cal_adr[0]&0xFF) * 4);
											v_app_adr[0] += v_cal_adr[0];
										}
									}
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_applength = 0;
									ShowProcessList(step_id + i,"打开app文件",v_appname,"",Common.Rnok);
									break;
								}
							else //hex
							{
								try {
									v_applength = Commonfunc.GetHexFileLength(v_app_file, v_app_adr);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_applength = 0;
									ShowProcessList(step_id + i,"打开app文件",v_appname,"",Common.Rnok);
									break;
								}
							}
							//
							appbuf = new byte[v_applength + 10];
							if(".s19".equals(v_appname.substring(v_appname.length() - 4)) || ".S19".equals(v_appname.substring(v_appname.length() - 4)))
								try {
									v_applength = Commonfunc.ReadS19File(v_app_file, appbuf,0, v_app_block);
									if(Pname.equals("CLM")) //需要刷标定文件
									{
										String v_cal_name = Common.cartype.getModuleStr(Pname, partnumber_cache,"calFileName");
										String v_cal_file = Common.Dir + "HOZON/S30/ECU/" + Pname + "/" +  v_cal_name;
										int [] v_cal_block = new int[32];
										int caldata_len = Commonfunc.ReadS19File(v_cal_file,appbuf,v_applength,v_cal_block);
										if(caldata_len > 0)
										{
											v_applength = caldata_len;
											for(int ssk = v_app_block[0]; ssk < (v_app_block[0] + v_cal_block[0]); ssk ++)
												v_app_block[1 + ssk] = v_cal_block[ssk + 1 - v_app_block[0]];
											v_app_block[0] += v_cal_block[0];
										}
									}
									/*if(Pname.equals("GW"))//GW特殊情况20210916
									{
										v_applength = 0x080000;
										v_app_block[1] = 0x080000;
									}
									*/
									v_crc = Commonfunc.s30_CalcCRC(v_applength, appbuf, v_crc);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									ShowProcessList(step_id + i,"解析app文件",v_appname,"",Common.Rnok);
									break;
								}
							else  //hex
							{
								try {
									v_applength = Commonfunc.ReadHexFile(v_app_file, appbuf, v_app_block);
									v_crc = Commonfunc.s30_CalcCRC(v_applength, appbuf, v_crc);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									ShowProcessList(step_id + i,"解析app文件",v_appname,"",Common.Rnok);
									break;
								}
							}
							if(v_applength < 100)
							{
								ShowProcessList(step_id + i,"App数据解析","失败","",Common.Rnok);
								break;
							}
						}

					}
					else if(i == 6) //关闭DTC
					{
						t_com.CanSendOneOnly(0x7DF, cmdextd, cmdextd.length);
						Sleep(50);
						t_com.CanSendOneOnly(0x7DF, cmdendtc, cmdendtc.length);
						Sleep(50);
						t_com.CanSendOneOnly(0x7DF, cmdendtc2, cmdendtc2.length);
						Sleep(50);
					}
					else if(i == 27) //打开DTC
					{
						t_com.CanSendOneOnly(0x7DF, cmddisdtc2, cmddisdtc2.length);
						Sleep(50);
						t_com.CanSendOneOnly(0x7DF, cmddisdtc, cmddisdtc.length);
						Sleep(50);
					}
					else if(cmd[1] == 0x27 && cmd[2] == 0x12) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hozon_S30_safekey11(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}
					else if(cmd[1] == 0x2E) //写
					{
						int v_code = (cmd[2]&0xFF) * 0x100 + (cmd[3]&0xFF);
						switch(v_code)
						{
							case 0xF199:
								System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
								break;
						}
					}
					else if(i == 13) //开始下载驱动
					{
						if(v_drilength < 10) //无需下载驱动
						{
							i += 2;
							continue;
						}
						else //下载驱动,驱动暂时按单块处理,FLR也是多块
						{
							if(v_dri_down_block < v_dri_block[0])
							{
								System.arraycopy(v_dri_adr, 1 + 4 * v_dri_down_block, cmd, 4, 4);
								v_dri_len[0] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 24) & 0xFF);
								v_dri_len[1] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 16) & 0xFF);
								v_dri_len[2] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 8) & 0xFF);
								v_dri_len[3] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 0) & 0xFF);
								v_dri_down_length += v_dri_block[1 + v_dri_down_block];
								System.arraycopy(v_dri_len, 0, cmd, 8, 4);
							}
						}
					}
					else if(i == 14) //下载驱动
					{
						byte [] cmd_down = new byte[v_maxlen + 2];
						int v_len = 0;
						int v_start = 0;
						int v_frame = 1;
						while(v_dri_down < v_dri_down_length)
						{
							if((v_dri_down + v_maxlen - 2) >= v_dri_down_length) //last
							{
								v_len = v_dri_down_length - v_dri_down;
							}
							else
							{
								v_len = v_maxlen - 2;
							}
							if((v_len + 2) >= 0x100)
							{
								v_start = 1;
								cmd_down[0] = (byte) ((v_len + 2) / 0x100);
							}
							else
								v_start = 0;
							//拷贝数据
							cmd_down[v_start ++] = (byte) ((v_len + 2) % 0x100);
							cmd_down[v_start ++] = 0x36;
							cmd_down[v_start ++] = (byte)(v_frame ++);
							System.arraycopy(dribuf, v_dri_down, cmd_down, v_start, v_len);

							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_down,v_len+v_start, Readbuf, 5003, Common.cmdmaxtime, 2);
							if((error > 100) || ((Readbuf[1]&0xFF) == 0x7F))//异常
							{
								break;
							}
							v_dri_down += v_len;
							ShowFuncTip("下载数据:" + v_drilength + "\\" +  v_dri_down);
						}
						v_dri_down_block ++;
					}
					else if(i == 16) //擦除APP,按多个块处理
					{
						if(Pname.equals("GW"))//GW特殊情况20220516
						{
							if(v_down_block >1)
							{
								v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
								v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								v_down_length += v_app_block[1 + v_down_block];
								continue;
							}
							else
							{
								System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 5, 4);
								v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
								v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								v_down_length += v_app_block[1 + v_down_block];
								byte []  v_app_lentemp = new byte[4];
								v_app_lentemp[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_lentemp[1] = (byte) (v_app_len[1] + 0x01);
								v_app_lentemp[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_lentemp[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								System.arraycopy(v_app_lentemp, 0, cmd, 9, 4);
							}
						}
						else if(v_down_block < v_app_block[0])
						{
							System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 5, 4);
							v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
							v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
							v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
							v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
							v_down_length += v_app_block[1 + v_down_block];
							System.arraycopy(v_app_len, 0, cmd, 9, 4);
						}
					}
					else if(i == 17)
					{
						if(Pname.equals("EHB") || Pname.equals("LSA") || Pname.equals("MCU") || Pname.equals("APA") || Pname.equals("EGSM") ||
								Pname.equals("WPC")	|| Pname.equals("MFCP")	|| Pname.equals("PLG") || Pname.equals("OBC") || Pname.equals("EPS") ||
								Pname.equals("PTC") || Pname.equals("BMS")|| Pname.equals("GW")|| Pname.equals("BTM"))
							continue;
					}
					else if(i == 18) //请求下载APP
					{
						if(v_down_block < v_app_block[0])
						{
							System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 4, 4);
							System.arraycopy(v_app_len, 0, cmd, 8, 4);
						}
					}
					else if(i == 19) //下载数据
					{
						byte [] cmd_down = new byte[v_maxlen + 2];
						int v_len = 0;
						int v_start = 0;
						int v_frame = 1;
						while(v_app_down < v_down_length)
						{
							if((v_app_down + v_maxlen - 2) >= v_down_length) //last
							{
								v_len = v_down_length - v_app_down;
							}
							else
							{
								v_len = v_maxlen - 2;
							}
							if((v_len + 2) >= 0x100)
							{
								v_start = 1;
								cmd_down[0] = (byte) ((v_len + 2) / 0x100);
							}
							else
								v_start = 0;
							//拷贝数据
							cmd_down[v_start ++] = (byte) ((v_len + 2) % 0x100);
							cmd_down[v_start ++] = 0x36;
							cmd_down[v_start ++] = (byte)(v_frame ++);
							System.arraycopy(appbuf, v_app_down, cmd_down, v_start, v_len);

							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_down,v_len+v_start, Readbuf, 5003, Common.cmdmaxtime, 2);
							if((error > 100) || ((Readbuf[1]&0xFF) == 0x7F))//异常
							{
								break;
							}
							v_app_down += v_len;
							ShowFuncTip("下载数据:" + v_applength + "\\" +  v_app_down);
						}
						v_down_block ++;
					}
					else if(i == 21) //crc
					{
						//MCU特殊处理,重复刷写的问题20211113
						if(Pname.equals("MCU") && mcu_padrnumber.length() < 2) //没读到零件号
						{
							mcu_padrnumber = Common.cartype.getModuleStr(Pname, "partNumber");
						}
						if(Pname.equals("BDCM") || Pname.equals("FLC")|| Pname.equals("EACP")  ||
								(Pname.equals("MCU") && (mcu_padrnumber.equals("S30-2103020AF") || mcu_padrnumber.equals("S30-2103020")))
								|| Pname.equals("FLR") || (use_old_check == 1))
						{
							cmd[0] = 0x06;
							cmd[5] = (byte) ((v_crc&0xFFFF) / 0x100);
							cmd[6] = (byte) ((v_crc&0xFFFF) % 0x100);
							cmdlen = 7;
						}
						else
						{
							System.arraycopy(v_app_adr, 1, cmd, 5, 4);
							System.arraycopy(v_app_len, 0, cmd, 9, 4);
							cmd[13] = (byte) ((v_crc&0xFFFF) / 0x100);
							cmd[14] = (byte) ((v_crc&0xFFFF) % 0x100);
						}
					}
					else if(i == 22)
					{
						if(Pname.equals("PLG"))
						{
							continue;
						}
					}
					else if(i == 23)
					{
						if(Pname.equals("BTM"))
							break;
					}
					else if(i == 27)
					{
						if(Pname.equals("OBC"))
							break;
					}

					//特殊处理多次发送命令
					if(i == 14 || i == 19)
					{

					}
					else if(i < 8)
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					else if(i == 16)
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 10, 3);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						if(i > 8)
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
							break;
						}
						else
							continue;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						if(i > 8)
						{
							if(i == 21 && Readbuf[3] == 0x13)
							{
								i --;
								use_old_check = 1;
								continue;
							}
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							return;
						}
						else
							continue;
					}
					else if(error < 500) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F && i > 7) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							break;
						}
						else if((Readbuf[1]&0xFF) == 0x7F && i < 8)
							continue;
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if((Readbuf[1] == 0x62) && ((Readbuf[2]&0xFF) == 0xF1) && (((Readbuf[3]&0xFF) == 0x88) || ((Readbuf[3]&0xFF) == 0xC0)))
					{
						byte [] v_soft = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_soft, 0, Readbuf[0] - 3);
						String v_softver = new String(v_soft);
						String v_getsoft = Common.cartype.getModuleStr(Pname,partnumber_cache, "appFileVersionLatest");
						if(i == 3) //不一致才继续
						{
							if(v_softver.equals(v_getsoft))
							{
								ShowProcessList(step_id + i,step_name,v_softver,"",Common.Rok);
								if(Common.cartype.OptMode == 0)
									break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,v_softver+ "[" + v_getsoft + "]","",Common.Rok);
							}
						}
						else if(i == 28)
						{
							if(v_softver.equals(v_getsoft))
							{
								ShowProcessList(step_id + i,step_name,v_softver,"",Common.Rok);
							}
							else if(v_getsoft.length() > 0)
							{
								ShowProcessList(step_id + i,step_name,v_softver+ "[" + v_getsoft + "]","",Common.Rnok);
							}
							else
								ShowProcessList(step_id + i,step_name,v_softver,"",Common.Rnull);
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(Pname) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname,partnumber_cache, "hardwareVersion");
								break;
							case 0xF18C:
								v_show = new String(v_data);
								v_get = "";
								break;
							case 0xF1C0:
							case 0xF188:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"softwareVersion");
								break;
						}
						v_show = v_show.replace(" ", "");
						v_show = v_show.trim();
						if(Pname.equals("MCU") && v_code == 0xF187)
						{
							mcu_padrnumber = v_show;
						}
						else if(Pname.equals("VCU") && v_code == 0xF191)
						{
							v_get = "";
						}
						if(v_get.length() > 0)
						{
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rok);
							else if(Common.cartype.OptMode == 1)
								ShowProcessList(step_id + i,step_name,v_show + "[" + v_get + "]","",Common.Rnull);
							else
							{
								ShowProcessList(step_id + i,step_name,v_show + "[" + v_get + "]","",Common.Rnok);
								break;
							}
						}
						else
						{
							if(Common.cartype.OptMode == 1)
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
							else
							{
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
								break;
							}
						}
					}
					else if(i == 15) //结束下载driver
					{
						if(v_dri_down_block < v_dri_block[0]) //多块处理
						{
							i = 12;continue;
						}
					}
					else if(i == 20) //结束下载app
					{
						if(v_down_block < v_app_block[0]) //多块处理
						{
							i = 15;continue;
						}
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pname + "1",dtc,dtclist,3,(byte)0x01);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x11) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x74)
					{
						v_maxlen = (Readbuf[3]& 0xFF) * 0x100 + (Readbuf[4]& 0xFF);
					}
					else if(Readbuf[1] == 14)
					{
						ShowProcessList(step_id + i,step_name, "块:" + v_dri_down_block,"",Common.Rok);
					}
					else if(Readbuf[1] == 19)
					{
						ShowProcessList(step_id + i,step_name, "块:" + v_down_block,"",Common.Rok);
					}
					else if(Readbuf[1] == 0x51)
					{
						Sleep(1000);
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && Readbuf[4] == 0x00)
					{
						if(Readbuf[5] == 0x03)
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x02)
						{
							ShowProcessList(step_id + i,step_name,"","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"" + (Readbuf[5] & 0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x01)
					{
						if((i == 21) && Pname.equals("BDCM"))
						{
							if(Readbuf[5] == 0x02) //ok
								ShowProcessList(step_id + i,step_name, "","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name, "","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(1000);
						if((i == 21) && Pname.equals("GW"))
						{
							byte [] cmd_gw = {0x04,0x31,0x01,(byte) 0xDF,(byte) 0xFF};
							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_gw,cmd_gw.length, Readbuf, 5003, Common.cmdmaxtime, 3);
							Sleep(300);
						}
					}
					else if(Readbuf[1] == 0x51)
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(1000);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}

		private void fun_EP12_F1_EcuFlash(Profile.Section Pstep,String Pname)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq(Pname);
			int v_res = Common.cartype.getModuleIdRes(Pname);
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdextd = new byte[] {0x02,0x10,0x03};
			byte [] cmdendtc = new byte[] {0x02,(byte) 0x85,0x02};
			byte [] cmdendtc2 = new byte[] {0x03,0x28,0x03,0x03};
			byte [] cmddisdtc = new byte[] {0x02,(byte) 0x85,0x01};
			byte [] cmddisdtc2 = new byte[] {0x03,0x28,0x00,0x03};
			byte [] cmdlink = {0x02,0x3E, (byte) 0x80};
			String partnumber_cache = "";  //特殊处理
			int v_maxlen = 0;
			short v_crc = (short) 0xFFFF;
			if(Pname.equals("EPS"))
				SetCanInitTimer(5);
			else if (Pname.equals("BDCM")||Pname.equals("EHB")||Pname.equals("OBC")||Pname.equals("OBCe")){
				SetCanInitTimer(1);
			}
			else
				SetCanInitTimer(0);
			//先解析刷写数据
			String v_app_file = "";
			String v_dri_file = "";
			byte [] appbuf = null;
			byte [] dribuf = null;
			byte [] v_dri_adr = new byte[256];
			byte [] v_dri_len = new byte[4];
			int [] v_dri_block = new int[10];
			int v_drilength = 0;
			byte [] v_app_adr = new byte[512];
			byte [] v_app_len = new byte[4];
			int [] v_app_block = new int[100];
			int v_applength = 0;
			int v_down_block = 0; //已经下载的块数
			int v_down_length = 0;
			int v_app_down = 0;
			//driver
			int v_dri_down_block = 0;
			int v_dri_down_length = 0;
			int v_dri_down = 0;
			String mcu_padrnumber = "";  //MCU特殊处理
			int use_old_check = 0;
			int error = 0;
			long starttime = 0;
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					ShowFuncTip(step_name + "...");
					//---------------------发送前处理---------------------
					//解析下载文件
					if(i == 5)
					{
						if((Common.cartype.getModuleNum(Pname) > 1)&&("".equals(partnumber_cache))&&(Common.cartype.OptMode==1)) //多模块
						{
							Common.PartNumber="";
							Common.SelectPartNumberList = null;
							//当多模块且读不出零件号时  弹窗选择
							List<String> partNumbers = Common.cartype.getModuleStrALL(Pname,"partNumber");
							System.out.println(partNumbers);
							Common.SelectPartNumberList = partNumbers;
							//弹窗进行选择
							Common.wait = true;
							m_handler.obtainMessage(Message.MSG_Select_Partnumber, 0, 0,"").sendToTarget();
							while (Common.wait){
								Sleep(1000);
							}
							partnumber_cache = Common.PartNumber;
						}
						System.out.println(partnumber_cache);

						//先解析driver文件
						ShowFuncTip(getString(R.string.msg_show_context59) + "...");
						String v_drivername = Common.cartype.getModuleStr(Pname, partnumber_cache,"driverFileName");
						t_com.PrintLog("DriverName:" + v_drivername);
						v_dri_file = Common.Dir + "HOZON/"+Common.cartype.car+"/ECU/"+ Pname + "/" +  v_drivername;
						t_com.PrintLog("Driver:" + v_dri_file);
						if (((v_drivername != null) && (v_drivername.length() > 5))&&((".s19".equals(v_drivername.substring(v_drivername.length() - 4))||".S19".equals(v_drivername.substring(v_drivername.length() - 4))||".hex".equals(v_drivername.substring(v_drivername.length() - 4)))==false))break;
						// 下载文件判断
						int a=fileExists(v_dri_file);
						if((v_drivername != null) && (v_drivername.length() > 5)&&a!=1){ //下载文件
							String param = "path="+v_drivername;
							t_com.PrintLog("download:" + param);
							t_com.PrintLog("Url:" + Common.Web_Url_Networ_Disk_File);
							HttpRequest.sendGetBufferedReader(Common.Web_Url_Networ_Disk_File,param,v_dri_file);
						}
						if((v_drivername != null) && (v_drivername.length() > 5)) {
							if (".s19".equals(v_drivername.substring(v_drivername.length() - 4)) || ".S19".equals(v_drivername.substring(v_drivername.length() - 4)))
							{
								try {
									v_drilength = Commonfunc.GetS19FileLength(v_dri_file, v_dri_adr);
								} catch (IOException e) {
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i, getString(R.string.msg_show_context60), v_drivername,"", Common.Rnok);
									break;
								}
							} else //hex
							{
								try {
									v_drilength = Commonfunc.GetHexFileLength(v_dri_file, v_dri_adr);
								} catch (IOException e) {
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i, getString(R.string.msg_show_context60), v_drivername,"", Common.Rnok);
									break;
								}
							}
							dribuf = new byte[v_drilength + 10];
							if (".s19".equals(v_drivername.substring(v_drivername.length() - 4)) || ".S19".equals(v_drivername.substring(v_drivername.length() - 4)))
							{
								try {
									v_drilength = Commonfunc.ReadS19File(v_dri_file, dribuf, 0, v_dri_block);
								} catch (IOException e) {
									e.printStackTrace();
									v_drilength = 0;
									ShowProcessList(step_id + i, getString(R.string.msg_show_context59), v_drivername,"", Common.Rnok);
									break;
								}
							} else if(".hex".equals(v_drivername.substring(v_drivername.length() - 4)))  //hex
								{
									try {
										v_drilength = Commonfunc.ReadHexFile(v_dri_file, dribuf, v_dri_block);
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										v_drilength = 0;
										ShowProcessList(step_id + i,getString(R.string.msg_show_context59),v_drivername,"",Common.Rnok);
										break;
									}
								}
							if(v_drilength <= 10)
							{
								ShowProcessList(step_id + i,getString(R.string.msg_show_context59),v_drivername,"",Common.Rnok);
								break;
							}
							else
								t_com.PrintLog("Driver File length=" + v_drilength);
						}
						else
						{
							t_com.PrintLog("No DriverFile,or file length too low");
						}
						if(Pname.equals("BDCM")||Pname.equals("BMS")||Pname.equals("EPB")){
							if(v_drilength <= 10)
							{
								ShowProcessList(step_id + i,getString(R.string.msg_show_context59),v_drivername,"",Common.Rnok);
								break;
							}
						}
						ShowFuncTip(getString(R.string.msg_show_context61) + "...");
						String v_appname = Common.cartype.getModuleStr(Pname,partnumber_cache, "appFileName");
						t_com.PrintLog("App file name:" + v_appname);
						v_app_file = Common.Dir + "HOZON/"+Common.cartype.car+"/ECU/"+ Pname + "/" +  v_appname;
						t_com.PrintLog("app:" + v_dri_file);
						if (((v_appname != null) && (v_appname.length() > 5))&&((".s19".equals(v_appname.substring(v_appname.length() - 4))||".S19".equals(v_appname.substring(v_appname.length() - 4))||".hex".equals(v_appname.substring(v_appname.length() - 4)))==false))break;
						// 下载文件判断
						int b=fileExists(v_app_file);
						if((v_appname != null) && (v_appname.length() > 5)&&b!=1){ //下载文件
							String param = "path="+v_appname;
							t_com.PrintLog("download:" + param);
							HttpRequest.sendGetBufferedReader(Common.Web_Url_Networ_Disk_File,param,v_app_file);
						}
						if((v_appname != null) && (v_appname.length() > 5)) {
							if (".s19".equals(v_appname.substring(v_appname.length() - 4)) || ".S19".equals(v_appname.substring(v_appname.length() - 4)))
							{
								try {
									v_applength = Commonfunc.GetS19FileLength(v_app_file, v_app_adr);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_applength = 0;
									ShowProcessList(step_id + i, getString(R.string.msg_show_context62), v_appname,"", Common.Rnok);
									break;
								}
						}
							else //hex
							{
								try {
									v_applength = Commonfunc.GetHexFileLength(v_app_file, v_app_adr);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									v_applength = 0;
									ShowProcessList(step_id + i, getString(R.string.msg_show_context62), v_appname,"", Common.Rnok);
									break;
								}
							}
							//
							appbuf = new byte[v_applength + 10];
							if (".s19".equals(v_appname.substring(v_appname.length() - 4)) || ".S19".equals(v_appname.substring(v_appname.length() - 4)))
							{
								try {
									v_applength = Commonfunc.ReadS19File(v_app_file, appbuf, 0, v_app_block);
									v_crc = Commonfunc.s30_CalcCRC(v_applength, appbuf, v_crc);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									ShowProcessList(step_id + i, getString(R.string.msg_show_context61), v_appname,"", Common.Rnok);
									break;
								}
						}
							else  //hex
							{
								try {
									v_applength = Commonfunc.ReadHexFile(v_app_file, appbuf, v_app_block);
									v_crc = Commonfunc.s30_CalcCRC(v_applength, appbuf, v_crc);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									ShowProcessList(step_id + i,getString(R.string.msg_show_context61),v_appname,"",Common.Rnok);
									break;
								}
							}
							if(v_applength < 100)
							{
								ShowProcessList(step_id + i,getString(R.string.msg_show_context61),"Fail","",Common.Rnok);
								break;
							}
						}
						else
						{
							t_com.PrintLog("No appfile,or file length too low");
						}
					}
					else if(i == 6) //关闭DTC
					{
						t_com.CanSendOneOnly(0x7DF, cmdextd, cmdextd.length);
						Sleep(800);
						t_com.CanSendOneOnly(0x7DF, cmdendtc, cmdendtc.length);
						Sleep(200);
						t_com.CanSendOneOnly(0x7DF, cmdendtc2, cmdendtc2.length);
						Sleep(200);
					}
					else if(i == 25) //打开DTC
					{
						t_com.CanSendOneOnly(0x7DF, cmddisdtc2, cmddisdtc2.length);
						Sleep(50);
						t_com.CanSendOneOnly(0x7DF, cmddisdtc, cmddisdtc.length);
						Sleep(50);
					}
					else if(cmd[1] == 0x27 && cmd[2] == 0x12) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hozon_S30_safekey11(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
						starttime = System.currentTimeMillis(); //获得起始时间
					}
					else if(cmd[1] == 0x2E) //写
					{
						int v_code = (cmd[2]&0xFF) * 0x100 + (cmd[3]&0xFF);
						switch(v_code)
						{
							case 0xF199:
								System.arraycopy(Common.cartype.time, 0, cmd, 4, cmd[0] - 3);
								break;
						}
					}
					else if(i == 12) //开始下载驱动
					{
						if(v_drilength < 10) //无需下载驱动
						{
							i += 2;
							continue;
						}
						else //下载驱动,驱动暂时按单块处理,FLR也是多块
						{
							t_com.PrintLog("dri_block num=" + v_dri_block[0] + "block = " + v_dri_down_block);
							if(v_dri_down_block < v_dri_block[0])
							{
								System.arraycopy(v_dri_adr, 1 + 4 * v_dri_down_block, cmd, 4, 4);
								v_dri_len[0] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 24) & 0xFF);
								v_dri_len[1] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 16) & 0xFF);
								v_dri_len[2] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 8) & 0xFF);
								v_dri_len[3] = (byte) ((v_dri_block[1 + v_dri_down_block] >>> 0) & 0xFF);
								v_dri_down_length += v_dri_block[1 + v_dri_down_block];
								System.arraycopy(v_dri_len, 0, cmd, 8, 4);
								t_com.PrintLog("dri_block len:" + Commonfunc.bytesToHexString(v_dri_len,0,4));
							}
						}
					}
					else if(i == 13) //下载驱动
					{
						byte [] cmd_down = new byte[v_maxlen + 2];
						int v_len = 0;
						int v_start = 0;
						int v_frame = 1;
						while(v_dri_down < v_dri_down_length)
						{
							if((v_dri_down + v_maxlen - 2) >= v_dri_down_length) //last
							{
								v_len = v_dri_down_length - v_dri_down;
							}
							else
							{
								v_len = v_maxlen - 2;
							}
							if((v_len + 2) >= 0x100)
							{
								v_start = 1;
								cmd_down[0] = (byte) ((v_len + 2) / 0x100);
							}
							else
								v_start = 0;
							//拷贝数据
							cmd_down[v_start ++] = (byte) ((v_len + 2) % 0x100);
							cmd_down[v_start ++] = 0x36;
							cmd_down[v_start ++] = (byte)(v_frame ++);
							System.arraycopy(dribuf, v_dri_down, cmd_down, v_start, v_len);

							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_down,v_len+v_start, Readbuf, 5003, Common.cmdmaxtime, 2);
							if((error > 100) || ((Readbuf[1]&0xFF) == 0x7F))//异常
							{
								break;
							}
							Sleep(2);
							t_com.CanSendOneOnly(0x7DF,cmdlink,3);
							v_dri_down += v_len;
							ShowFuncTip("Downloading:" + v_drilength + "\\" +  v_dri_down);
						}
						v_dri_down_block ++;
					}
					else if(i == 15) //擦除APP,按多个块处理
					{
						if(Pname.equals("OBC"))//GW特殊情况20220516
						{
							if(v_down_block >1)
							{
								v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
								v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								v_down_length += v_app_block[1 + v_down_block];
								continue;
							}
							else
							{
								System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 5, 4);
								v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
								v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								v_down_length += v_app_block[1 + v_down_block];
								byte []  v_app_lentemp = new byte[4];
								v_app_lentemp[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
								v_app_lentemp[1] = (byte) (v_app_len[1] + 0x01);
								v_app_lentemp[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
								v_app_lentemp[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
								System.arraycopy(v_app_lentemp, 0, cmd, 9, 4);
							}
						}
						else if(v_down_block < v_app_block[0])
						{
							System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 5, 4);
							v_app_len[0] = (byte) ((v_app_block[1 + v_down_block] >>> 24) & 0xFF);
							v_app_len[1] = (byte) ((v_app_block[1 + v_down_block] >>> 16) & 0xFF);
							v_app_len[2] = (byte) ((v_app_block[1 + v_down_block] >>> 8) & 0xFF);
							v_app_len[3] = (byte) ((v_app_block[1 + v_down_block] >>> 0) & 0xFF);
							v_down_length += v_app_block[1 + v_down_block];
							System.arraycopy(v_app_len, 0, cmd, 9, 4);
						}
//						System.out.println(v_applength);
					}
					else if(i == 16)
					{
						if(Pname.equals("EHB") || Pname.equals("CLM")|| Pname.equals("LSA") || Pname.equals("MCU") || Pname.equals("APA") || Pname.equals("EGSM") ||
							Pname.equals("OBC")	||Pname.equals("WPC")	|| Pname.equals("MFCP")	|| Pname.equals("PLG") || Pname.equals("EPS") ||
								Pname.equals("PTC") || Pname.equals("BMS")|| Pname.equals("GW")|| Pname.equals("BTM")||Pname.equals("ESC")||Pname.equals("EPB")||
								Pname.equals("OBCe") || Pname.equals("ICU")|| Pname.equals("EPBi"))
							continue;
					}
					else if(i == 17) //请求下载APP
					{
						if(v_applength < 10) //无需下载app
						{
							i += 2;
							continue;
						} else if (v_down_block < v_app_block[0])
						{
							System.arraycopy(v_app_adr, 1 + 4 * v_down_block, cmd, 4, 4);
							System.arraycopy(v_app_len, 0, cmd, 8, 4);
						}
					}
					else if(i == 18) //下载数据
					{
						byte [] cmd_down = new byte[v_maxlen + 2];
						int v_len = 0;
						int v_start = 0;
						int v_frame = 1;
						while(v_app_down < v_down_length)
						{
							if((v_app_down + v_maxlen - 2) >= v_down_length) //last
							{
								v_len = v_down_length - v_app_down;
							}
							else
							{
								v_len = v_maxlen - 2;
							}
							if((v_len + 2) >= 0x100)
							{
								v_start = 1;
								cmd_down[0] = (byte) ((v_len + 2) / 0x100);
							}
							else
								v_start = 0;
							//拷贝数据
							cmd_down[v_start ++] = (byte) ((v_len + 2) % 0x100);
							cmd_down[v_start ++] = 0x36;
							cmd_down[v_start ++] = (byte)(v_frame ++);
							System.arraycopy(appbuf, v_app_down, cmd_down, v_start, v_len);
							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_down,v_len+v_start, Readbuf, 5003, Common.cmdmaxtime, 2);
							if((error > 100) || ((Readbuf[1]&0xFF) == 0x7F))//异常
							{
								break;
							}
							v_app_down += v_len;
							Sleep(2);
							t_com.CanSendOneOnly(0x7DF,cmdlink,3);
							ShowFuncTip("Downloading:" + v_applength + "\\" +  v_app_down);
						}
						v_down_block ++;
					}
					else if(i == 20) //crc
					{
						//MCU特殊处理,重复刷写的问题20211113
						if(Pname.equals("MCU") && mcu_padrnumber.length() < 2) //没读到零件号
						{
							mcu_padrnumber = Common.cartype.getModuleStr(Pname, "partNumber");
						}
						if( Pname.equals("FLC")|| Pname.equals("EACP")  ||
								(Pname.equals("MCU") && (mcu_padrnumber.equals("S30-2103020AF") || mcu_padrnumber.equals("S30-2103020")))
								|| Pname.equals("FLR") || (use_old_check == 1))
						{
							cmd[0] = 0x06;
							cmd[5] = (byte) ((v_crc&0xFFFF) / 0x100);
							cmd[6] = (byte) ((v_crc&0xFFFF) % 0x100);
							cmdlen = 7;
						}
						else
						{
							System.arraycopy(v_app_adr, 1, cmd, 5, 4);
							System.arraycopy(v_app_len, 0, cmd, 9, 4);
							cmd[13] = (byte) ((v_crc&0xFFFF) / 0x100);
							cmd[14] = (byte) ((v_crc&0xFFFF) % 0x100);
						}
					}
					else if(i == 21)
					{
						long endtime = System.currentTimeMillis(); //获得起始时间
						int usertime = (int) ((endtime-starttime)/1000);
						String time = usertime+"s";
						ShowProcessList(step_id + i,"Download time",time,"",Common.Rok);
						if(Pname.equals("PLG"))
						{
							continue;
						}
					}

					else if(i == 22)
					{
						if(Pname.equals("BTM"))
							break;
					}
					else if(i == 26)
					{
						if(Pname.equals("OBC"))
							break;
					}

					//特殊处理多次发送命令
					if(i == 13 || i == 18)
					{

					}
					else if(i < 8)
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime, 3);
					}
					else if(i == 15)
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 10, 3);
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 2, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						if(i > 8)
						{
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
							break;
						}
						else
							continue;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						if(i > 8)
						{
							if(i == 21 && Readbuf[3] == 0x13)
							{
								i --;
								use_old_check = 1;
								continue;
							}
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							return;
						}
						else
							continue;
					}
					else if(error < 500) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F && i > 7) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							break;
						}
						else if((Readbuf[1]&0xFF) == 0x7F && i < 8)
							continue;
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if((Readbuf[1] == 0x62) && ((Readbuf[2]&0xFF) == 0xF1) && (((Readbuf[3]&0xFF) == 0x88) || ((Readbuf[3]&0xFF) == 0xC0)))
					{
						byte [] v_soft = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_soft, 0, Readbuf[0] - 3);
						String v_softver = new String(v_soft);
						//String v_getsoft = Common.cartype.getModuleStr(Pname,partnumber_cache, "appFileVersionLatest");softwareVersion
						String v_getsoft = Common.cartype.getModuleStr(Pname,partnumber_cache, "softwareVersion"); //20231129金工沟通改的。
						if(i == 3) //不一致才继续
						{
							if(v_softver.equals(v_getsoft))
							{
								ShowProcessList(step_id + i,step_name,v_softver,v_getsoft,Common.Rok);
								if(Common.cartype.OptMode == 0)
									break;
							}
							else
							{
								ShowProcessList(step_id + i,step_name,v_softver, v_getsoft,Common.Rok);
							}
						}
						else if(i == 27)
						{
							if(v_softver.equals(v_getsoft))
							{
								ShowProcessList(step_id + i,step_name,v_softver,v_getsoft,Common.Rok);
							}
							else if(v_getsoft.length() > 0)
							{
								ShowProcessList(step_id + i,step_name,v_softver,v_getsoft ,Common.Rnok);
							}
							else
								ShowProcessList(step_id + i,step_name,v_softver,"",Common.Rnull);
						}
					}
					else if(Readbuf[1] == 0x62) //读数据
					{
						byte [] v_data = new byte[Readbuf[0] - 3];
						System.arraycopy(Readbuf, 4, v_data, 0, Readbuf[0] - 3);
						String v_show = "";
						String v_get = "";
						int v_code = (Readbuf[2]&0xFF) * 0x100 + (Readbuf[3]&0xFF);
						switch(v_code)
						{
							case 0xF190:
								v_show = new String(v_data);
								v_get = new String(Common.cartype.vin);
								break;
							case 0xF187:
								v_show = new String(v_data);
								v_show = v_show.replace(" ", "");
								v_show = v_show.trim();
								if(Common.cartype.getModuleNum(Pname) > 1) //多模块
									partnumber_cache = v_show;
								else
									partnumber_cache = "";
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"partNumber");
								break;
							case 0xF18A:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"supplierId");
								break;
							case 0xF191:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname,partnumber_cache, "hardwareVersion");
								break;
							case 0xF18C:
								v_show = new String(v_data);
								v_get = "";
								break;
							case 0xF1C0:
							case 0xF188:
								v_show = new String(v_data);
								v_get = Common.cartype.getModuleStr(Pname, partnumber_cache,"softwareVersion");
								break;
						}
						v_show = v_show.replace(" ", "");
						v_show = v_show.trim();
						if(Pname.equals("MCU") && v_code == 0xF187)
						{
							mcu_padrnumber = v_show;
						}
						else if(Pname.equals("VCU") && v_code == 0xF191)
						{
							v_get = "";
						}
						if(v_get.length() > 0)
						{
							if(v_show.equals(v_get))
								ShowProcessList(step_id + i,step_name,v_show,v_get,Common.Rok);
							else if(Common.cartype.OptMode == 1)
								ShowProcessList(step_id + i,step_name,v_show , v_get ,Common.Rnull);
							else
							{
								ShowProcessList(step_id + i,step_name,v_show, v_get ,Common.Rnok);
								//break;
							}
						}
						else
						{
							if(Common.cartype.OptMode == 1)
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnull);
							else
							{
								ShowProcessList(step_id + i,step_name,v_show,"",Common.Rnok);
								break;
							}
						}
					}
					else if(i == 14) //结束下载driver
					{
						if(v_dri_down_block < v_dri_block[0]) //多块处理
						{
							i = 11;continue;
						}
					}
					else if(i == 19) //结束下载app
					{
						if(v_down_block < v_app_block[0]) //多块处理
						{
							i = 14;

							continue;
						}
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x59) //读故障码
					{
						//处理故障码
						byte [] dtc = new byte[256];
						dtc[0] = (byte) (((Readbuf[0]&0xFF) - 3) / 4);
						System.arraycopy(Readbuf, 4, dtc, 1, Readbuf[0] - 3);
						ArrayList<Map<String,String>> dtclist = new ArrayList<Map<String,String>>();
						GetDtcArray(Pname + "1",dtc,dtclist,3,(byte)0x01);
						if(dtclist.size() > 0)
						{
							t_isok = false;
							for(int z = 0; z < dtclist.size(); z ++)
							{
								ShowProcessList(step_id + i,Pstep.getName(),dtclist.get(z).get("NAME") + "-" + dtclist.get(z).get("CONTEXT"),"",Common.Rnok);
							}
						}
						else
							ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context27),"",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x11) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else if(Readbuf[1] == 0x74)
					{
						v_maxlen = (Readbuf[3]& 0xFF) * 0x100 + (Readbuf[4]& 0xFF);
					}
					else if(Readbuf[1] == 14)
					{
						ShowProcessList(step_id + i,step_name, "block:" + v_dri_down_block,"",Common.Rok);
					}
					else if(Readbuf[1] == 19)
					{
						ShowProcessList(step_id + i,step_name, "block:" + v_down_block,"",Common.Rok);
					}
					else if(Readbuf[1] == 0x51)
					{
						Sleep(1000);
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x03 && Readbuf[4] == 0x00)
					{
						if(Readbuf[5] == 0x03)
						{
							i --;
							Sleep(1000);
						}
						else if(Readbuf[5] == 0x02)
						{
							ShowProcessList(step_id + i,step_name,"","",Common.Rok);
						}
						else
						{
							ShowProcessList(step_id + i,step_name,"" + (Readbuf[5] & 0xFF),"",Common.Rnok);
							break;
						}
					}
					else if(Readbuf[1] == 0x71 && Readbuf[2] == 0x01)
					{
						if((i == 20) && Pname.equals("BDCM"))
						{
							if(Readbuf[5] == 0x02) //ok
								ShowProcessList(step_id + i,step_name, "","",Common.Rok);
							else
								ShowProcessList(step_id + i,step_name, "","",Common.Rnok);
						}
						else
							ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(1000);
						if((i == 20) && Pname.equals("GW"))
						{
							byte [] cmd_gw = {0x04,0x31,0x01,(byte) 0xDF,(byte) 0xFF};
							error = t_com.CanOne2OneUDS(v_req, v_res, cmd_gw,cmd_gw.length, Readbuf, 5003, Common.cmdmaxtime, 3);
							Sleep(300);
						}
					}
					else if(Readbuf[1] == 0x51)
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
						Sleep(2000);
					}

					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}
		private void fun_S30_F1_Switch1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq("VCU");
			int v_res = Common.cartype.getModuleIdRes("VCU");
			if(v_req == 0)
			{
				v_req = Common.cartype.getModuleIdReq("PDCS");
				v_res = Common.cartype.getModuleIdRes("PDCS");
			}
			if(v_req == 0) return;
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,(byte) 0x80};
			int error = 0;
			int esc_a = 3;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					ShowFuncTip(step_name + "...");

					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}


					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime * 3, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[2] == 0x64 && Readbuf[3] == 0x15) //lock
					{
						if(Readbuf[4] == 0x01)
							ShowProcessList(step_id + i,step_name,"unlock","",Common.Rok);
						else if(Readbuf[4] == 0x00)
							ShowProcessList(step_id + i,step_name,"lock","",Common.Rok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						//ShowProcessList(step_id + i,step_name, "",Common.Rok);
					}
				}
			}
		}

		private void fun_EP12_F1_Switch1(Profile.Section Pstep)
		{
			int num = Integer.valueOf(Pstep.get("NUM"));
			int step_id = Integer.valueOf(Pstep.get("MODULE_ID"));
			int v_req = Common.cartype.getModuleIdReq("VCU1");
			int v_res = Common.cartype.getModuleIdRes("VCU1");
			if(v_req == 0)
			{
				v_req = Common.cartype.getModuleIdReq("PDCS");
				v_res = Common.cartype.getModuleIdRes("PDCS");
			}
			if(v_req == 0) return;
			String step_t = null;
			String step_name = null;
			byte [] Readbuf = new byte[512];
			byte [] seed = new byte[4];
			byte [] key = new byte[4];
			byte [] cmdlink = new byte[]{0x02,0x3E,(byte) 0x80};
			int error = 0;
			int esc_a = 3;
			SetCanInitTimer(15);
			for(int i = 1; i < (num + 1); i ++)
			{
				step_t = Pstep.get("T" + i);
				step_name = t_lang.get((step_id + i) + "");
				if(step_t == null) break;
				if(step_t.length() < 3) continue;
				if(step_t.substring(0, 3).equals("CMD"))  //cmd
				{
					//获取cmd长度
					int cmdlen = Commonfunc.HexStringtoInt(step_t.substring(4, 6)) + 1;
					byte [] cmd = new byte[cmdlen];
					if(Commonfunc.CmdStringToBytes(step_t.substring(4), cmd, cmdlen) != cmd.length) return;
					//---------------------发送前处理---------------------
					ShowFuncTip(step_name + "...");

					if(cmd[1] == 0x27 && cmd[2] == 0x04) //安全认证
					{
						if((seed[0] | seed[1]) == 0) continue;
						EolFunction.hezhong_safekey(seed,key);
						System.arraycopy(key, 0, cmd, 3, cmd[0] - 2);
					}


					//特殊处理多次发送命令
					if(i == -1)
					{
					}
					else //普通发送命令
					{
						error = t_com.CanOne2OneUDS(v_req, v_res, cmd,cmdlen, Readbuf, 5003, Common.cmdmaxtime*3, 3);
					}
					Sleep(Common.waittime);
					//错误处理
					if(error == 5001) //通讯超时
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,getString(R.string.msg_show_context24),"",Common.Rnok);
						break;
					}
					else if(error == 5002) //点击了返回
					{
						return;
					}
					else if(error > 5000)//其他负响应
					{
						t_isok = false;
						ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
						return;
					}
					else if(error < 100) //异常回复
					{
						if((Readbuf[1]&0xFF) == 0x7F) //异常
						{
							ShowProcessList(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),"",Common.Rnok);
							continue;
						}
					}
					else
						ShowProcessListHide(step_id + i,step_name,Commonfunc.bytesToHexString(Readbuf, 0,Readbuf[0]+1),Common.Rok);
					//特殊处理返回结果
					if(Readbuf[1] == 0x62 && Readbuf[2] == 0x64 && Readbuf[3] == 0x15) //lock
					{
						if(Readbuf[4] == 0x01)
							ShowProcessList(step_id + i,step_name,"unlock","",Common.Rok);
						else if(Readbuf[4] == 0x00)
							ShowProcessList(step_id + i,step_name,"lock","",Common.Rok);
					}
					else if(Readbuf[1] == 0x54) //清故障码
					{
						ShowProcessList(step_id + i,step_name,"","",Common.Rok);
					}
					else if(Readbuf[1] == 0x67 && Readbuf[2] == 0x03) //seed
					{
						System.arraycopy(Readbuf, 3, seed, 0, Readbuf[0] - 2);
					}
					else
					{
						ShowProcessList(step_id + i,step_name, "","",Common.Rok);
					}
				}
			}
		}

		public void saveData(String id,String step_name,String content,String result){
			HashMap<String, String> map=new HashMap<String, String>();
			map.put("SHOW_ID", id);
			map.put("SHOW_CONTEXT", step_name);
			if("".equals(content)){
				content="---";
			}
			map.put("SHOW_RESULT", content);
			map.put("SHOW_TIME", result);
			updateData.add(map);
		}
		///////////////////////////////////////功能模块-end////////////////////////////////////////////////
	}
	private void Sleep(int time)
	{
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}
	//---------------------------------端口初始化----------------------------------------
	//获取K线波特率
	private int GetKwpbaud(String module)
	{
		int v_baud = 0;
		//获取配置文件路径
		String v_path = Common.Dir + "protocol.ini";
		Ini inican;
		try {
			inican = new Ini(new File(v_path));
			Profile.Section sec = inican.get(module);
			v_baud = Integer.valueOf(sec.get("BAUD"));
			if(Common.Debug) Log.i(TAG,"BAUD = " + v_baud);
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return v_baud;
	}
//	private void SaveDataToDb(String vin)
//	{
//		DbAdapter db = new DbAdapter(context);
//		//---------------------第一步存主数据库---------------------
//		db.open();
//		//查询测试次数
//		int oldtestnum = db.QueryMainTestNum(new String(vin));
//		if(Common.Debug) Log.i(TAG,"已经存储次数为:" + oldtestnum);
//		ContentValues cv = new ContentValues();
//		cv.put(TableMain.vin, new String(vin));
//		cv.put(TableMain.cartype, "");
//		cv.put(TableMain.station, m_func_title.getText().toString());
//		SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date time = new java.util.Date();
//		cv.put(TableMain.timestart, tempDate.format(time).toString());
//		cv.put(TableMain.timeend, tempDate.format(time).toString());
//		boolean isok = true;
//		for(int i = 0; i < m_listshowArray.size(); i ++)
//		{
//			if(m_listshowArray.get(i).get("RESULT").equals("N.OK"))
//			{
//				isok = false;
//				break;
//			}
//		}
//		if(isok)
//			cv.put(TableMain.result, "OK");
//		else
//			cv.put(TableMain.result, "N.OK");
//		cv.put(TableMain.printdata, "---");
//		cv.put(TableMain.device, "");
//		cv.put(TableMain.appversion,"team A");
//		cv.put(TableMain.testnum, oldtestnum + 1);
//		if(Common.Debug) Log.i(TAG,cv.toString());
//		db.addmainTable(cv,false);
//		db.close();
//		//保存数据到服务器
//		ShowFuncTip(R.string.func_show_connectserver);
//		ServerSocket v_server = new ServerSocket();
//		if(v_server.ConnectServer(Common.Server_IP, Common.Server_PORT))
//		{
//			if(v_server.SendDataToServer("1:" + cv.toString()))
//			{
//				ShowFuncTip(R.string.func_show_saveupdate_ok);
//			}
//			else
//			{
//				ShowFuncTip(R.string.func_show_saveupdate_nok);
//			}
//		}
//		v_server.close();
//	}
	//上传TBOX数据至斯润服务器
	private String UpdateDataToWebServer(ContentValues Pvalue)
	{
		JSONObject  cv = new JSONObject();
		try {
			cv.put("vin", Pvalue.get("vin"));
			cv.put("deliveryDate", Pvalue.get("deliveryDate"));
			cv.put("theTypeOfVehicle", Pvalue.get("theTypeOfVehicle"));
			cv.put("serialNumber", Pvalue.get("serialNumber"));
			cv.put("mdn",Pvalue.get("mdn"));
			cv.put("iccid",Pvalue.get("iccid"));
			cv.put("hardwareVersion",Pvalue.get("hardwareVersion"));
			cv.put("softwareVersion",Pvalue.get("softwareVersion"));
			cv.put("manufacturer",Pvalue.get("manufacturer"));
			cv.put("partName",Pvalue.get("partName"));
			cv.put("partNum",Pvalue.get("partNum"));
			cv.put("loginName", "tye-net");
			cv.put("imsi",Pvalue.get("imsi"));
			cv.put("vehicleConfig",Pvalue.get("vehicleConfig"));
			cv.put("defaultKey", Pvalue.get("defaultKey"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String v_update = cv.toString();
		if(Common.Debug) Log.i(TAG,v_update);
		byte [] v_buf = new byte[v_update.length() + 10];
		int v_len = DesUtil.DES(0, v_update.getBytes(), v_update.length(), v_buf);
		String v_sendbuf = Commonfunc.bytesToHexStringP(v_buf, 0, v_len);
		//String st = HttpRequest.sendPost("http://124.202.152.26:8110/crmeps/{theTypeOfVehicle}/accountStart", v_sendbuf);
		String st = HttpRequest.sendPost(Common.Tbox_Server + ":" + Common.Tbox_Port + Common.Tbox_start,v_sendbuf);
		return st;
	}
	//参数：modename -- 模块名称;cmd -- 故障码数组，1byte（ num） + 4byte(高、中、低、状态) + ... ; DtcLen -- 单个故障码字节数,Pstatus-- 判断位
	private void GetDtcArray(String modename,byte [] cmd,ArrayList<Map<String,String>> list,int DtcLen,byte Pstatus)
	{
		int dtcnum = (cmd[0] & 0xFF);
		if(Common.Debug) Log.i(TAG,"DTC num:" + dtcnum);
		if(dtcnum == 0) return;
		byte [] code = new byte[3];
		Profile.Section sec = Common.cartype.idtc.get(modename);
		//查找过滤DTC数组
		Profile.Section secfilter = Common.cartype.idtc.get(modename + "_FILTER");
		int filternum = 0;
		if(secfilter != null)
			if(secfilter.get("NUM") != null)
				filternum = Integer.valueOf(secfilter.get("NUM"));//过滤的故障码个数
		if(Common.Debug) Log.i(TAG,"Mode=" + modename + ",filternum =" + filternum);
		ArrayList<String> filterlistname = new ArrayList<String>(); //过滤数组
		if(filternum > 0)
		{
			for(int i = 0; i < filternum; i ++)
			{
				String data = secfilter.get("T" + (i + 1));
				if(Common.Debug) Log.i(TAG,"i = " + i + "data = " + data);
				if(data.length() < 5) continue;
				filterlistname.add(data);
			}
		}
		//过滤故障码
		try {
			Ini DTC_disable = new Ini(new File(Common.Dir + Common.GuestName + "/" + Common.cartype.car + "/" + "DTC_Disable_" + Common.cartype.station + ".ini"));
			Profile.Section sec_0filter = DTC_disable.get(modename);
			int filter = 0;
			if (sec_0filter !=null){
				if (sec_0filter.get("NUM")!=null){
					filter = Integer.valueOf(sec_0filter.get("NUM"));
				}
			}
			if(filter > 0)
			{
				for(int i = 0; i < filter; i ++)
				{
					String data = sec_0filter.get("T" + (i + 1));
					if(Common.Debug) Log.i(TAG,"i = " + i + "data = " + data);
					if(data.length() < 5) continue;
					filterlistname.add(data);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < dtcnum; i ++)
		{
			System.arraycopy(cmd, 1 + i * 4, code, 0, 3); //负责dtc
			byte status = cmd[i * 4 + 3 + 1];
			String pcode = GetPUBCCode(code,DtcLen); //getPCBUcode
			//比较是否被过滤
			if(filterlistname != null)
			{
				int kk = 0;
				for(kk = 0; kk < filterlistname.size();kk ++)
				{
					if(pcode.equals(filterlistname.get(kk)))
					{
						break;
					}
				}
				if(kk < filterlistname.size())
					continue;
			}
			HashMap<String,String> map = new HashMap<String, String>();
			String dtc = null;
			if(sec != null)
				dtc = sec.get(pcode);
			if(dtc == null)
				dtc = getString(R.string.msg_show_context63);
			else if(dtc.length() <= 0)
				dtc = getString(R.string.msg_show_context63);
			//判断是否历史和当前
			if((status & Pstatus) == 0) //历史
				continue;

			//pcode += "-" + Commonfunc.byteToHexString(status);
			map.put("NAME",pcode);
			if(Common.Debug) Log.i(TAG,"Dtc ID:" + pcode);
			map.put("CONTEXT", dtc);
			if(Common.Debug) Log.i(TAG,"Dtc Code:"  + dtc);
			list.add(map);
		}
	}
	private String GetPUBCCode(byte [] code,int dtclen)
	{
		String pcode = null;
		switch(code[0] & 0xC0)
		{
			case 0x00:
				pcode = "P";
				break;
			case 0x40:
				pcode = "C";
				break;
			case 0x80:
				pcode = "B";
				break;
			case 0xC0:
				pcode = "U";
				break;
			default:
				pcode = "P";
				break;
		}
		pcode += Commonfunc.byteToHexString((byte) (code[0] & 0x3F));
		pcode += Commonfunc.byteToHexString(code[1]);
		if(dtclen == 3)
			pcode += Commonfunc.byteToHexString(code[2]);
		return pcode;
	}
	private void AddModulePrintList(String Pmodule,String Pcontext)
	{
		if(modules.size() <= 0)
			return;
		for(EcuModule mod : modules)
		{
			if(Pmodule.equals(mod.getname()))
			{
				mod.addcontext(Pcontext);
				return;
			}
		}
	}
	private int fileExists(String path){
		File file = new File(path);
		if(file.isFile())
			return 1; // 是文件
		else if(file.isDirectory())
		{
			if(file.list().length>0)
				return 2;   // 非空文件夹
			else
				return 3;   // 空文件夹
		}
		return 0; // 路径不存在
	}
}

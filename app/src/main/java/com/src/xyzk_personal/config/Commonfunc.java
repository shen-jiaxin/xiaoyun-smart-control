package com.src.xyzk_personal.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Commonfunc {
	private final static String TAG = "Commonfunc";
	/****
	 * @author 把字节数组转换成16进制字符串
	 * @param bArray
	 *            字节数组
	 * @return String 转换成的字符串
	 */
	public static final String bytesToHexString(byte[] bArray, int start,int end) {
		StringBuffer sb = new StringBuffer(bArray.length);
		String sTemp;
		// int v_len = bArray.length;
		int last = end - 1;
		//sb.append("0x");
		for (int i = start; i < end; i++) {
			sTemp = Integer.toHexString(0xFF & bArray[i]);
			if (sTemp.length() < 2)
				sb.append("0");
			sb.append(sTemp.toUpperCase());
			if (i != last)
				sb.append(" ");
		}
		return sb.toString();
	}
	public static final String bytesToHexStringP(byte[] bArray, int start,int end) {
		StringBuffer sb = new StringBuffer(bArray.length);
		String sTemp;
		int v_len = bArray.length;
		int last = end - 1;
		//sb.append("0x");
		for (int i = start; i < end; i++) {
			sTemp = Integer.toHexString(0xFF & bArray[i]);
			if (sTemp.length() < 2)
				sb.append("0");
			sb.append(sTemp.toUpperCase());
		}
		return sb.toString();
	}
	public static final String bytesToHexStringP2(byte[] bArray, int start,int end) {
		StringBuffer sb = new StringBuffer(bArray.length);
		String sTemp;
		int v_len = bArray.length;
		int last = end - 1;
		//sb.append("0x");
		for (int i = start; i < end; i++) {
			sTemp = Integer.toHexString(0xFF & bArray[i]);

			sb.append(sTemp.toUpperCase());
		}
		return sb.toString();
	}
	public static final String byteToHexString(byte bt)
	{//
		String st = Integer.toHexString(0xFF & bt);
		if(st.length() < 2)
			st = "0" + st;
		return st.toUpperCase();
	}
	public static final String byteToString(byte bt)
	{
		String st = Integer.toString(0xFF & bt);
		if(st.length() < 2)
			st = "0" + st;
		return st;
	}
	public static final String msToString(int ms)
	{
		String st = Integer.toString(ms);
		if(st.length() < 2)
			st = "00" + st;
		else if(st.length() < 3)
			st = "0" + st;
		return st;
	}
	public static final String canidToHexString(int canid)
	{
		String st = Integer.toHexString(canid).toUpperCase();
		if(st.length() < 2)
			st = "00" + st;
		else if(st.length() < 3)
			st = "0" + st;
		return st;
	}
	public static byte CalcChechSum(byte[] data, int start, int end) {
		byte chechsum = 0;
		if (start >= 0 && start < end && data != null && data.length > 0) {
			//int calc_len = end - start;
			for (int i = start; i < end; i++) {
				chechsum += data[i];
			}
			return (byte) chechsum;
		}
		return (byte) 0x00;
	}
	public static int HexStringtoInt(String hexstr)
	{
		int x=0,iRet = 0;
		if(hexstr == null)
			return iRet;
		int length = hexstr.length();
		for(int i = 0; i < length; i ++)
		{
			char c = hexstr.charAt(i);
			//对'0'->0，'a'->10
			if(c>='a'&&c<='f'){
				x=c-'a'+10;
			}else if(c>='A'&&c<='F'){
				x=c-'A'+10;
			}else if(c>='0'&&c<='9'){
				x=c-'0';
			}
			iRet=(iRet<<4)| x;//n=n*4+x,移位拼接
		}
		return iRet;
	}
	public static int CmdStringToBytes(String cmdstr,byte[] cmdbuf,int cmdlen)
	{
		if(Common.Debug) Log.i(TAG,"Src:" + cmdstr);
		int strlen = cmdstr.length();
		for(int i = 0; i < cmdlen; i ++)
		{
			String data = "";
			if(strlen >= (i* 3 + 2))
				data = cmdstr.substring(i * 3, i * 3 + 2);
			//if(Common.Debug) Log.i(TAG,"data:"+ data);
			if(data != null)
				cmdbuf[i] = (byte) HexStringtoInt(data);
		}
		if(Common.Debug) Log.i(TAG,"Obj:"+ bytesToHexString(cmdbuf, 0,cmdlen));
		return cmdlen;
	}

	public static int CheryCmdStringToBytes(String cmdstr,byte[] cmdbuf,int cmdlen)
	{
		//		Log.e("length", "---------------------"+cmdstr.length());
		for(int i = 0; i < cmdlen; i ++)
		{
			String data = cmdstr.substring(i*2,i*2+2);
			//if(Common.Debug) Log.i(TAG,"data:"+ data);
			//Log.e("DATA", "---------------------"+data);
			cmdbuf[i] = (byte) HexStringtoInt(data);
		}
//		if(Common.Debug) Log.i(TAG,"Obj:"+ bytesToHexString(cmdbuf, 0,cmdlen));
		return cmdlen;
	}

	public static int StringToBytes(String str,byte[] buf,int cmdlen)
	{
		if(Common.Debug) Log.i(TAG,"Src:" + str);
		int len = 0;
		int strlen = str.length();
		for(int i = 0; i < cmdlen; i ++)
		{
			String data = "";
			if(strlen >= (i* 2 + 2))
				data = str.substring(i * 2, i * 2 + 2);
			else
				break;
			//if(Common.Debug) Log.i(TAG,"data:"+ data);
			buf[i] = (byte) HexStringtoInt(data);
			len ++;
		}
		//if(Common.Debug) Log.i(TAG,"Obj:"+ bytesToHexString(buf, 0,cmdlen));
		return len;
	}
	//字符串转为byte数组
	public static int StringTobyteString(String str,byte[] buf,int cmdlen)
	{
		for(int i = 0; i < cmdlen; i ++)
		{
			buf[i] = (byte) str.charAt(i);
		}
		return cmdlen;
	}
	//整数转ip
	public static String IntToIp(int ip)
	{
		return ( ip & 0xFF) + "." +
				((ip >> 8 ) & 0xFF) + "." +
				((ip >> 16 ) & 0xFF)  + "." +
				((ip >> 24 ) & 0xFF );
	}
	//vin校验获取字符对应值
	public static int GetVinFormat(byte Pv)
	{
		if(Pv >= '0' && Pv <= '9')
			return Pv - 0x30;
		else if(Pv >= 'A' && Pv <= 'H')
			return Pv - 'A' + 1;
		else if(Pv >= 'J' && Pv <= 'N')
			return Pv - 'J' + 1;
		else if(Pv == 'P')
			return 7;
		else if(Pv == 'R')
			return 9;
		else if(Pv >= 'S' && Pv <= 'Z')
			return Pv - 'S' + 2;
		else
			return -1;
	}
	//校验VIN是否合法
	public static boolean CheckVinFormat(byte [] Pvin)
	{
		if(Pvin.length != 17) return false;
		if(Pvin[0] < 'A' || Pvin[0] > 'Z') return false;
		int [] v_jiaquan = {8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2};
		int v_slt = 0;
		for(int i = 0; i < 17; i ++)
		{
			if(i == 8) continue;
			v_slt += GetVinFormat(Pvin[i]) * v_jiaquan[i];
		}
		int v_result = v_slt % 11;
		if(v_result == 10)
		{
			if(Pvin[8] == 'X') return true;
		}
		else
		{
			if(Pvin[8] == (v_result + 0x30)) return true;
		}
		return false;
	}
	//校验配置码是否合法
	public static boolean CheckConfigFormat(String Pvin)
	{

		if(Pvin.length()== 196||Pvin.length()==16)
		{
			for (int i = 0; i < Pvin.length(); i++) {
				if(Pvin.charAt(i) > 'A' && Pvin.charAt(i) < 'Z'||Pvin.charAt(i) > '0' && Pvin.charAt(i) < '9')
				{
					return true;
				}
			}

		}
		return false;
	}
	//十六进制转BCD码
	public static byte HexToBCD(byte code)
	{
		byte bcd = 0;
		bcd = (byte) ((code / 10) * 0x10 + (code % 10));
		return bcd;
	}
	public static byte HexStrToByte(byte[] Pstr,int start)
	{
		byte a = (byte) ((CharToHexInt(Pstr[start]) << 4) + CharToHexInt(Pstr[start + 1]));
		return  a;
	}
	public static int CharToHexInt(byte c)
	{
		int x = 0;
		if(c>='a'&&c<='f'){
			x=c-'a'+10;
		}else if(c>='A'&&c<='F'){
			x=c-'A'+10;
		}else if(c>='0'&&c<='9'){
			x=c-'0';
		}
		return x;
	}
	public static boolean bytesCompare(byte [] A,byte [] B)
	{
		boolean isok = true;
		for(int i = 0; i < A.length; i ++)
		{
			if(A[i] != B[i])
			{
				isok = false;
				break;
			}
		}
		return isok;
	}
	//获取Dictionary下面的子文件夹
	public static List<String> GetDictionarySonList(String DirName)
	{
		List<String> mylist = new ArrayList<String>();
		File mainfile = new File(DirName);
		if(mainfile == null) return mylist;
		if(mainfile.isDirectory() == false) return mylist;
		File [] files = mainfile.listFiles();
		if(files.length <= 0) return mylist;
		for(int i = 0; i < files.length; i ++)
		{
			if(files[i].isDirectory())
				mylist.add(files[i].getName());
		}
		return mylist;
	}
	//5byte head
	public static String IntToHeadStr(int Psize)
	{
		String v_data  = "0000" + Psize;
		int len = v_data.length();
		v_data = v_data.substring(len - 5, len);
		return v_data;
	}
	public static String FindValueFromJson(String Pstr,String Pname,String Pitem)
	{
		String v_value = "";
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(Pstr);
			JSONArray jsonArray = jsonObject.getJSONArray("value");
			for(int i = 0; i < jsonArray.length(); i ++)
			{
				JSONObject item = jsonArray.getJSONObject(i);
				if(item.get("name").equals(Pname))
				{
					v_value = item.getString(Pitem);
					break;
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v_value;
	}
	public static List<Ecu> readXML(InputStream inStream) throws Exception {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser saxParser = spf.newSAXParser(); // 创建解析器
		XmlContentHandler handler = new XmlContentHandler();
		saxParser.parse(inStream, handler);
		inStream.close();
		return handler.getEcus();
	}

	static long []  s30_crctab =
			{
					0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
					0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
					0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
					0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
					0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
					0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
					0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
					0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
					0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
					0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
					0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
					0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
					0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
					0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
					0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
					0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
					0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
					0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
					0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
					0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
					0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
					0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
					0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
					0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
					0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
					0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
					0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
					0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
					0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
					0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
					0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
					0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
			};
	public static short s30_CalcCRC(int size, byte [] data,short lastcrc)
	{
		short tmp;
		int i;
		for(i=0;i<size;i++)
		{
			tmp=  (short) ((((lastcrc&0xFFFF)>>8)^data[i]) & 0xFF);
			lastcrc=(short) (((lastcrc&0xFFFF)<<8)^s30_crctab[(tmp&0xFFFF)]);
		}
		return lastcrc;
	}
	//返回读到的多段数据的总长度,Padr[0] -- 表示地址组个数,4byte一组
	public static int GetS19FileLength(String Pfile,byte [] Padr) throws IOException
	{
		int readlen = 0;
		FileInputStream fileInputStream = new FileInputStream(Pfile);
		InputStreamReader inputreader = new InputStreamReader(fileInputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line = "";
		long len = 0;
		long v_adress = 0;
		long v_blockadress = 0;
		long v_block = 0;
		long v_blocklen = 0;
		boolean isstart = false;
		byte [] v_adr = new byte[4];
		//byte [] v_buf = new byte[64];
		while ((line = buffreader.readLine()) != null)
		{
			//解析数据
			if(line == null) break;
			if(line.length() > 0)
			{
				v_adr.clone();
				len = ReadS19Line(line, v_adr,null);
				v_adress = (v_adr[0]&0xFF) * 0x1000000 + (v_adr[1]&0xFF) * 0x10000 + (v_adr[2]&0xFF) * 0x100 + (v_adr[3]&0xFF);
				if(len > 0)  //开始
				{
					if(isstart == false || ((v_blockadress + v_blocklen) != v_adress)) //开启一个新的块
					{
						isstart = true;
						//拷贝起始地址
						v_blockadress = v_adress;
						v_blocklen = 0;
						System.arraycopy(v_adr, 0, Padr, (int) (1 + v_block * 4), 4);
						v_block ++;
					}
					v_blocklen += len;
					readlen += len;
				}
				else if(len == -1) //开始
				{

				}
				else
				{
					break;
				}

			}
		}
		if(v_block < 255) //正常
			Padr[0] = (byte) v_block;
		else
			readlen = 0; //异常
		return readlen;
	}
	//返回总长度，Pblock[0] -- 块数，后面是每块长度
	public static int ReadS19File(String Pfile,byte [] readbuf,int Pbufadr,int [] Pblock) throws IOException
	{
		int readlen = Pbufadr;
		FileInputStream fileInputStream = new FileInputStream(Pfile);
		InputStreamReader inputreader = new InputStreamReader(fileInputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line = "";
		long len = 0;
		long v_adress = 0;
		long v_blockadress = 0;
		long v_block = 0;
		long v_blocklen = 0;
		long v_buf_size = readbuf.length;
		byte [] v_adr = new byte[4];
		byte [] v_buf = new byte[64];
		while ((line = buffreader.readLine()) != null)
		{
			//解析数据
			if(line == null) break;
			if(readlen >= readbuf.length) break;
			if(line.length() > 0)
			{
				v_adr.clone();
				len = ReadS19Line(line, v_adr, v_buf);
				v_adress = (v_adr[0]&0xFF) * 0x1000000 + (v_adr[1]&0xFF) * 0x10000 + (v_adr[2]&0xFF) * 0x100 + (v_adr[3]&0xFF);
				if(len > 0)  //开始
				{
					if(((v_blockadress + v_blocklen) != v_adress) || (v_adress == 0)) //开启一个新的块
					{
						//拷贝起始地址
						//System.arraycopy(v_adr, 0, Padr, 1 + v_block * 4, 4);
						if(v_blocklen > 0)
							Pblock[(int) v_block] = (int) v_blocklen;
						v_blocklen = 0;
						v_blockadress = v_adress;
						v_block ++;
					}
					if(v_buf_size < (readlen + len))
						break;
					System.arraycopy(v_buf, 0, readbuf, readlen, (int) len);
					v_blocklen += len;
					readlen += len;
				}
				else if(len == -1) //开始
				{

				}
				else
				{
					break;
				}
			}
		}
		//记录最后一块
		if(v_blocklen > 0)
			Pblock[(int) v_block] = (int) v_blocklen;
		//记录块数
		Pblock[0] = (int) v_block;
		return readlen;
	}
	static int ReadS19Line(String Pline,byte [] Padr, byte [] Pbuf)
	{
		int len = 0;
		String head = Pline.substring(0,2);
		Pline = Pline.substring(2);
		byte [] v_buf = new byte[Pline.length() / 2];
		if(head.equals("S3") || head.equals("S2") || head.equals("S1"))
		{
			len = Commonfunc.StringToBytes(Pline, v_buf, v_buf.length);
			if(len < 5 || (len != ((v_buf[0]&0xFF) + 1))) return 0;
			//addr
			int adrlen = (head.getBytes()[1]&0xFF) - 0x30 + 1;
			System.arraycopy(v_buf, 1, Padr, 4 - adrlen, adrlen);
			len = len - adrlen - 2;
			if(Pbuf != null)
				System.arraycopy(v_buf, adrlen + 1, Pbuf, 0, len);
		}
		else if(head.equals("S0")) //开始获取地址高字节
		{
			len = -1;
		}
		else if(head.equals("S7") || head.equals("S8")) //结束
		{
			len = -2;
		}
		return len;
	}
	/*	static int ReadS19address(String Pline,byte [] Padr)
        {
            int len = 0;
            String head = Pline.substring(0,2);
            Pline = Pline.substring(2);
            byte [] v_buf = new byte[5];
            if(head.equals("S3") || head.equals("S2") || head.equals("S1"))
            {
                len = Commonfunc.StringToBytes(Pline, v_buf, v_buf.length);
                if(len < 4) return 0;
                //addr
                int adrlen = (head.getBytes()[1]&0xFF) - 0x30 + 1;
                System.arraycopy(v_buf, 1, Padr, 4 - adrlen, adrlen);
                len = v_buf[0] - adrlen - 1;
            }
            else if(head.equals("S0")) //开始获取地址高字节
            {
                len = -1;
            }
            else if(head.equals("S7") || head.equals("S8")) //结束
            {
                len = -2;
            }
            return len;
        }
    */
	//按S19多段解析存储
	public static int GetHexFileLength(String Pfile,byte [] Padr) throws IOException
	{
		int readlen = 0;
		FileInputStream fileInputStream = new FileInputStream(Pfile);
		InputStreamReader inputreader = new InputStreamReader(fileInputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line = "";
		int len = 0;
		//int v_adress = 0;
		boolean isstart = false;
		byte [] v_adr = new byte[4];
		//byte [] v_buf = new byte[64];
		while ((line = buffreader.readLine()) != null)
		{
			//解析数据
			if(line == null) break;
			if(line.length() > 0)
			{
				v_adr.clone();
				len = ReadHexLine(line, v_adr,null);
				//v_adress = (v_adr[0]&0xFF) * 0x1000000 + (v_adr[1]&0xFF) * 0x10000 + (v_adr[2]&0xFF) * 0x100 + (v_adr[3]&0xFF);
				if(isstart == false)
				{
					if(len == 1000) //头
					{
						isstart = true;
						//copy
						//System.arraycopy(v_adr, 0, Padr, 0, 2);
						Padr[0] = 0x01;
						Padr[1] = v_adr[0];
						Padr[2] = v_adr[1];
						readlen = 0;
					}
					else if(len == 0)
					{
						break;
					}
				}
				else if(len > 0 && len < 100)
				{
					if(readlen == 0)
						System.arraycopy(v_adr, 0, Padr, 3, 2);
					readlen += len;
				}
				else if(len == 1000)
				{

				}
				else
				{
					break;
				}
			}
		}
		return readlen;
	}
	//返回 0--结束,1000-头地址,1- 100实际数据长度
	static int ReadHexLine(String Pline,byte [] Padr,byte [] Pdata)
	{
		int error = 0;
		int len = 0;
		String head = Pline.substring(0,3);
		Pline = Pline.substring(3);
		byte [] v_buf = new byte[64];
		if(head.equals(":02")) //头
		{
			len = Commonfunc.StringToBytes(Pline, v_buf, v_buf.length);
			if(len < 4) return 0;
			System.arraycopy(v_buf, 3, Padr, 0,2);
			error = 1000;
		}
		else if(head.equals(":00")) //结束
		{
			error = 0;
		}
		else
		{
			int buflen = HexStringtoInt(head.substring(1));
			len = Commonfunc.StringToBytes(Pline, v_buf, v_buf.length);
			if(len < 4) return 0;
			if((buflen + 4) == len) //ok
			{
				error = buflen;
				//copy
				System.arraycopy(v_buf, 0, Padr, 0,2);  //address
				if(Pdata != null)
					System.arraycopy(v_buf, 3, Pdata, 0,buflen);	//data
			}
			else
				error = 0;
		}
		return error;
	}
	//返回总长度，Pblock[0] -- 块数，后面是每块长度
	public static int ReadHexFile(String Pfile,byte [] readbuf,int [] Pblock) throws IOException
	{
		int readlen = 0;
		FileInputStream fileInputStream = new FileInputStream(Pfile);
		InputStreamReader inputreader = new InputStreamReader(fileInputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line = "";
		int len = 0;
		//int v_adress = 0;
		boolean isstart = false;
		byte [] v_adr = new byte[4];
		byte [] v_buf = new byte[64];
		while ((line = buffreader.readLine()) != null)
		{
			//解析数据
			if(line == null) break;
			if(line.length() > 0)
			{
				v_adr.clone();
				len = ReadHexLine(line, v_adr,v_buf);
				//v_adress = (v_adr[0]&0xFF) * 0x1000000 + (v_adr[1]&0xFF) * 0x10000 + (v_adr[2]&0xFF) * 0x100 + (v_adr[3]&0xFF);
				if(isstart == false)
				{
					if(len == 1000) //头
					{
						isstart = true;
						//copy
						//System.arraycopy(v_adr, 0, Padr, 0, 2);
						//Padr[0] = v_adr[0];
						//Padr[1] = v_adr[1];
						readlen = 0;
					}
					else if(len == 0)
					{
						break;
					}
				}
				else if(len > 0 && len < 100)
				{
					System.arraycopy(v_buf, 0, readbuf, readlen, len);
					readlen += len;
				}
				else if(len == 1000)
				{

				}
				else
				{
					break;
				}
			}
		}
		//临时先用
		Pblock[0] = 1;
		Pblock[1] = readlen;
		return readlen;
	}
	//序列号格式校验
	public static boolean CheckSerialformat(byte [] Pdata)
	{
		boolean isok = true;
		for(int i = 0; i < Pdata.length; i ++)
		{
			if(checkbyte(Pdata[i]) == false)
			{
				isok = false;
				break;
			}
		}
		return isok;
	}
	public static boolean checkbyte(byte P)
	{
		short p = (short) (P&0xFF);
		if(P == 0 || P == 0x20)
			return true;
		else if(p > 0x40 && p < 0x7B)
			return true;
		else if(p > 0x2A && p < 0x3A)
			return true;
		else
			return false;
	}
}

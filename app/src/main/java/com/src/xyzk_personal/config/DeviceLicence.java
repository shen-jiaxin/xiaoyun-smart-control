package com.src.xyzk_personal.config;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;

import com.src.tsdl_personal.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

public class DeviceLicence {
	private static ArrayList<String> m_devicelist = null;

	public DeviceLicence() {
		// TODO Auto-generated constructor stub
		m_devicelist = new ArrayList<String>();
		m_devicelist.add("50:80:4a:ba:8f:16"); //test
		//第1批10台
		m_devicelist.add("64:c4:03:01:d2:93"); //1
		m_devicelist.add("64:c4:03:01:d5:23"); //2
		m_devicelist.add("64:c4:03:01:d2:6a"); //3
		m_devicelist.add("64:c4:03:01:d3:07"); //4
		m_devicelist.add("64:c4:03:01:d2:47"); //5
		m_devicelist.add("64:c4:03:01:d5:73"); //6
		m_devicelist.add("e4:08:e7:6d:b1:41"); //7
		m_devicelist.add("e4:08:e7:6d:b2:2e"); //8

	}
	public boolean CheckDevice(Context context)
	{
		boolean isok = false;
		String result = "非法设备!";
		WifiAdmin m_wifi = new WifiAdmin(context);
		if(m_wifi.getState() < 2) //已经关闭，则打开
		{
			Toast.makeText(context, R.string.main_tip_text7, Toast.LENGTH_SHORT).show();
			m_wifi.openWifi();
			return false;
		}
		String defaultMac = "02:00:00:00:00:00";
		String addr = m_wifi.getMacAddress();
		if(addr.equals(defaultMac)) //高级版本
			addr = getWifiMacAddress();
		for(int i = 0; i < m_devicelist.size(); i ++)
		{
			if(addr.equals(m_devicelist.get(i)))
			{
				result = "OK";
				isok = true;
				break;
			}
		}
		if(isok == false)
		{
			Toast.makeText(context, result, Toast.LENGTH_LONG).show();
		}
		return isok;
	}

	@SuppressLint("NewApi")
	private String getWifiMacAddress() {
		String macAddress = null;
		StringBuffer buf = new StringBuffer();
		NetworkInterface networkInterface = null;
		try {
			networkInterface = NetworkInterface.getByName("eth1");
			if (networkInterface == null) {
				networkInterface = NetworkInterface.getByName("wlan0");
			}
			if (networkInterface == null) {
				return "02:00:00:00:00:00";
			}
			byte[] addr = networkInterface.getHardwareAddress();
			for (byte b : addr) {
				buf.append(String.format("%02x:", b));
			}
			if (buf.length() > 0) {
				buf.deleteCharAt(buf.length() - 1);
			}
			macAddress = buf.toString();
		} catch (SocketException e) {
			e.printStackTrace();
			return "02:00:00:00:00:00";
		}
		return macAddress;
	}
}

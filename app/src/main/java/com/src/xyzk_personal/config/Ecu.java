package com.src.xyzk_personal.config;

public class Ecu {
    private int reqid;
    private int resid;
    private String ecuName;
    private String manufactureDate;
    private String supplierId;
    private String partNumber;
    private String softwareAss;
    private String hardwareVersion;
    private String softwareVersion;
    private String eolConfigCode;
    private String ecode;
    private String serialNumber;
    private String driverFileVersion;
    private String driverFileVersionLatest;
    private String driverFileName;
    private String driverServerPath;
    private String appFileVersion;
    private String appFileVersionLatest;
    private String appFileName;
    private String appServerPath;
    private String calFileVersion;
    private String calFileVersionLatest;
    private String calFileName;
    private String calServerPath;
    private String iccid;
    private String name;
    //mes add
    private String app_MD5;
    private String cal_MD5;
    private String driver_MD5;
    private String vin;
    public Ecu() {
        reqid = 0;
        resid = 0;
        ecuName = "";
        manufactureDate = "";
        supplierId = "";
        partNumber = "";
        softwareAss = "";
        hardwareVersion = "";
        softwareVersion = "";
        eolConfigCode = "";
        ecode = "";
        serialNumber = "";
        driverFileVersion = "";
        driverFileVersionLatest = "";
        driverFileName = "";
        driverServerPath = "";
        appFileVersion = "";
        appFileVersionLatest = "";
        appFileName = "";
        appServerPath = "";
        calFileVersion = "";
        calFileVersionLatest = "";
        calFileName = "";
        calServerPath = "";
        name = "";
        app_MD5 = "";
        cal_MD5 = "";
        driver_MD5 = "";
        iccid = "";
        vin = "";
    }

    public Ecu(int Preq, int Pres) {
        super();
        this.reqid = Preq;
        this.resid = Pres;
    }
    public Integer getreqid() {
        return reqid;
    }
    public void setreqid(int P) {
        this.reqid = P;
    }

    public Integer getresid() {
        return resid;
    }
    public void setresid(int P) {
        this.resid = P;
    }

    public String getecuName() {
        return ecuName;
    }
    public void setecuName(String P) {
        this.ecuName = P;
    }
    public String getmanufactureDate() {
        return manufactureDate;
    }
    public void setmanufactureDate(String P) {
        this.manufactureDate = P;
    }

    public String getsupplierId() {
        return supplierId;
    }
    public void setsupplierId(String P) {
        this.supplierId = P;
    }

    public String getpartNumber() {
        return partNumber;
    }
    public void setpartNumber(String P) {
        this.partNumber = P;
    }

    public String getsoftwareAss() {
        return softwareAss;
    }
    public void setsoftwareAss(String P) {
        this.softwareAss = P;
    }

    public String gethardwareVersion() {
        return hardwareVersion;
    }
    public void sethardwareVersion(String P) {
        this.hardwareVersion = P;
    }

    public String getsoftwareVersion() {
        return softwareVersion;
    }
    public void setsoftwareVersion(String P) {
        this.softwareVersion = P;
    }

    public String geteolConfigCode() {
        return eolConfigCode;
    }
    public void seteolConfigCode(String P) {
        this.eolConfigCode = P;
    }

    public String getecode() {
        return ecode;
    }
    public void setecode(String P) {
        this.ecode = P;
    }

    public String getserialNumber() {
        return serialNumber;
    }
    public void setserialNumber(String P) {
        this.serialNumber = P;
    }

    public String getdriverFileVersion() {
        return driverFileVersion;
    }
    public void setdriverFileVersion(String P) {
        this.driverFileVersion = P;
    }

    public String getdriverFileVersionLatest() {
        return driverFileVersionLatest;
    }
    public void setdriverFileVersionLatest(String P) {
        this.driverFileVersionLatest = P;
    }

    public String getdriverFileName() {
        return driverFileName;
    }
    public void setdriverFileName(String P) {
        //过滤+号
        P = P.replace('+', ' ');
        this.driverFileName = P;
    }

    public String getdriverServerPath() {
        return driverServerPath;
    }
    public void setdriverServerPath(String P) {
        this.driverServerPath = P;
    }

    public String getappFileVersion() {
        return appFileVersion;
    }
    public void setappFileVersion(String P) {
        this.appFileVersion = P;
    }

    public String getappFileVersionLatest() {
        return appFileVersionLatest;
    }
    public void setappFileVersionLatest(String P) {
        this.appFileVersionLatest = P;
    }

    public String getappFileName() {
        return appFileName;
    }
    public void setappFileName(String P) {
        //过滤+号
        P = P.replace('+', ' ');
        this.appFileName = P;
    }

    public String getappServerPath() {
        return appServerPath;
    }
    public void setappServerPath(String P) {
        this.appServerPath = P;
    }

    public String getcalFileVersion() {
        return calFileVersion;
    }
    public void setcalFileVersion(String P) {
        this.calFileVersion = P;
    }

    public String getcalFileVersionLatest() {
        return calFileVersionLatest;
    }
    public void setcalFileVersionLatest(String P) {
        this.calFileVersionLatest = P;
    }

    public String getcalFileName() {
        return calFileName;
    }
    public void setcalFileName(String P) {
        //过滤+号
        P = P.replace('+', ' ');
        this.calFileName = P;
    }
    public String getcalServerPath() {
        return calServerPath;
    }
    public void setcalServerPath(String P) {
        this.calServerPath = P;
    }
    public String getapp_MD5() {
        return app_MD5;
    }
    public void setapp_MD5(String P) {
        this.app_MD5 = P;
    }
    public String getcal_MD5() {
        return cal_MD5;
    }
    public void setcal_MD5(String P) {
        this.cal_MD5 = P;
    }
    public String getdriver_MD5() {
        return driver_MD5;
    }
    public void setdriver_MD5(String P) {
        this.driver_MD5 = P;
    }
    public String geticcid() {
        return iccid;
    }
    public void seticcid(String P) {
        this.iccid = P;
    }
    public String getvin() {
        return vin;
    }
    public void setvin(String P) {
        this.vin = P;
    }
}

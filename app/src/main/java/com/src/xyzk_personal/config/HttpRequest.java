package com.src.xyzk_personal.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.omg.CORBA.NameValuePair;

import android.util.Log;

public class HttpRequest {
	private final static String TAG = "HttpRequest";
	/**
	 * 向指定URL发送GET方法的请求
	 *
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
	public static String sendGetJson(String url, String param) {
		String result = "";
		PrintWriter Pout = null;
		BufferedReader in = null;
		try {
			String urlNameString = url;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("user-agent","ApiPOST Runtime +https://www.apipost.cn");

			connection.setDoOutput(true);
			connection.setDoInput(true);
			//input
			Pout = new PrintWriter(connection.getOutputStream());
			Pout.println(param);
			Pout.flush();
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 *
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！"+e);
			e.printStackTrace();
		}
		//使用finally块来关闭输出流、输入流
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static String sendPost2(String url){
		String result ="";
		try {
			HttpURLConnection conn=(HttpURLConnection) new URL(url).openConnection();
			conn.setConnectTimeout(3000);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			//conn.setRequestProperty("Content-Length",String.valueOf(par.getBytes().length));
			OutputStream out=conn.getOutputStream();
			//out.write(par.getBytes());
			BufferedReader br;
			String line="";
			Log.e("HttpRequest", "code="+conn.getResponseCode());
			//测试返回500
			if(conn.getResponseCode()==HttpURLConnection.HTTP_OK){
				InputStream in=conn.getInputStream();
				br=new BufferedReader(new InputStreamReader(in));
				while((line=br.readLine())!=null){
					result+=line;
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	/*
	public static String sendPost3(String url,String par){
		String result ="";
		try {
			HttpPost post=new HttpPost(url);
			ArrayList<NameValuePair> pars=new ArrayList<NameValuePair>();
			pars.add(new BasicNameValuePair("json", par));
			post.setEntity(new UrlEncodedFormEntity(pars,HTTP.UTF_8));
			HttpResponse http=new DefaultHttpClient().execute(post);
			Log.e("tedu", "result="+http.getStatusLine().getStatusCode());
			Log.e("tedu", "result="+EntityUtils.toString(http.getEntity()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
*/

	public static String sendGetBufferedReader(String url, String param, String savePath){
		String result = "";
		FileOutputStream output = null;
		try {
			String urlNameString = url + "?" + param;
//			System.out.println(urlNameString);
			URL realUrl = new URL(urlNameString);
			// 打开网络连接
			HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
			HttpURLConnection connection1 = (HttpURLConnection)realUrl.openConnection();
			connection.setRequestMethod("GET");
			//设置超时时间和读写超时时间
			connection.setConnectTimeout(80000);
			connection.setReadTimeout(50000);
			connection.setRequestProperty("Accept-Encoding", "gzip, deflate,br");
			connection.connect();
			BufferedReader br;
			String line="";
			if(connection1.getResponseCode()==HttpURLConnection.HTTP_OK){
				InputStream in=connection1.getInputStream();
				br=new BufferedReader(new InputStreamReader(in));
				line=br.readLine();
				result+=line;
			}
			int statuscode;
			if (result.indexOf("code") > 0){
				statuscode =500;
			}else statuscode=200;
			if (statuscode == 200) {
				InputStream input = connection.getInputStream();
				File file = null;
				file = new File(savePath);
				// 创建文件夹
				String fileDir = savePath.substring(0, savePath.lastIndexOf("/"));
				File fileDirFile = new File(fileDir);
				if (!fileDirFile.exists())
				{
					fileDirFile.mkdirs();
				}
				output = new FileOutputStream(file);
				byte[] buffer = new byte[1024];

				int j = 0;
				while ((j=input.read(buffer)) != -1) {
					output.write(buffer,0,j);
					output.flush();
				}
			}
		} catch(Exception e){
			System.out.println("使用get方式获取下载文件异常" + e);
			e.printStackTrace();
		}
		// 关闭流
		finally{
			try {
				if (output != null) {
					output.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	public static String sendPostToMyMes(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("user-agent","ApiPOST Runtime +https://www.apipost.cn");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！"+e);
			e.printStackTrace();
		}
		//使用finally块来关闭输出流、输入流
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}
	public static String sendPut(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
			conn.setRequestMethod("PUT");
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			conn.setRequestProperty("Content-Type","application/json");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 PUT 请求出现异常！"+e);
			e.printStackTrace();
		}
		//使用finally块来关闭输出流、输入流
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		return result;
	}
}

package com.src.xyzk_personal.config;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlContentHandler extends DefaultHandler{
    private List<Ecu> ecus;
    private Ecu ecu;
    private String preTag;
    //其他数据
    private String vin;
    private String modelName;
    private String variantCoding;
    private String pin;
    private String bodyColor;
    private String esk;
    private String lfId;
    private String lrId;
    private String rrId;
    private String rfId;

    public String getvin() {
        return vin;
    }
    public String getmodelName() {
        return modelName;
    }
    public String getvariantCoding() {
        return variantCoding;
    }
    public String getpin() {
        return pin;
    }
    public String getbodyColor() {
        return bodyColor;
    }
    public String getesk() {
        return esk;
    }
    public String getlfId() {
        return lfId;
    }
    public String getlrId() {
        return lrId;
    }
    public String getrrId() {
        return rrId;
    }
    public String getrfId() {
        return rfId;
    }

    @Override
    public void startDocument() throws SAXException {
        // TODO Auto-generated method stub
        ecus = new ArrayList<Ecu>();
        vin = "";
        modelName = "";
        variantCoding = "";
        pin = "";
        bodyColor = "";
        esk = "";
        lfId = "";
        lrId = "";
        rrId = "";
        rfId = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        // TODO Auto-generated method stub
        if(ecu != null)
        {
            String p = new String(ch,start,length);
            if ("ecuName".equals(preTag)) {
                ecu.setecuName(p);
            }else if ("manufactureDate".equals(preTag)) {
                ecu.setmanufactureDate(p);
            }else if ("supplierId".equals(preTag)) {
                ecu.setsupplierId(p);
            }
            else if ("partNumber".equals(preTag)) {
                ecu.setpartNumber(p);
            }
            else if ("softwareAss".equals(preTag)) {
                ecu.setsoftwareAss(p);
            }
            else if ("hardwareVersion".equals(preTag)) {
                ecu.sethardwareVersion(p);
            }
            else if ("softwareVersion".equals(preTag)) {
                ecu.setsoftwareVersion(p);
            }
            else if ("eolConfigCode".equals(preTag)) {
                ecu.seteolConfigCode(p);
            }
            else if ("ecode".equals(preTag)) {
                ecu.setecode(p);
            }
            else if ("serialNumber".equals(preTag)) {
                ecu.setserialNumber(p);
            }
            else if ("driverFileVersion".equals(preTag)) {
                ecu.setdriverFileVersion(p);
            }
            else if ("driverFileVersionLatest".equals(preTag)) {
                ecu.setdriverFileVersionLatest(p);
            }
            else if ("driverFileName".equals(preTag)) {
                ecu.setdriverFileName(p);
            }
            else if ("driverServerPath".equals(preTag)) {
                ecu.setdriverServerPath(p);
            }
            else if ("appFileVersion".equals(preTag)) {
                ecu.setappFileVersion(p);
            }
            else if ("appFileVersionLatest".equals(preTag)) {
                ecu.setappFileVersionLatest(p);
            }
            else if ("appFileName".equals(preTag)) {
                ecu.setappFileName(p);
            }
            else if ("appServerPath".equals(preTag)) {
                ecu.setappServerPath(p);
            }
            else if ("calFileVersion".equals(preTag)) {
                ecu.setcalFileVersion(p);
            }
            else if ("calFileVersionLatest".equals(preTag)) {
                ecu.setcalFileVersionLatest(p);
            }
            else if ("calFileName".equals(preTag)) {
                ecu.setcalFileName(p);
            }
            else if ("calServerPath".equals(preTag)) {
                ecu.setcalServerPath(p);
            }
        }
        else  //处理其他
        {
            String p = new String(ch,start,length);
            if ("vin".equals(preTag)) {
                vin = p;
            }
            else if ("modelName".equals(preTag)) {
                modelName = p;
            }
            else if ("variantCoding".equals(preTag)) {
                variantCoding = p;
            }
            else if ("pin".equals(preTag)) {
                pin = p;
            }
            else if ("bodyColor".equals(preTag)) {
                bodyColor = p;
            }
            else if ("esk".equals(preTag)) {
                esk = p;
            }
            else if ("lfId".equals(preTag)) {
                lfId = p;
            }
            else if ("lrId".equals(preTag)) {
                lrId = p;
            }
            else if ("rrId".equals(preTag)) {
                rrId = p;
            }
            else if ("rfId".equals(preTag)) {
                rfId = p;
            }

        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        // TODO Auto-generated method stub
        if ("ecu".equals(localName)) {
            ecu= new Ecu();
        }
        preTag = localName;
    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        // TODO Auto-generated method stub
        if (ecu != null && "ecu".equals(localName)) {
            ecus.add(ecu);
            ecu = null;
        }
        preTag = null;
    }
    public List<Ecu> getEcus() {
        return ecus;
    }
}

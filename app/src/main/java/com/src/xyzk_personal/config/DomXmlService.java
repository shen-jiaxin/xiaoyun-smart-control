package com.src.xyzk_personal.config;

import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;


public class DomXmlService {
    private List<Ecu> ecus;
    private Ecu ecu;
    private String preTag;
    //其他数据
    private String vin;
    private String modelName;
    private String variantCoding;
    private String pin;
    private String bodyColor;
    private String esk;
    private String lfId;
    private String lrId;
    private String rrId;
    private String rfId;
    public DomXmlService()
    {
        ecus = new ArrayList<Ecu>();
        vin = "";
        modelName = "";
        variantCoding = "";
        pin = "";
        bodyColor = "";
        esk = "";
        lfId = "";
        lrId = "";
        rrId = "";
        rfId = "";
    }
    public String getvin() {
        return vin;
    }
    public String getmodelName() {
        return modelName;
    }
    public String getvariantCoding() {
        return variantCoding;
    }
    public String getpin() {
        return pin;
    }
    public String getbodyColor() {
        return bodyColor;
    }
    public String getesk() {
        return esk;
    }
    public String getlfId() {
        return lfId;
    }
    public String getlrId() {
        return lrId;
    }
    public String getrrId() {
        return rrId;
    }
    public String getrfId() {
        return rfId;
    }
    public List<Ecu> getEcus() {
        return ecus;
    }

    public void getPersonsNew(InputStream inputStream) throws Exception
    {
        //获取工厂对象，以及通过DOM工厂对象获取DOMBuilder对象
        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        //解析XML输入流，得到Document对象，表示一个XML文档
        Document document=builder.parse(inputStream);
        //获得文档中的次以及节点，persons
        Element element=document.getDocumentElement();
        // 获取Element下一级的person节点集合，以NodeList的形式存放。
//        NodeList ecuNodes=element.getElementsByTagName("ecu");
        //解析其他数据
        NodeList mainNodes=element.getChildNodes();
        for(int j=0;j<mainNodes.getLength();j++)
        {
            //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
            if(mainNodes.item(j).getNodeType()==Node.ELEMENT_NODE)
            {
                Node node = mainNodes.item(j).getFirstChild();
                if(node == null) continue;
                String p = node.getNodeValue();
                if(p == null) continue;
                String name = mainNodes.item(j).getNodeName();
                int determine = name.indexOf(":");
                name=name.substring(determine+1,name.length());
                if("vin".equals(name))
                {
                    vin = p;
                }
                else if("model_name".equals(name))
                {
                    modelName = p;
                }
                else if("pin".equals(name))
                {
                    pin = p;
                }
                else if("body_color".equals(name))
                {
                    bodyColor = p;
                }
                else if("esk".equals(name))
                {
                    esk = p;
                }
                else if("lfId".equals(name))
                {
                    lfId = p;
                }
                else if("lrId".equals(name))
                {
                    lrId = p;
                }
                else if("rrId".equals(name))
                {
                    rrId = p;
                }
                else if("rfId".equals(name))
                {
                    rfId = p;
                } else if ("ecuArray".equals(name)) {
                    //遍历ECU数据
                    //循环获取索引为i的person节点
                    Element personElement=(Element) mainNodes.item(j);
                    ecu =new Ecu();
                    //通过属性名，获取节点的属性id
                    //ecu.setId(Integer.parseInt(personElement.getAttribute("id")));
                    //获取索引i的person节点下的子节点集合
                    NodeList childNodes=personElement.getChildNodes();
                    for(int i=0;i<childNodes.getLength();i++)
                    {
                        //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
                        if(childNodes.item(i).getNodeType()==Node.ELEMENT_NODE)
                        {
                            Node nodeecu = childNodes.item(i).getFirstChild();
                            if(nodeecu == null) continue;
                            String value = nodeecu.getNodeValue();
                            if(value == null) continue;
                            String ECUname = childNodes.item(i).getNodeName();
                            int ECUdetermine = ECUname.indexOf(":");
                            ECUname=ECUname.substring(ECUdetermine+1,ECUname.length());
                            if("ecu_name".equals(ECUname))
                            {
                                ecu.setecuName(value);
                            }
                            else if("manufacture_date".equals(ECUname))
                            {
                                ecu.setmanufactureDate(value);
                            }
                            else if("supplier_id".equals(ECUname))
                            {
                                ecu.setsupplierId(value);
                            }
                            else if("part_number".equals(ECUname))
                            {
                                ecu.setpartNumber(value);
                            }
                            else if("software_ass".equals(ECUname))
                            {
                                ecu.setsoftwareAss(value);
                            }
                            else if("hardware_version".equals(ECUname))
                            {
                                ecu.sethardwareVersion(value);
                            }
                            else if("software_version".equals(ECUname))
                            {
                                ecu.setsoftwareVersion(value);
                            }
                            else if("eol_config_code".equals(ECUname))
                            {
                                ecu.seteolConfigCode(value);
                            }
                            else if("ecode".equals(ECUname))
                            {
                                ecu.setecode(value);
                            }
                            else if("serial_number".equals(ECUname))
                            {
                                ecu.setserialNumber(value);
                            }
                            else if("driver_file_version".equals(ECUname))
                            {
                                ecu.setdriverFileVersion(value);
                            }
                            else if("driver_file_version_latest".equals(ECUname))
                            {
                                ecu.setdriverFileVersionLatest(value);
                            }
                            else if("driver_file_path".equals(ECUname))
                            {
                                ecu.setdriverServerPath(value);
                                //获取到
                                if(value.length() > 0)
                                {
                                    int v_end = value.indexOf("%BB%B6%2F",0);
                                    if(v_end > 0)
                                    {
                                        ecu.setdriverFileName(value.substring(v_end + 9));
                                    }
                                }
                            }
                            else if("app_file_version".equals(ECUname))
                            {
                                ecu.setappFileVersion(value);
                            }
                            else if("app_file_version_latest".equals(ECUname))
                            {
                                ecu.setappFileVersionLatest(value);
                            }
                            else if("app_file_path".equals(ECUname))
                            {
                                ecu.setappServerPath(value);
                                //获取到
                                if(value.length() > 0)
                                {
                                    int v_end = value.indexOf("%BB%B6%2F",0);
                                    if(v_end > 0)
                                    {
                                        ecu.setappFileName(value.substring(v_end + 9));
                                    }
                                }
                            }
                            else if("cal_file_version".equals(ECUname))
                            {
                                ecu.setcalFileVersion(value);
                            }
                            else if("cal_file_version_latest".equals(ECUname))
                            {
                                ecu.setcalFileVersionLatest(value);
                            }
                            else if("cal_file_path".equals(ECUname))
                            {
                                ecu.setcalFileName(value);
                            }
                            else if("cal_file_path".equals(ECUname))
                            {
                                ecu.setcalServerPath(value);
                            }
                            else if("iccid_code".equals(ECUname))
                            {
                                ecu.seticcid(value);
                            }
                        }
                    }
                    //把解析的person对象加入的list集合中
                    ecus.add(ecu);
                }
            }
        }
    }

    public void getPersons(InputStream inputStream) throws Exception
    {
        //获取工厂对象，以及通过DOM工厂对象获取DOMBuilder对象
        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        //解析XML输入流，得到Document对象，表示一个XML文档
        Document document=builder.parse(inputStream);
        //获得文档中的次以及节点，persons
        Element element=document.getDocumentElement();
        // 获取Element下一级的person节点集合，以NodeList的形式存放。
        NodeList ecuNodes=element.getElementsByTagName("ecu");
        for(int i=0;i<ecuNodes.getLength();i++)
        {
            //循环获取索引为i的person节点
            Element personElement=(Element) ecuNodes.item(i);
            ecu =new Ecu();
            //通过属性名，获取节点的属性id
            //ecu.setId(Integer.parseInt(personElement.getAttribute("id")));
            //获取索引i的person节点下的子节点集合
            NodeList childNodes=personElement.getChildNodes();
            for(int j=0;j<childNodes.getLength();j++)
            {
                //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
                if(childNodes.item(j).getNodeType()==Node.ELEMENT_NODE)
                {
                    Node node = childNodes.item(j).getFirstChild();
                    if(node == null) continue;
                    String p = node.getNodeValue();
                    if(p == null) continue;
                    if("ecuName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setecuName(p);
                    }
                    else if("manufactureDate".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setmanufactureDate(p);
                    }
                    else if("supplierId".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsupplierId(p);
                    }
                    else if("partNumber".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setpartNumber(p);
                    }
                    else if("softwareAss".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsoftwareAss(p);
                    }
                    else if("hardwareVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.sethardwareVersion(p);
                    }
                    else if("softwareVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsoftwareVersion(p);
                    }
                    else if("eolConfigCode".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.seteolConfigCode(p);
                    }
                    else if("ecode".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setecode(p);
                    }
                    else if("serialNumber".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setserialNumber(p);
                    }
                    else if("driverFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileVersion(p);
                    }
                    else if("driverFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileVersionLatest(p);
                    }
                    else if("driverFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileName(p);
                    }
                    else if("driverServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverServerPath(p);
                    }
                    else if("appFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileVersion(p);
                    }
                    else if("appFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileVersionLatest(p);
                    }
                    else if("appFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileName(p);
                    }
                    else if("appServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappServerPath(p);
                    }
                    else if("calFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileVersion(p);
                    }
                    else if("calFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileVersionLatest(p);
                    }
                    else if("calFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileName(p);
                    }
                    else if("calServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalServerPath(p);
                    }
                    else if("iccid_code".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.seticcid(p);
                    }
                }
            }
            //把解析的person对象加入的list集合中
            ecus.add(ecu);
        }
        //解析其他数据
        NodeList mainNodes=element.getChildNodes();
        for(int j=0;j<mainNodes.getLength();j++)
        {
            //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
            if(mainNodes.item(j).getNodeType()==Node.ELEMENT_NODE)
            {
                Node node = mainNodes.item(j).getFirstChild();
                if(node == null) continue;
                String p = node.getNodeValue();
                if(p == null) continue;
                if("vin".equals(mainNodes.item(j).getNodeName()))
                {
                    vin = p;
                }
                else if("modelName".equals(mainNodes.item(j).getNodeName()))
                {
                    modelName = p;
                }
                else if("pin".equals(mainNodes.item(j).getNodeName()))
                {
                    pin = p;
                }
                else if("bodyColor".equals(mainNodes.item(j).getNodeName()))
                {
                    bodyColor = p;
                }
                else if("esk".equals(mainNodes.item(j).getNodeName()))
                {
                    esk = p;
                }
                else if("lfId".equals(mainNodes.item(j).getNodeName()))
                {
                    lfId = p;
                }
                else if("lrId".equals(mainNodes.item(j).getNodeName()))
                {
                    lrId = p;
                }
                else if("rrId".equals(mainNodes.item(j).getNodeName()))
                {
                    rrId = p;
                }
                else if("rfId".equals(mainNodes.item(j).getNodeName()))
                {
                    rfId = p;
                }
            }
        }
    }
    public void getPersonsMes(String input) throws Exception
    {
        StringReader sr = new StringReader(input);
        InputSource is = new InputSource(sr);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        //解析XML输入流，得到Document对象，表示一个XML文档
        Document document = builder.parse(is);
        //获得文档中的次以及节点，persons
        Element element=document.getDocumentElement();
        // 获取Element下一级的person节点集合，以NodeList的形式存放。
        NodeList ecuNodes=element.getElementsByTagName("ecu");
        for(int i=0;i<ecuNodes.getLength();i++)
        {
            //循环获取索引为i的person节点
            Element personElement=(Element) ecuNodes.item(i);
            ecu =new Ecu();
            //通过属性名，获取节点的属性id
            //ecu.setId(Integer.parseInt(personElement.getAttribute("id")));
            //获取索引i的person节点下的子节点集合
            NodeList childNodes=personElement.getChildNodes();
            for(int j=0;j<childNodes.getLength();j++)
            {
                //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
                if(childNodes.item(j).getNodeType()==Node.ELEMENT_NODE)
                {
                    Node node = childNodes.item(j).getFirstChild();
                    if(node == null) continue;
                    String p = node.getNodeValue();
                    if(p == null) continue;
                    if("ecuName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setecuName(p);
                    }
                    else if("manufactureDate".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setmanufactureDate(p);
                    }
                    else if("supplierId".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsupplierId(p);
                    }
                    else if("partNumber".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setpartNumber(p);
                    }
                    else if("softwareAss".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsoftwareAss(p);
                    }
                    else if("hardwareVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.sethardwareVersion(p);
                    }
                    else if("softwareVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setsoftwareVersion(p);
                    }
                    else if("eolConfigCode".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.seteolConfigCode(p);
                    }
                    else if("ecode".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setecode(p);
                    }
                    else if("serialNumber".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setserialNumber(p);
                    }
                    else if("driverFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileVersion(p);
                    }
                    else if("driverFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileVersionLatest(p);
                    }
                    else if("driverFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverFileName(p);
                    }
                    else if("driverServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setdriverServerPath(p);
                    }
                    else if("appFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileVersion(p);
                    }
                    else if("appFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileVersionLatest(p);
                    }
                    else if("appFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappFileName(p);
                    }
                    else if("appServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setappServerPath(p);
                    }
                    else if("calFileVersion".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileVersion(p);
                    }
                    else if("calFileVersionLatest".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileVersionLatest(p);
                    }
                    else if("calFileName".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalFileName(p);
                    }
                    else if("calServerPath".equals(childNodes.item(j).getNodeName()))
                    {
                        ecu.setcalServerPath(p);
                    }
                }
            }
            //把解析的person对象加入的list集合中
            ecus.add(ecu);
        }
        //解析其他数据
        NodeList mainNodes=element.getChildNodes();
        for(int j=0;j<mainNodes.getLength();j++)
        {
            //循环遍历每个person下的子节点，如果判断节点类型是ELEMENT_NODE，就可以依据节点名称给予解析
            if(mainNodes.item(j).getNodeType()==Node.ELEMENT_NODE)
            {
                Node node = mainNodes.item(j).getFirstChild();
                if(node == null) continue;
                String p = node.getNodeValue();
                if(p == null) continue;
                if("vin".equals(mainNodes.item(j).getNodeName()))
                {
                    vin = p;
                }
                else if("modelName".equals(mainNodes.item(j).getNodeName()))
                {
                    modelName = p;
                }
                else if("pin".equals(mainNodes.item(j).getNodeName()))
                {
                    pin = p;
                }
                else if("bodyColor".equals(mainNodes.item(j).getNodeName()))
                {
                    bodyColor = p;
                }
                else if("esk".equals(mainNodes.item(j).getNodeName()))
                {
                    esk = p;
                }
                else if("lfId".equals(mainNodes.item(j).getNodeName()))
                {
                    lfId = p;
                }
                else if("lrId".equals(mainNodes.item(j).getNodeName()))
                {
                    lrId = p;
                }
                else if("rrId".equals(mainNodes.item(j).getNodeName()))
                {
                    rrId = p;
                }
                else if("rfId".equals(mainNodes.item(j).getNodeName()))
                {
                    rfId = p;
                }
            }
        }

    }
    public boolean getPersonsMesJson(String input) throws Exception
    {
        int Padr = 0;
        //int Pmaxlen = input.length();
        int v_start = 0;
        int v_end = 0;
        Ecu ecu = null;
        do
        {
            Padr = input.indexOf("ecuArray=ECU", Padr);
            if(Padr > 0)
            {
                //查找 {}这个区间的数据
                v_start = input.indexOf("{", Padr);
                v_end = input.indexOf("};", Padr);
                if((v_end > v_start) && (v_start > 0) && (v_end > 0))
                {
                    String v_ecu = input.substring(v_start + 1, v_end);
                    if(v_ecu != null) //解析模块数据
                    {
                        ecu = GetEcuInfo(v_ecu);
                        ecus.add(ecu);
                    }
                }
                Padr = v_end;
            }
            else
                break;
        }
        while(Padr > 0);
        //解析其他数据
        //颜色
        v_start = input.indexOf("body_color=", 0);
        if(v_start > 0)
        {
            v_end = input.indexOf(";", v_start);
            if(v_end > 0)
                bodyColor = input.substring(v_start + 11,v_end);
        }
        //esk
        for(int i = 0; i < 16; i ++)
        {
            v_start = input.indexOf("esk=", v_start);
            if(v_start > 0)
            {
                v_end = input.indexOf(";", v_start);
                if(v_end > 0)
                {
                    String sk = input.substring(v_start + 4,v_end);
                    if(sk.length() == 1)
                        esk += "0" + sk;
                    else
                        esk += sk;
                }
                v_start = v_end;
            }
        }
        //model name
        v_start = input.indexOf("model_name=", v_start);
        if(v_start > 0)
        {
            v_end = input.indexOf(";", v_start);
            if(v_end > 0)
                modelName = input.substring(v_start + 11,v_end);
        }
        //pin
        v_start = input.indexOf("pin=", v_start);
        if(v_start > 0)
        {
            v_end = input.indexOf(";", v_start);
            if(v_end > 0)
                pin = input.substring(v_start + 4,v_end);
        }
        //vin
        v_start = input.indexOf("vin=", v_start);
        if(v_start > 0)
        {
            v_end = input.indexOf(";", v_start);
            if(v_end > 0)
                vin = input.substring(v_start + 4,v_end);
        }

        return true;
    }
    public boolean getDataFromEolJson(String Pstr) throws Exception
    {
        JSONObject obj = new JSONObject(Pstr);
        Ecu ecu = null;
        if(obj == null) return false;
        JSONObject objdata = new JSONObject(obj.getString("data"));
        if(objdata == null) return false;
        vin = objdata.getString("vin");
        modelName = objdata.getString("modelName");
        bodyColor = objdata.getString("bodyColor");
        esk = objdata.getString("esk");
        pin = objdata.getString("pin");
        //lfId = objdata.getString("flid");
        String value = "";
        int v_end = 0;
        JSONArray ObjecuList = new JSONArray(objdata.getString("tableModuleEcuarrays"));
        int nums = ObjecuList.length();
        for(int i =0; i < nums; i ++)
        {
            String ss = ObjecuList.get(i).toString();
            JSONObject ObjSon = new JSONObject(ss);
            ecu = new Ecu();
            ecu.setecuName(ObjSon.getString("ecuName"));
            ecu.setecode(ObjSon.getString("ecode"));
            ecu.seteolConfigCode(ObjSon.getString("eolConfigCode"));
            ecu.sethardwareVersion(ObjSon.getString("hardwareVersion"));
            ecu.setsoftwareVersion(ObjSon.getString("softwareVersion"));
            ecu.seticcid(ObjSon.getString("iccidCode"));
            ecu.setmanufactureDate(ObjSon.getString("manufactureDate"));
            ecu.setpartNumber(ObjSon.getString("partNumber"));
            ecu.setserialNumber(ObjSon.getString("serialNumber"));
            ecu.setsoftwareAss(ObjSon.getString("softwareAss"));
            ecu.setsupplierId(ObjSon.getString("supplierId"));
            //file
            value = ObjSon.getString("appFilePath");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setappFileName(value.substring(v_end + 9));
                else
                    ecu.setappFileName(value);
            }
            ecu.setappServerPath(value);
            ecu.setapp_MD5(ObjSon.getString("appMd5"));
            ecu.setappFileVersionLatest(ObjSon.getString("appFileVersionLatest"));
            ecu.setcal_MD5(ObjSon.getString("calMd5"));
            value = ObjSon.getString("calFilePath");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setcalFileName(value.substring(v_end + 9));
                else
                    ecu.setcalFileName(value);
            }
            ecu.setcalServerPath(value);
            ecu.setcalFileVersionLatest(ObjSon.getString("calFileVersionLatest"));
            ecu.setdriver_MD5(ObjSon.getString("driverMd5"));
            value = ObjSon.getString("driverFilePath");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setdriverFileName(value.substring(v_end + 9));
                else
                    ecu.setdriverFileName(value);
            }
            ecu.setdriverServerPath(value);
            ecu.setdriverFileVersionLatest(ObjSon.getString("driverFileVersionLatest"));
            ecus.add(ecu);
        }
        return true;
    }
    public boolean getDataFromThaiMesJson(String Pstr) throws Exception
    {
        JSONObject obj = new JSONObject(Pstr);
        Ecu ecu = null;
        if(obj == null) return false;
        JSONObject objdata = new JSONObject(obj.getString("Outputs"));
        if(objdata == null) return false;
        vin = objdata.getString("VIN");
        modelName = objdata.getString("modelName");
        bodyColor = objdata.getString("bodyColor");
        esk = objdata.getString("esk");
        pin = objdata.getString("pin");
        //lfId = objdata.getString("flid");
        String value = "";
        int v_end = 0;
        JSONArray ObjecuList = new JSONArray(objdata.getString("TABLEMODULEECUARRAYS"));
        int nums = ObjecuList.length();
        for(int i =0; i < nums; i ++)
        {
            String ss = ObjecuList.get(i).toString();
            JSONObject ObjSon = new JSONObject(ss);
            ecu = new Ecu();
            ecu.setecuName(ObjSon.getString("ECU_NAME"));
            ecu.setecode(ObjSon.getString("ECODE"));
            ecu.seteolConfigCode(ObjSon.getString("EOL_CONFIG_CODE"));
            ecu.sethardwareVersion(ObjSon.getString("HARD_WAREVERSION"));
            ecu.setsoftwareVersion(ObjSon.getString("SOFT_WAREVERSION"));
            ecu.seticcid(ObjSon.getString("ICCID_CODE"));
            ecu.setmanufactureDate(ObjSon.getString("MANUFACTURE_DATE"));
            ecu.setpartNumber(ObjSon.getString("PART_NUMBER"));
            ecu.setserialNumber(ObjSon.getString("SERIAL_NUMBER"));
            ecu.setsoftwareAss(ObjSon.getString("SOFTWARE_ASS"));
            ecu.setsupplierId(ObjSon.getString("SUPPLIERID"));
            //file
            value = ObjSon.getString("APP_FILE_PATH");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setappFileName(value.substring(v_end + 9));
                else
                    ecu.setappFileName(value);
            }
            ecu.setappServerPath(value);
            ecu.setapp_MD5(ObjSon.getString("APP_MD5"));
            ecu.setappFileVersionLatest(ObjSon.getString("APP_FILE_VERSION_LATEST"));
            ecu.setcal_MD5(ObjSon.getString("CAL_MD5"));
            value = ObjSon.getString("CAL_FILE_PATH");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setcalFileName(value.substring(v_end + 9));
                else
                    ecu.setcalFileName(value);
            }
            ecu.setcalServerPath(value);
            ecu.setcalFileVersionLatest(ObjSon.getString("CAL_FILE_VERSION_LATEST"));
            ecu.setdriver_MD5(ObjSon.getString("DRIVER_MD5"));
            value = ObjSon.getString("DRIVER_FILE_PATH");
            if(value.length() > 0)
            {
                v_end = value.indexOf("%BB%B6%2F",0);
                if(v_end > 0)
                    ecu.setdriverFileName(value.substring(v_end + 9));
                else
                    ecu.setdriverFileName(value);
            }
            ecu.setdriverServerPath(value);
            ecu.setdriverFileVersionLatest(ObjSon.getString("DRIVER_FILE_VERSION_LATEST"));
            ecus.add(ecu);
        }
        return true;
    }
    //解析模块字符串
    private Ecu GetEcuInfo(String Pdata)
    {
        Ecu ecu = new Ecu();
        int v_adr = 0;
        int v_end = 0;
        do
        {
            v_end = Pdata.indexOf(";", 0);
            if(v_end > 0)
            {
                String item = Pdata.substring(0, v_end);
                Pdata = Pdata.substring(v_end + 2);
                //区分名字和值
                String name = "";
                String value = "";
                v_adr = item.indexOf("=");
                if(v_adr > 0)
                {
                    name = item.substring(0,v_adr);
                    value = item.substring(v_adr + 1);
                    if(value.equals("null"))
                        value = "";
                    if("app_MD5".equals(name))
                    {
                        ecu.setapp_MD5(value);
                    }
                    else if("app_file_path".equals(name))
                    {
                        ecu.setappServerPath(value);
                        //获取到
                        if(value.length() > 0)
                        {
                            v_end = value.indexOf("%BB%B6%2F",0);
                            if(v_end > 0)
                            {
                                ecu.setappFileName(value.substring(v_end + 9));
                            }
                        }
                    }
                    else if("app_file_version".equals(name))
                    {
                        ecu.setappFileVersion(value);
                    }
                    else if("app_file_version_latest".equals(name))
                    {
                        ecu.setappFileVersionLatest(value);
                    }
                    else if("appFileName".equals(name))
                    {
                        ecu.setappFileName(value);
                    }
                    else if("cal_MD5".equals(name))
                    {
                        ecu.setcal_MD5(value);
                    }
                    else if("cal_file_path".equals(name))
                    {
                        ecu.setcalServerPath(value);
                        //获取到
                        if(value.length() > 0)
                        {
                            v_end = value.indexOf("%BB%B6%2F",0);
                            if(v_end > 0)
                            {
                                ecu.setcalFileName(value.substring(v_end + 9));
                            }
                        }
                    }
                    else if("cal_file_version".equals(name))
                    {
                        ecu.setcalFileVersion(value);
                    }
                    else if("cal_file_version_latest".equals(name))
                    {
                        ecu.setcalFileVersionLatest(value);
                    }
                    else if("calFileName".equals(name))
                    {
                        ecu.setcalFileName(value);
                    }
                    else if("driver_MD5".equals(name))
                    {
                        ecu.setdriver_MD5(value);
                    }
                    else if("driver_file_path".equals(name))
                    {
                        ecu.setdriverServerPath(value);
                        //获取到
                        if(value.length() > 0)
                        {
                            v_end = value.indexOf("%BB%B6%2F",0);
                            if(v_end > 0)
                            {
                                ecu.setdriverFileName(value.substring(v_end + 9));
                            }
                        }
                    }
                    else if("driver_file_version".equals(name))
                    {
                        ecu.setdriverFileVersion(value);
                    }
                    else if("driver_file_version_latest".equals(name))
                    {
                        ecu.setdriverFileVersionLatest(value);
                    }
                    else if("driverFileName".equals(name))
                    {
                        ecu.setdriverFileName(value);
                    }
                    else if("ecode".equals(name))
                    {
                        ecu.setecode(value);
                    }
                    else if("ecu_name".equals(name))
                    {
                        ecu.setecuName(value);
                    }
                    else if("eol_config_code".equals(name))
                    {
                        ecu.seteolConfigCode(value);
                    }
                    else if("hardware_version".equals(name))
                    {
                        ecu.sethardwareVersion(value);
                    }
                    else if("iccid_code".equals(name))
                    {
                        ecu.seticcid(value);
                    }
                    else if("manufacture_date".equals(name))
                    {
                        ecu.setmanufactureDate(value);
                    }
                    else if("part_number".equals(name))
                    {
                        ecu.setpartNumber(value);
                    }
                    else if("serial_number".equals(name))
                    {
                        ecu.setserialNumber(value);
                    }
                    else if("software_ass".equals(name))
                    {
                        ecu.setsoftwareAss(value);
                    }
                    else if("software_version".equals(name))
                    {
                        ecu.setsoftwareVersion(value);
                    }
                    else if("supplier_id".equals(name))
                    {
                        ecu.setsupplierId(value);
                    }
                    else if("vin".equals(name))
                    {
                        ecu.setvin(value);
                    }
                }
            }
        }
        while(v_end > 0);
        return ecu;
    }
}


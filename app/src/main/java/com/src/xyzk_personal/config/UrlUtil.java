package com.src.xyzk_personal.config;

import java.io.UnsupportedEncodingException;

public class UrlUtil {
    private final static String ENCODE = "UTF-8";

    //url解码
    public static String getURLDecoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLDecoder.decode(str, ENCODE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }
    //url转码
    public static String getURLEncoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, ENCODE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static String getURLDecodeHozon(String str)
    {
        String result = "";
        result  = getURLDecoderString(str);
        result = result.replaceAll("&amp;", "&");
        return result;
    }
}

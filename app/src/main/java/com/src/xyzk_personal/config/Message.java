package com.src.xyzk_personal.config;

public class Message {
	public final static int MSG_Dialog_WaitTime = 101;	//弹出等待提示对话框
	public final static int MSG_Start_Diagnose = 102;	//开始执行
	public final static int MSG_Initial_Module = 103;	//初始化
	public final static int MSG_ShowArray	=	104;	//显示
	public final static int MSG_ShowArrayHide = 105;	//隐藏的列表
	public final static int MSG_Dialog_WaitIimeAutoClose = 106; //弹出自动关闭窗口
	public final static int MSG_Dialog_close = 107;	//关闭窗口
	public final static int MSG_Func_ShowTip = 108;	//显示提示信息
	public final static int MSG_Dialog_Show_Text = 109;	//在弹出窗口显示提示信息
	public final static int MSG_Dialog_SHow_YESNO = 110;	//弹出提示对话框
	public final static int MSG_ShowArray_module = 111;  //显示模块列表
	public final static int MSG_Main_GetVersion = 112;  //主界面弹框等待
	public final static int MSG_IndivateList = 113; //刷写界面
	public final static int MSG_Updata_Version = 300;  //检查软件更新使用
	public final static int MSG_Select_Partnumber = 114; //选择零件号
}

package com.src.xyzk_personal.config;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import android.util.Log;

public class ServerSocket {
	private final static String TAG = "ServerSocket";
	private static Socket socket = null;
	private static DataOutputStream dout=null;
	private static DataInputStream  din =null;
	boolean exit = false;
	public ServerSocket() {
		// TODO Auto-generated constructor stub
		exit = false;
	}
	void exit()
	{
		exit = true;
	}
	public boolean ConnectServer(String ip,int port)
	{
		//建立socket连接
		try {
			socket = new Socket();
			SocketAddress addr = new InetSocketAddress(ip, port);
			socket.connect(addr, 3000);
			dout =new DataOutputStream(socket.getOutputStream());
			din  =new DataInputStream (socket.getInputStream());
			Sleep(50);    //实际中刚连上后发数据需要一定延时  确保双方链接初始化完成
			if(Common.Debug) Log.i(TAG,"Socket连接成功!");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//毫秒
	private void Sleep(int times)
	{
		try {
			Thread.sleep(times, 0);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int SearchVinConfigFromConfigDb(String Pvin,byte [] Readbuf,int Psize)
	{
		int len = 0;
		if(dout == null || din == null) return 0; //未连接
		try {
			if(din.available() > 0)  //先清除数据
			{
				byte[] Precv = new byte[din.available()];
				din.read(Precv, 0, din.available());
			}
			String Pdata = Commonfunc.IntToHeadStr(Pvin.length() + 2) + "2:" + Pvin;
			dout.write(Pdata.getBytes());
			dout.flush();
			if(Common.Debug) Log.i(TAG,"Send:" + Pdata);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		len = Readdata(Common.ServerMaxTime,Readbuf,Psize);
		//处理数据
		if(len > 3)
			return len;
		else
			return 0;
	}
	public int SearchVersionInfoFromConfigDb(String Pvin,byte [] Readbuf,int Psize)
	{
		int len = 0;
		if(dout == null || din == null) return 0; //未连接
		try {
			if(din.available() > 0)  //先清除数据
			{
				byte[] Precv = new byte[din.available()];
				din.read(Precv, 0, din.available());
			}
			String Pdata = Commonfunc.IntToHeadStr(Pvin.length() + 2) + "8:" + Pvin;
			dout.write(Pdata.getBytes());
			dout.flush();
			if(Common.Debug) Log.i(TAG,"Send:" + Pdata);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		len = Readdata(Common.ServerMaxTime,Readbuf,Psize);
		//处理数据
		if(len > 3)
			return len;
		else
			return 0;
	}
	public boolean SendDataToServer(String Pdata)
	{
		int len = 0;
		if(dout == null || din == null) return false; //未连接
		try {
			if(din.available() > 0)  //先清除数据
			{
				byte[] Precv = new byte[din.available()];
				din.read(Precv, 0, din.available());
			}
			//dout.write(Pdata.length());
			dout.write(Pdata.getBytes());
			dout.flush();
			if(Common.Debug) Log.i(TAG,"Send:" + Pdata);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte [] buf = new byte[128];
		len = Readdata(Common.ServerMaxTime,buf,buf.length);
		if(len > 0)
			return true;
		else
			return false;
	}
	public int SendDataToServer(String Pdata,byte [] Pbuf)
	{
		int len = 0;
		if(dout == null || din == null) return len; //未连接
		try {
			if(din.available() > 0)  //先清除数据
			{
				byte[] Precv = new byte[din.available()];
				din.read(Precv, 0, din.available());
			}
			//dout.write(Pdata.length());
			dout.write(Pdata.getBytes());
			dout.flush();
			if(Common.Debug) Log.i(TAG,"Send:" + Pdata);
		} catch (IOException e) {
			e.printStackTrace();
		}
		len = Readdata(Common.ServerMaxTime,Pbuf,Pbuf.length);
		return len;
	}
	private int Readdata(int maxtime)
	{
		int len = 0;
		if(dout == null || din == null)
		{
			if(Common.Debug) Log.e(TAG,"Readdata exit 1");
			return -1; //未连接
		}
		try {
			long starttime = System.currentTimeMillis(); //获得起始时间
			long endtime = 0;
			while(true)
			{
				Sleep(1);
				if(dout == null || din == null  || exit == true)
				{
					if(Common.Debug) Log.e(TAG,"Readdata exit 2");
					break; //未连接
				}
				len = din.available();
				if(len > 0)
				{
					byte[] Precv = new byte[len];
					din.read(Precv,0,len);
					if(Common.Debug) Log.i(TAG,"Read:" + new String(Precv));
					break;
				}
				endtime = System.currentTimeMillis();
				if((endtime - starttime) > maxtime) //超时
				{
					if(Common.Debug) Log.i(TAG,"超时退出!");
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return len;
	}
	private int Readdata(int maxtime,byte [] Pbuf,int Psize)
	{
		int len = 0;
		if(dout == null || din == null)
		{
			if(Common.Debug) Log.e(TAG,"Readdata exit 1");
			return -1; //未连接
		}
		try {
			long starttime = System.currentTimeMillis(); //获得起始时间
			long endtime = 0;
			while(true)
			{
				Sleep(1);
				if(dout == null || din == null  || exit == true)
				{
					if(Common.Debug) Log.e(TAG,"Readdata exit 2");
					break; //未连接
				}
				len = din.available();
				if(len > 0)
				{
					byte[] Precv = new byte[len];
					din.read(Precv,0,len);
					if(len > Psize)
					{
						System.arraycopy(Precv, 0, Pbuf, 0, Psize);
					}
					else
						System.arraycopy(Precv, 0, Pbuf, 0, len);
					if(Common.Debug) Log.i(TAG,"Read:" + new String(Precv));
					break;
				}
				endtime = System.currentTimeMillis();
				if((endtime - starttime) > maxtime) //超时
				{
					if(Common.Debug) Log.i(TAG,"超时退出!");
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return len;
	}
	//释放
	public void close()
	{
		exit = true;
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			if(dout != null)
			{
				dout.close();
				dout = null;
			}
			if(din != null)
			{
				din.close();
				din = null;
			}
			if(socket != null)
			{
				socket.close();
				socket = null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

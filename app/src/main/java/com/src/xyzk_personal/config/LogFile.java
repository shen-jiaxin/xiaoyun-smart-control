package com.src.xyzk_personal.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.util.Log;

public class LogFile {
	private static final String TAG = "LogFile";
	public static String m_filename = null;
	private String m_canid_send = null;
	private String m_canid_recv = null;
	public LogFile(String filename)
	{
		// TODO Auto-generated constructor stub
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyyMMdd");
		Date time = new java.util.Date();
		m_filename = Common.LogDir + tempDate.format(time).toString() + "/"; // + filename + ".txt";
		if(Common.Debug) Log.i(TAG,"Create Dir:" + m_filename);
		//创建文件夹

		File file = new File(m_filename);
		if(!file.exists())
		{
			file.mkdirs();
		}
		SimpleDateFormat tempDatetime = new SimpleDateFormat("hhmmss");
		m_filename += filename + "-" + tempDatetime.format(time).toString() + ".txt";
		if(Common.Debug) Log.i(TAG,"Create File:" + m_filename);
		//创建文件
		File dir = new File(m_filename);
		if(!dir.exists())
		{
			try {
				dir.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public String getFilePath(){
		return m_filename;
	}
	public LogFile(String filename,boolean Popen)
	{
		if(Popen == false) return;
		m_filename = Common.LogDir + filename;
	}
	public long getFileSize()
	{
		long v_len = 0;
		File file = new File(m_filename);
		if(file == null) return v_len;
		//Reader reader = null;
		if(file.exists() && file.isFile())
			v_len = file.length();
		return v_len;
	}
	public long readFile(char [] Pbuf)
	{
		long v_len = 0;
		File file = new File(m_filename);
		if(file == null) return v_len;
		if(file.exists() && file.isFile())
			v_len = file.length();
		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(file));
			v_len = reader.read(Pbuf);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e1) {
			}
		}
		return v_len;
	}
	//写入文件 canid = 1 send   canid = 2 recv   canid = 0 null
	public void PrintAppend(int canid,String data)
	{
		FileWriter fw = null;
		String datetime = "";
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd" + " "
				+ "HH:mm:ss");
		Date time = new java.util.Date();
		datetime = tempDate.format(time).toString() + "." + Commonfunc.msToString((int) (time.getTime() % 1000));
		try {
			fw = new FileWriter(m_filename, true);
			if(canid == 1)
			{
				fw.write(datetime + "-->" + m_canid_send + data + "\n");
			}
			else if(canid == 2)
			{
				fw.write(datetime + "-->" + m_canid_recv + data + "\n");
			}
			else if(canid == 0)
			{
				fw.write(datetime + "-->"+ data + "\n");
			}
			else
			{
				fw.write(datetime + "-->" + Commonfunc.canidToHexString(canid) + ":" + data + "\n");
			}
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//
	}
	public void PrintAppend(String Pmode,int canid,String data)
	{
		FileWriter fw = null;
		String datetime = "";
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd" + " " + "HH:mm:ss");
		Date time = new java.util.Date();
		datetime = tempDate.format(time).toString() + "." + Commonfunc.msToString((int) (time.getTime() % 1000));
		try {
			fw = new FileWriter(m_filename, true);
			fw.write(datetime + "-->" + Commonfunc.canidToHexString(canid) + "(" + Pmode + ")" +":" + data + "\n");
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//
	}
	public void PrintLog(String Pstr)
	{
		FileWriter fw = null;
		String datetime = "";
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd" + " " + "HH:mm:ss");
		Date time = new java.util.Date();
		datetime = tempDate.format(time).toString() + "." + Commonfunc.msToString((int) (time.getTime() % 1000));
		try {
			fw = new FileWriter(m_filename, true);
			fw.write(datetime + "-->" + Pstr + "\n");
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//
	}
	//设置can ID
	public void SetCanId(byte[] buf)
	{
		m_canid_send = Commonfunc.canidToHexString((buf[0] & 0xFF) * 0x100 + (buf[1] & 0xFF)) + ":";
		m_canid_recv = Commonfunc.canidToHexString((buf[2] & 0xFF) * 0x100 + (buf[3] & 0xFF)) + ":";
	}

}

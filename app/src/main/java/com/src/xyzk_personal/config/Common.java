package com.src.xyzk_personal.config;

import com.src.xyzk_personal.EOL.Cartype;

import android.content.Context;
import android.os.Environment;

import java.util.List;

public class Common {
	public static boolean Debug = false;
	public static boolean wait = false; //等待
	public static Context context = null;
	public static boolean dialog_result = true;
	public static boolean showEolactivity = false;	//activity运行状态
	public static int setmaxtime = 1000;
	public static int cmdsetmaxtime = 2000; //设置命令超时时间
	public static int cmdmaxtime = 2500;
	public static int ServerMaxTime = 5000;	//服务器最大延时
	public static int cmd_freq = 3;
	public static int cmd_freg_Init = 8;	//初始化特殊处理
	public static int waittime = 100; //通讯时间间隔
//	public static String Dir = Environment.getExternalStorageDirectory() + "/EOL_DOWNLINE/";  //存储目录
	public static String Dir = Environment.getExternalStorageDirectory() + "/";  //存储目录
	public static String GuestName = "HOZON";	//客户名称，也是目录名称
	public static String ProjectName = "Eol_HOZON";  //项目名称
	public static String LogDir = Dir + "LOG/";	//LOG记录目录
	public static final String db_name = Dir + "result.db";
	public static final int Rnull = 0; //结果无
	public static final int Rok = 1;	//ok
	public static final int Rnok = 2;	//nok
	public static String Server_IP = "192.168.10.1";
	public static String Server_Home = "192.168.10.1";
	public static int Server_PORT = 4000;
	public static String ServerName = "PCHOST";  //服务器名称
//	public static String BoxName = "330100001000";		//Box名称
	public static String BoxName = "880100100797";		//Box名称
	public static String BoxIP = "192.168.1.1";
	public static int BoxPORT = 5000;
	public static String Tbox_Server = "";  		//TBOX相关变量
	public static int Tbox_Port = 8110;
	public static String Tbox_start ="";
	public static Cartype cartype = new Cartype();	//初始化
	public static int Printnum = 1;	//打印机序号
	public static int Printnumber = 0;	//	打印机数量
	public static int getxmlmode = 0; //默认从mes获取
	public static String mes_url = "";
	public static int mes_enable = 0;
	public static String mes_eol = "";
	public static int mes_eol_enable = 0;
	public static String Lang = "zh";
	public static String Web_Url_Networ_Disk_File = ""; //刷写文件下载Url
	public static String Web_Result_Add = "";//数据上传Url
	public static String Web_Result_Uploadfile = ""; //日志文件上传UrL
	public static boolean updata_isok = true;//上传数据成功或者失败
	public static boolean dialog_isok = true; //是否要弹出窗口，避免弹出多个窗口
	public static String FileName = null; //版本更新下载的文件名称
	public static Boolean downStatus = true ; //下载状态
	public static String resource = null; //软件版本更新路径，下载版本时使用
	public static String web_API_AppUpdate = ""; //下载版本软件ip地址
	public static String web_API_SelectUpdate = ""; //查询版本更新ip地址
//	public static String saveApkPath = Environment.getExternalStorageDirectory() + "/DownLoad/android/apk/";   //apk存储目录
	public static String saveApkPath = Environment.getExternalStorageDirectory()+"/HOZON/";   //apk存储目录
	public static String appVersion = null ; //服务器上的软件版本
	public static String appNote = null; //软件版本更新内容
	public static List<String> SelectPartNumberList=null;
	public static String web_API_AppStatus = "";//上送终端状态地址
	public static String PartNumber = "";
}

package com.src.xyzk_personal.config;

import java.util.ArrayList;

public class EcuModule
{
	String name;
	int Position;
	int result; //0--失败，1--成功
	ArrayList<String> context;

	public EcuModule()
	{
		name = "";
		Position  = -1;
		result = -1;
		context = new ArrayList<String>();
	}
	public EcuModule(String Pname)
	{
		name = Pname;
		Position  = -1;
		result = -1;
		context = new ArrayList<String>();
	}
	public String getname() {
		return name;
	}
	public void setname(String P) {
		this.name = P;
	}
	public Integer getPosition() {
		return Position;
	}
	public void setPosition(int P) {
		this.Position = P;
	}
	public Integer getresult() {
		return result;
	}
	public void setresult(int P) {
		this.result = P;
	}
	public void addcontext(String Pcontext)
	{
		context.add(Pcontext);
	}
	public ArrayList<String> getcontext()
	{
		return context;
	}
	public String toString()
	{
		String str = "";
		if(result == 1) //ok
		{
			str = "★" + name + ":  OK;";
		}
		else
		{
			str = "★" + name + ":  NOK;";
			if(context != null)
			{
				for(int k = 0; k < context.size(); k ++)
					str += context.get(k).toString() + ";";
			}
		}
		return str;
	}
}
package com.src.xyzk_personal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import com.src.tsdl_personal.R;
import com.src.xyzk.bluetooth.BluetoothDeviceListActivity;
import com.src.xyzk.bluetooth.service.BluetoothAdapterService;
import com.src.xyzk_personal.EOL.EolFunction;
import com.src.xyzk_personal.Service.UpdateEolService;
import com.src.xyzk_personal.UpApp.UpdateApp;
import com.src.xyzk_personal.UpApp.UpdateAppService;
import com.src.xyzk_personal.config.Common;
import com.src.xyzk_personal.config.Commonfunc;
import com.src.xyzk_personal.config.DeviceLicence;
import com.src.xyzk_personal.config.DomXmlService;
import com.src.xyzk_personal.config.HttpRequest;
import com.src.xyzk_personal.config.Message;
import com.src.xyzk_personal.config.WifiAdmin;
import com.src.xyzk_personal.ga1027.Eolfunctionmain;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static String TAG = "MainActivityActivity";
    Context context = MainActivity.this;
    //锁定屏幕
    private PowerManager powerManager = null;
    private WakeLock wakeLock = null;
    private EditText m_vin = null;    //VIN码输入窗口
    private TextView m_showcartype = null;    //显示cartype
    private TextView m_title = null;
    //car list
    private ListView m_listcar = null;
    private ArrayList<String> m_listcar_data = null;
    private ListColorMainAdapter m_listcar_adapter = null;
    //func list
    private ListView m_listfunc = null;
    private ArrayList<String> m_listfunc_data = null;
    private ListColorAdapter m_listfunc_adapter = null;
    //按钮
    private Button m_but_clear = null;
    private Button m_but_ok = null;
    private Button m_but_menu = null;
    //记录选择
    private int m_select_car = -1;    //车型选择
    private int m_select_func = -1;    //功能选择
    //对话框
    private ProgressDialog progressdialog;
    //缓存
    SharedPreferences.Editor m_editor = null;
    SharedPreferences m_pref = null;
    //	private DbAdapter m_db = null;
    //初始化连接service//蓝牙服务
    BluetoothAdapterService m_blueservice = BluetoothAdapterService.getInstance();
    //消息
    private static final int SHOW_ACTIVITY_RESULT = 101;

    private static final int PERMISSION_REQUEST_CODE = 122;
    private static final int PERMISSION_REQUEST_CODE2 = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main_activity);
        //锁定屏幕初始化
        powerManager = (PowerManager) this.getSystemService(Service.POWER_SERVICE);
        wakeLock = this.powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Lock");
        //是否需计算锁的数量
        wakeLock.setReferenceCounted(false);
        //title
        m_title = (TextView) findViewById(R.id.main_show_title);
        m_title.setText(getString(R.string.app_name) + " - " + getVersionName());
        //VIN码输入窗口初始化
        m_vin = (EditText) findViewById(R.id.main_vin_edit);
        //界面显示cartype
        m_showcartype = (TextView) findViewById(R.id.main_cartype);
        //m_vin.setText("LVV");
        BluetoothAdapterService t_com = BluetoothAdapterService.getInstance();
        //初始化CarList
        m_listcar = (ListView) findViewById(R.id.main_show_car);
        m_listcar_data = new ArrayList<String>();
        m_listcar_adapter = new ListColorMainAdapter(m_listcar_data);
        m_listcar.setAdapter(m_listcar_adapter);
        //初始化FuncList
        m_listfunc = (ListView) findViewById(R.id.main_show_func);
        m_listfunc_data = new ArrayList<String>();
        m_listfunc_adapter = new ListColorAdapter(m_listfunc_data);
        m_listfunc.setAdapter(m_listfunc_adapter);
        //list点击事件
        m_listcar.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                AutoExeEOL(arg2);
            }
        });
        //函数选择事件
        m_listfunc.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (Common.cartype.OptMode == 0) //自动时，不能点击该列表
                {
                    Toast.makeText(context, R.string.main_tip_text5, Toast.LENGTH_SHORT).show();
                    return;
                }
                m_select_func = arg2;
                m_listfunc_adapter.setSelected(arg2);
                final MsgWaitDialog waitdialog = new MsgWaitDialog(MainActivity.this);
                waitdialog.setText(getString(R.string.main_tip_text9) + m_listfunc_data.get(arg2) + "?");
                waitdialog.setWaitTime(0);
                waitdialog.ShowCancel();
                waitdialog.show();
                waitdialog.setOnOKButton(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        waitdialog.dismiss();
                        //执行功能
                        //Toast.makeText(context, R.string.main_tip_text6, Toast.LENGTH_SHORT).show();
                        m_listfunc_adapter.notifyDataSetChanged();
                        //执行
                        Startdiagnose(m_select_car, "STR");
                    }
                });
            }
        });
        //清除按钮点击事件
        m_but_clear = (Button) findViewById(R.id.main_but_clear);
        m_but_clear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ClearData();
                m_blueservice.StopService();
            }
        });
        //OK按钮
        m_but_ok = (Button) findViewById(R.id.main_but_ok);
        m_but_ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //开始执行新流程
                //先清除显示
                ClearData();
                //第一步，扫描VIN
                StartInputVin();
            }
        });
        m_but_menu = (Button) findViewById(R.id.main_opt_menu);
        //为按钮绑定上下文菜单（注意不是绑定监听器）
        registerForContextMenu(m_but_menu);
        //检查设备非法状态
        DeviceLicence licence = new DeviceLicence();
        if (licence.CheckDevice(context) == false) {
//			finish();
        }

        String locale = Locale.getDefault().getLanguage();
        if (locale.equals("en"))
            Common.Lang = "en";
        else if (locale.equals("zh"))
            Common.Lang = "zh";
        m_editor = this.getSharedPreferences("config", MODE_PRIVATE).edit(); //不存在则创建
        m_pref = getSharedPreferences("config", MODE_PRIVATE);
        //获取读sd的动态权限
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                    PERMISSION_REQUEST_CODE);
//        } else {
            LoadConfig();
//        }

//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE2);
//        } else {
//            Log.i("duan==", "获取成功" + getSN());
//            Common.BoxName = getSN();
//        }

        //test();

        //启动软件更新查询服务
        Intent it = new Intent(this, UpdateAppService.class);
        startService(it);
        //启动自动上传服务
        Common.context = MainActivity.this;
        Intent EOLit = new Intent(this, UpdateEolService.class);
        startService(EOLit);

    }

    /**
     * 获取SN
     *
     * @return
     */
    public static String getSN() {
        String serial = "";
        //通过android.os获取sn号
        try {
            serial = android.os.Build.SERIAL;
            if (!serial.equals("") && !serial.equals("unknown")) return serial;
        } catch (Exception e) {
        }
        //通过反射获取sn号
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialno");
            if (!serial.equals("") && !serial.equals("unknown")) return serial;

            //9.0及以上无法获取到sn，此方法为补充，能够获取到多数高版本手机 sn
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) serial = Build.getSerial();
        } catch (Exception e) {
            serial = "";
        }
        return serial;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

    //系统菜单处理方法
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (item.getItemId()) {
//			case R.id.main_system_setting:  //系统设置
//				Intent newset = new Intent(this,SystemSettingActivity.class);
//				startActivity(newset);
//				break;
            case R.id.main_setting_print:    //打印机设置
                Intent newintent = new Intent(this, UpdateDataActivity.class);
                startActivity(newintent);
                break;
            case R.id.main_setting_update:    //update  todo
                progressdialog = ProgressDialog.createDialog(context, getString(R.string.main_tip_text10));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getAppVersion();
                    }
                }).start();
                break;
            case R.id.main_setting_about:    //about
                LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.main_alertdiag_title, null);
                new AlertDialog.Builder(MainActivity.this)
                        .setCustomTitle(view)
                        .setMessage(R.string.my_about)
                        .setPositiveButton(R.string.but_ok, null)
                        .show();
                break;
            case R.id.main_settings_exit:    //exit
                m_blueservice.StopService();
                System.exit(0);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.main_activity, menu);
        //registerForContextMenu(v);
    }

    //系统菜单处理方法
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (item.getItemId()) {
//			case R.id.main_system_setting:  //系统设置
//				Intent newset = new Intent(this,SystemSettingActivity.class);
//				startActivity(newset);
//				break;
            case R.id.main_setting_print:    //打印机设置
                Intent newintent = new Intent(this, UpdateDataActivity.class);
                startActivity(newintent);
                break;
            case R.id.main_setting_update:    //update  todo
                progressdialog = ProgressDialog.createDialog(context, getString(R.string.main_tip_text10));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getAppVersion();
                    }
                }).start();
                break;
            case R.id.main_setting_about:    //about
                LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.main_alertdiag_title, null);
                new AlertDialog.Builder(MainActivity.this)
                        .setCustomTitle(view)
                        .setMessage(R.string.my_about)
                        .setPositiveButton(R.string.but_ok, null)
                        .show();
                break;
            case R.id.main_settings_exit:    //exit
                m_blueservice.StopService();
                System.exit(0);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //请求常亮
        wakeLock.acquire();

        //todo 判断是否有新版本更新
        Boolean dialog_isok = Common.dialog_isok;
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);//拿到 sharedPreferences 的引用 MODE_PRIVATE表示只能被本应用读写，不能被其他应用读写
//		System.out.println("-------"+softTime);
        Boolean version_isok = false;
        String appVersion = sharedPreferences.getString("Version", "1");
        String Version = getVersionName();
        float newVersion = Float.parseFloat(appVersion.replace("V", "").replace(".", ""));
        float oldVersion = 0;
        if (Version != null && Version.length() > 3)
            oldVersion = Float.parseFloat(Version.replace("V", "").replace(".", ""));
        if (newVersion > oldVersion) {
            version_isok = true;
        } else {
            version_isok = false;
        }
        if (version_isok && dialog_isok) {
            final UpdateApp dialog = new UpdateApp(MainActivity.this);
            dialog.show();
            Common.dialog_isok = false;
        }

        //测试网络连接
    }

    //自动执行初始化功能
    void AutoExeEOL(int Pstation) {
        m_select_car = Pstation;
        m_listcar_adapter.setSelected(Pstation);
        m_listcar_adapter.notifyDataSetChanged();
        //写缓存
        m_editor.putInt("station", Pstation);
        m_editor.commit();
        LoadFunction(Pstation);
        //if(Common.cartype.getMap("AUTO") > 0) //自动时，直接执行
        if (Common.cartype.OptMode == 0) //自动时，直接执行
        {
            //test();
            //最后提示
            final MsgWaitDialog waitdialog = new MsgWaitDialog(MainActivity.this);
            waitdialog.setText(getString(R.string.main_tip_text9) + m_listcar_data.get(Pstation) + "?");
            waitdialog.setWaitTime(0);
            waitdialog.ShowCancel();
            waitdialog.show();
            waitdialog.setOnOKButton(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    waitdialog.dismiss();
                    //执行功能
                    //Toast.makeText(context, R.string.main_tip_text6, Toast.LENGTH_SHORT).show();
                    Startdiagnose(m_select_car, "STR");
                }
            });
        }
    }

    private Handler mhander = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case Message.MSG_Main_GetVersion:
                    progressdialog.dismiss();
                    if (msg.arg1 == 1) //ok
                    {
                        Loadcartype(Common.cartype.car);
                    } else {
                        if (Common.mes_enable == 0)
                            Toast.makeText(context, getString(R.string.main_tip_text11), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(context, getString(R.string.main_tip_text12), Toast.LENGTH_LONG).show();
                    }
                    break;
                case Message.MSG_Updata_Version:
                    progressdialog.dismiss();
                    //检测到有软件更新
                    if (msg.arg1 == 1) {
                        final UpdateApp dialog = new UpdateApp(MainActivity.this);
                        dialog.show();
                        Common.dialog_isok = false;
                    } else if (msg.arg1 == 2) {
                        Toast.makeText(context, getString(R.string.main_tip_text13), Toast.LENGTH_SHORT).show();
                    } else if (msg.arg1 == 3) {
                        Toast.makeText(context, getString(R.string.main_tip_text14), Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    break;
            }
        }

        ;
    };

    void test() {
        //String geturl = "https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169241933&rev=54221083dd914a72a90482ea1f5122ac&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112098-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E9%A9%B1%E5%8A%A8%E8%BD%AF%E4%BB%B6.s19";
        //String outurl = UrlUtil.getURLDecodeHozon(geturl);
        //outurl += "";
        //String key = EolFunction.GetPinCode("LUZAGBGA2PA096360");
        //String esk = EolFunction.genHozonESK("LUZAGBGA2PA096360");
        //key += " ";
        //String param = "path="+ "S11-2107099AD_1YB_CDU_ApplicationSoftware_05.01.03_H1.11_20230926.s19";
        //String v_app_file = "/storage/emulated/0/EOL_DOWNLINE/HOZON/EP12/ECU/OBCe/" + "S11-2107099AD_1YB_CDU_ApplicationSoftware_05.01.03_H1.11_20230926.s19";
        //String iRet = HttpRequest.sendGetBufferedReader(Common.Web_Url_Networ_Disk_File,param,v_app_file);

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        //取消屏幕常亮
        wakeLock.release();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        m_blueservice.StopService();
        super.onDestroy();
        //m_db.close();

    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        m_blueservice.StopService();
        super.finish();
    }

    //清除数据
    private void ClearData() {
        //先清除显示
        m_vin.setText("");
        m_showcartype.setText("");
        m_listcar_adapter.setSelected(-1);
        m_listcar_data.clear();
        m_listcar_adapter.notifyDataSetChanged();
        m_listfunc_data.clear();
        m_listfunc_adapter.notifyDataSetChanged();
    }

    //获取WIFI连接状况
    boolean GetWifiConnect() {
        WifiAdmin m_wifi = new WifiAdmin(context);
        if (m_wifi.getState() < 2) //已经关闭，则打开
        {
            Toast.makeText(context, R.string.main_tip_text7, Toast.LENGTH_SHORT).show();
            return false;
        }
        String ConnectName = m_wifi.getWifiConnectSSID();
        if (ConnectName == null) {
            Toast.makeText(context, R.string.main_tip_text8, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (ConnectName.equals(Common.BoxName))
            return true;
        else {
            Toast.makeText(context, R.string.main_tip_text8, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    //获取系统设置
    private void LoadConfig() {
        //结果初始化
        Ini inifile;
        try {
            String configfile = Common.Dir + Common.GuestName + "/config.ini";
            if (Common.Lang.equals("en"))
                configfile = Common.Dir + Common.GuestName + "/config_en.ini";
            inifile = new Ini(new File(configfile));
            Profile.Section sec = null;


            SharedPreferences settings = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
            //3.生成一个保存编辑变量
            int printnum = settings.getInt("Print", 1);
            Common.Printnum = printnum;


        } catch (InvalidFileFormatException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //获取mes地址
        Ini mesfile;
        try {
            mesfile = new Ini(new File(Common.Dir + Common.GuestName + "/mes.ini"));
            Profile.Section sec = null;
            //获取系统配置信息
            sec = mesfile.get("SETTING");
            if (sec != null) {
                Common.Server_IP = sec.get("IP");
                Common.Printnumber = Integer.valueOf(sec.get("PRINTNUMBER"));
                if (Common.Debug) Log.i(TAG, "PRINTNUM=" + Common.Printnum);
                String data = sec.get("LocalXML");

                //获取电检WEB服务器URL
                Common.Web_Url_Networ_Disk_File = Common.Server_IP + sec.get("WEB_networkDiskFile");
                Common.Web_Result_Add = Common.Server_IP + sec.get("WEB_Result_Add");
                Common.Web_Result_Uploadfile = Common.Server_IP + sec.get("WEB_Result_UploadFiles");
                Common.web_API_AppUpdate = Common.Server_IP + sec.get("web_API_AppUpdate");
                Common.web_API_SelectUpdate = Common.Server_IP + sec.get("web_API_SelectUpdate");
                Common.web_API_AppStatus = Common.Server_IP + sec.get("web_API_AppStatus");
                if (data != null)
                    Common.getxmlmode = Integer.valueOf(data);
            }
            //获取系统配置信息
            sec = mesfile.get("MES");
            if (sec != null) {
                Common.mes_url = sec.get("URL");
                String data = sec.get("ENABLE");
                if (data != null)
                    Common.mes_enable = Integer.valueOf(data);
                //mes eol
                Common.mes_eol = Common.Server_IP + sec.get("EOL");
                data = sec.get("USE_EOL");
                if (data != null)
                    Common.mes_eol_enable = Integer.valueOf(data);
            }


        } catch (InvalidFileFormatException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    //开始执行流程,输入VIN
    private void StartInputVin() {
        final InputVinDialog dialog = new InputVinDialog(this);
        //dialog.setEditString("MRTAGBGAXPA000025");
        dialog.setOnOKButton(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final String vin = dialog.GetEditString();
                if (Commonfunc.CheckVinFormat(vin.getBytes()) == false) {
                    Toast.makeText(context, getString(R.string.msg_show_context14), Toast.LENGTH_SHORT).show();
                    dialog.setEditString("");
                    return;
                } else //输入配置代码
                {
                    m_blueservice.StopService();
                    m_blueservice.m_blue_state.SetListen();
                    Common.cartype.clearall();
                    Common.cartype.setVin(vin);
                    dialog.dismiss();
                    m_vin.setText(vin);
                    m_blueservice.SetStartRecord(new String(Common.cartype.vin));
                    m_blueservice.PrintLog("Scan:" + vin);
                    //记录版本
                    m_blueservice.PrintLog(getString(R.string.app_name) + "_" + getString(R.string.Version));
                    //根据配置情况获取XML文件
                    //弹出对话框，开启线程
                    progressdialog = ProgressDialog.createDialog(context, getString(R.string.main_tip_text15));
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            boolean isok = false;
                            if (Common.mes_enable == 0) //本地获取
                            {
                                try {
                                    isok = GetVinInfoFromLocal(vin);
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    String err = e.toString();
                                    e.printStackTrace();
                                }

                            } else {
                                try {
                                    if (Common.mes_eol_enable > 0)
                                        isok = getMesVinInfofromEol(vin);
                                    else {
                                        //isok = getMesVinInfo(vin);
                                        isok = getMesVinInfoThai(vin);
                                    }
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                    m_blueservice.PrintLog(e.toString());
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                    m_blueservice.PrintLog(e.toString());
                                }
                            }
                            //test();
                            int result = 0;
                            if (isok)
                                result = 1;
                            mhander.obtainMessage(Message.MSG_Main_GetVersion, result, 0).sendToTarget();
                        }
                    }).start();

                }
            }
        });
        dialog.show();
    }

    //根据cartype查找相应的车型配置
    private void Loadcartype(String car) {
        //先遍历模块

        //load工位选择
        Ini inifile;
        try {
            String configfile = Common.Dir + Common.GuestName + "/config.ini";
            if (Common.Lang.equals("en"))
                configfile = Common.Dir + Common.GuestName + "/config_en.ini";
            inifile = new Ini(new File(configfile));
            Profile.Section sec = inifile.get("STATION");
            //数量
            int num = Integer.valueOf(sec.get("NUM"));
            String id = null;
            for (int i = 0; i < num; i++) {
                id = "T" + i;
                m_listcar_data.add(sec.get(id));
            }
        } catch (InvalidFileFormatException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Common.cartype.station = m_pref.getInt("station", -1);
        m_listcar.setSelection(Common.cartype.station);
        m_listcar_adapter.notifyDataSetChanged();
        //show cartype
        m_showcartype.setText(Common.cartype.car + "-" + Common.cartype.name);
        if (Common.cartype.station >= 0) {
            AutoExeEOL(Common.cartype.station);
        }
    }

    //获取需要执行的函数列表
    private void LoadFunction(int Pstation) {
        m_select_func = -1;    //初始化
        m_listfunc_data.clear();
        m_listfunc_adapter.setSelected(-1);
        //set
        Common.cartype.SetCartype(Pstation);
        //获取初始化列表
        if (Common.cartype.OptMode == 1) {
            ArrayList<String> list = Eolfunctionmain.GetStepInformationRepair();
            if (list == null) return;
            for (int i = 0; i < list.size(); i++) {
                m_listfunc_data.add(list.get(i));
            }
            m_listfunc.setClickable(true);
        } else {
            ArrayList<String> list = Eolfunctionmain.GetStepInformation(Common.cartype.istep);
            if (list == null) return;
            for (int i = 0; i < list.size(); i++) {
                m_listfunc_data.add(list.get(i));
            }
            if (Common.cartype.getMap("AUTO") > 0) //自动执行,则不能点击
            {
                m_listfunc.setClickable(false);
            }
        }
        m_listfunc_adapter.notifyDataSetChanged();
    }

    //开始诊断 selectfunc -- 执行项
    private void Startdiagnose(int selectfunc, String funcname) {
        //先检查WIFI连接情况
        //if(GetWifiConnect() == false) return;
        if (ConnectBluetooth() == false) return;
        Bundle bund = new Bundle();
        if (m_select_func >= 0) //返修功能
        {
            bund.putString("TITLE", m_listcar_data.get(m_select_car) + "->" + m_listfunc_data.get(m_select_func));
            String v_funcname = Common.cartype.SetRepairIstep(m_listfunc_data.get(m_select_func));
            bund.putString("FUNC_NAME", v_funcname);
        } else {
            bund.putString("TITLE", m_listcar_data.get(m_select_car));
            bund.putString("FUNC_NAME", "");
            Common.cartype.station = m_select_car;
        }
        bund.putInt("CAR", m_select_car);
        bund.putInt("FUNC", m_select_func);
        Intent newintent = new Intent(context, EolNewFunctionActivity.class);
        newintent.putExtras(bund);
        startActivity(newintent);
    }

    //屏蔽返回键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:    //返回键
                return true;

        }
        return super.onKeyDown(keyCode, event);
    }

    //蓝牙连接
    private boolean ConnectBluetooth() {
        if (m_blueservice.m_blue_state.IsConnected() == true) return true;
        Bundle bund = new Bundle();
        bund.putString("BOX", Common.BoxName);
        Intent intent = new Intent(context, BluetoothDeviceListActivity.class);
        intent.putExtras(bund);
        startActivityForResult(intent, SHOW_ACTIVITY_RESULT);
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHOW_ACTIVITY_RESULT:
                if (resultCode == BluetoothDeviceListActivity.RESULT_OK) {
                    //
                    Startdiagnose(m_select_car, "STR");
                }
                break;

            default:
                break;
        }
    }

    public boolean getMesVinInfo(String Pvin) throws IOException {
        String v_carinfo = "";
        String URL = Common.mes_url;
        String namespace = "http://impl.webservice.mes.com";//namespace
        String methodName = "getEolByVin";//要调用的方法名称

        SoapObject request = new SoapObject(namespace, methodName);
        request.addProperty("vin", Pvin);

        //创建SoapSerializationEnvelope 对象，同时指定soap版本号(之前在wsdl中看到的)
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapSerializationEnvelope.VER12);

        envelope.bodyOut = request;//由于是发送请求，所以是设置bodyOut
        envelope.dotNet = true;//

        HttpTransportSE httpTransportSE = new HttpTransportSE(URL);
        //MyHttpTransportSE httpTransportSE = new MyHttpTransportSE(URL);
        try {
            httpTransportSE.call(null, envelope);
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }//调用

        // 获取返回的数据
        SoapObject object = (SoapObject) envelope.bodyIn;
        // 获取返回的结果
        String result = object.getProperty(0).toString();

        // String result = "EOL{acu_isconfig=false; alm_isconfig=false; apa_isconfig=false;
        // bdcm_isconfig=false; bdcm_s_isconfig=false; bms_isconfig=false; body_color=玉石白; b
        // tm_isconfig=false; clm_isconfig=false; dvr_isconfig=false; eacp_isconfig=false;
        // ecuArray=ECU{app_MD5=04cbad927a12758bfbf025baa82b7812;
        // app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=202594235&rev=a9ce89fa36e7408eb737b9dfd7cbac6f&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115099BC_9AF_PTC_APP_S05.02.04_H1.03_20220310.s19; app_file_version=null; app_file_version_latest=05.02.04; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=08a4d3f22f4a3d128f4070bd03b33fdd; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=202594241&rev=ae023cb951d64da0b60519aac62056bc&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115098BC_9AF_PTC_DR_S05.02.04_H1.03_20220310.s19; driver_file_version=null; driver_file_version_latest=05.02.04; ecode=null; ecu_name=PTC; eol_config_code=-; error_code=1; hardware_version=H1.03; iccid_code=null; manufacture_date=null; part_number=S30-8115010BC; serial_number=null; software_ass=S30-8115999BC;
        // software_version=05.02.04; supplier_id=9AF; vin=LUZBGAFB1NA018786; };
        // ecuArray=ECU{app_MD5=65a407da52739463c028017eb0c799ae;
        // app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=207311000&rev=aaf2ddf904254f5fbfa59e77fad5952e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600099_9AT_BDCM_APP_S05.06.10_H1.12_20220425.s19;
        // app_file_version=null; app_file_version_latest=05.06.10; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=71d44d7e82e055c735001fd608fa7365; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=194808122&rev=371c94cf742e40b99f8af82720701765&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600098_9AT_BDCM_DR_S05.06.09_H1.12_20220312.s19; driver_file_version=null; driver_file_version_latest=05.06.09; ecode=null; ecu_name=BDCM; eol_config_code=09A5A4A120000000; error_code=1; hardware_version=H1.12; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3600999; software_version=05.06.10; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b69aa3d1be98dd068d67aa643442ba7f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409932&rev=745041a8846c9073&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604099_BDCM_S_APP.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=5670419043b96fd1d9414b1665fe3157; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409935&rev=39110adc4c874061bd03bff39f5a3b14&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604098_BDCM_S_Driver.s19; driver_file_version=null; driver_file_version_latest=05.00.03; ecode=null; ecu_name=BDCM_S; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3604999; software_version=05.00.03; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=F4A2D451A12F2CC0F2A7329284F7FAAB; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154977610&rev=4e12085fcae04082b8226051b1062475&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_05.13.01_01.02.0E__202106301429_processed.S19; app_file_version=null; app_file_version_latest=05.13.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=09SPE007NA0111C540000276; ecu_name=BMS; eol_config_code=-; error_code=1; hardware_version=H3.10; iccid_code=null; manufacture_date=null; part_number=S30-2101010NA; serial_number=null; software_ass=S30-2101999NA; software_version=05.13.01; supplier_id=1HA; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b88257c99ddb0bd1a60b243c6f048abc; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169207917&rev=51303ee0151f4558af5fb377e7fe3987&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112099-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E5%BA%94%E7%94%A8%E8%BD%AF%E4%BB%B6.s19; app_file_version=null; app_file_version_latest=05.04.04; cal_MD5=28f8697e661aca7b4eba0c0551a2fd4e; cal_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169255031&rev=60e6a5630b204651a30c19b4ef80bfe3&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112097-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E6%A0%87%E5%AE%9A%E8%BD%AF%E4%BB%B6.s19; cal_file_version=null; cal_file_version_latest=05.04.04; driver_MD5=b558277911aa2085c33fedd82fb79edb; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169241933&rev=54221083dd914a72a90482ea1f5122ac&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112098-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E9%A9%B1%E5%8A%A8%E8%BD%AF%E4%BB%B6.s19; driver_file_version=null; driver_file_version_latest=05.04.04; ecode=null; ecu_name=CLM; eol_config_code=890F000000000000; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-8112010; serial_number=null; software_ass=S30-8112999; software_version=05.04.04; supplier_id=9RY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=FBD256AFF37A7639FF5428F0DCE6E331; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203330&rev=4dc86e3c547042f2b2c3967c2be0e4b5&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103099BB.s19; app_file_version=null; app_file_version_latest=05.00.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=99785D729171D63D2D705A4894331FA4; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203333&rev=c84f682a6ec046b4a2ccef04d07fa1c0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103098BB.s19; driver_file_version=null; driver_file_version_latest=05.00.00; ecode=null; ecu_name=EACP; eol_config_code=-; error_code=1; hardware_version=H0.00; iccid_code=null; manufacture_date=null; part_number=S30-8103010BB; serial_number=null; software_ass=S30-8103999BB; software_version=05.00.00; supplier_id=9CY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b567b6ecb74e968980f618ba545a5a63; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=168457441&rev=bb9bf176cc984158bbbdd302660d5787&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EGSM_BD_SW_05.00.08H1.01+0818_DiPei.s19; app_file_version=null; app_file_version_latest=05.00.08; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EGSM; eol_config_code=-; error_code=1; hardware_version=H1.01; iccid_code=null; manufacture_date=null; part_number=S30-3774020BD; serial_number=null; software_ass=S30-3774999BD; software_version=05.00.08; supplier_id=6DP; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=8af30a1dc3c89d75ea183a9edd809003; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=11894836&rev=aedf0330c0f3486db42aecd3e409997e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30-EPS-V06.hex; app_file_version=null; app_file_version_latest=05.00.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EPS; eol_config_code=00; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3404010; serial_number=null; software_ass=S30-3404999; software_version=05.00.01; supplier_id=6YB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ESC; eol_config_code=00000003; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3570010BA; serial_number=null; software_ass=S30-3570999BA; software_version=05.01.01; supplier_id=6WD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=fbf4a8f209dc03c504f686056340110f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=54263846&rev=49e4e690d0eb4c37875f3b2d995f7c44&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3601099_9ML_GW_APP_S05.03.12_H1.10_20210204.s19; app_file_version=null; app_file_version_latest=05.03.12; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=GW; eol_config_code=0800; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3600525; serial_number=null; software_ass=S30-3601999; software_version=05.03.12; supplier_id=9ML; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=7efd08ece0acacc77b628b7898140982; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=170454338&rev=be4fc58e0fd140e0bd51a87d20a0ee4a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FHZEP30_9CJ_ICU_S30-7912020_05.01.00_H2.01_2021.09.18.S19; app_file_version=null; app_file_version_latest=05.01.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ICU; eol_config_code=A008C40000000000; error_code=1; hardware_version=H2.01; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=null; software_ass=S30-7912999BB; software_version=05.01.00; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1c7693635ea640e071085f779a02acb7; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=183133061&rev=14bb23b88a76425e9cb655ffeba18fc7&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7912099BC_9CJ_IHU_APP_S05.00.20_H2.01_20211020.zip; app_file_version=null; app_file_version_latest=05.00.20; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=IHU; eol_config_code=0886010080224000; error_code=1; hardware_version=H2.02; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=HEP302020D524A0008; software_ass=S30-7912999BC; software_version=05.00.20; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=78dcdadc72e33f2d38852dfd77fb6fa1; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=197035253&rev=2b726a19b81c45739efa37f4c54ba32b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7922099_9TB_LSA_APP_S05.03.01_H1.02_20220320.s19; app_file_version=null; app_file_version_latest=05.03.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=LSA; eol_config_code=00; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-7922010; serial_number=null; software_ass=S30-7922999; software_version=05.03.01; supplier_id=9TB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=5617210ea3be7d2044c1c57623165668; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=118205864&rev=83fe0769b32c46f58f278bbb97c40325&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2103099FA_1FS_MCU_APP_S05.00.03_H1.11_20210310.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=BAPF8E11442; ecu_name=MCU; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-2103010FA; serial_number=null; software_ass=S30-2103999FA; software_version=05.00.03; supplier_id=1FS; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1f58bf3a84fcdd16407b5e96937e7d2e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154990951&rev=624da02425fa453596e8d6d38cd4af58&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7950010BC_9AD_MFCP_APP_S05.00.11_H1.12_20210715.s19; app_file_version=null; app_file_version_latest=05.00.11; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=MFCP; eol_config_code=-; error_code=1; hardware_version=H1.13; iccid_code=null; manufacture_date=null; part_number=S30-7950010BC; serial_number=null; software_ass=S30-7950999BC; software_version=05.00.11; supplier_id=9AD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=f9a5b5062114139c693ca4080760b49e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29154919&rev=06ce86ed73414887a695ae869c8002e0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115099.s19; app_file_version=null; app_file_version_latest=05.02.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=08a4d3f22f4a3d128f4070bd03b33fdd; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29155000&rev=0fc15dde843541cf992cebc1b335fd8e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115098.s19; driver_file_version=null; driver_file_version_latest=05.02.02; ecode=null; ecu_name=PTC; eol_config_code=-; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-8115010; serial_number=null; software_ass=S30-8115999; software_version=05.02.02; supplier_id=9AF; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=6971c116d126ea40d792c157b47529a4; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184133417&rev=ce0e2815efe54af18b7bb12ac82b0253&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2107099_1VM_OBC_APP_S05.01.02_H1.20_20211113.s19; app_file_version=null; app_file_version_latest=05.01.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=OBC; eol_config_code=-; error_code=1; hardware_version=H1.20; iccid_code=null; manufacture_date=null; part_number=S30-2107010; serial_number=null; software_ass=S30-2107999; software_version=05.01.02; supplier_id=1VM; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=9f7c08185b1aee29551e9a16063642a0; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177252254&rev=dcefdc7b3842475290aca2f8e7a3429a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EV_MPC5606B_prod_2021_11_08_1539_D.05.06.22_DisCcp.s19; app_file_version=null; app_file_version_latest=05.06.22; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=VCU; eol_config_code=0D1D86; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-2108010; serial_number=null; software_ass=S30-2108999; software_version=05.06.22; supplier_id=1FY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=a5de5932e50dde8154f8474404168981; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184227857&rev=13d659267b724bc89af2ade7e7e3546b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2F30_7925010_9AV_TBOX_05.02.49_H1.11_20211210.zip; app_file_version=null; app_file_version_latest=05.02.49; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=TBOX; eol_config_code=01; error_code=1; hardware_version=H1.11; iccid_code=89860921770027188270; manufacture_date=null; part_number=S30-7925010; serial_number=null; software_ass=S30-7925999; software_version=05.02.49; supplier_id=9AV; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1f58bf3a84fcdd16407b5e96937e7d2e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154990951&rev=624da02425fa453596e8d6d38cd4af58&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7950010BC_9AD_MFCP_APP_S05.00.11_H1.12_20210715.s19; app_file_version=null; app_file_version_latest=05.00.11; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=MFCP; eol_config_code=-; error_code=1; hardware_version=H1.13; iccid_code=null; manufacture_date=null; part_number=S30-7950010BC; serial_number=null; software_ass=S30-7950999BC; software_version=05.00.11; supplier_id=9AD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=6971c116d126ea40d792c157b47529a4; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184133417&rev=ce0e2815efe54af18b7bb12ac82b0253&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2107099_1VM_OBC_APP_S05.01.02_H1.20_20211113.s19; app_file_version=null; app_file_version_latest=05.01.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=OBC; eol_config_code=-; error_code=1; hardware_version=H1.20; iccid_code=null; manufacture_date=null; part_number=S30-2107010; serial_number=null; software_ass=S30-2107999; software_version=05.01.02; supplier_id=1VM; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=78dcdadc72e33f2d38852dfd77fb6fa1; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=197035253&rev=2b726a19b81c45739efa37f4c54ba32b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7922099_9TB_LSA_APP_S05.03.01_H1.02_20220320.s19; app_file_version=null; app_file_version_latest=05.03.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=LSA; eol_config_code=00; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-7922010; serial_number=null; software_ass=S30-7922999; software_version=05.03.01; supplier_id=9TB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=5617210ea3be7d2044c1c57623165668; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=118205864&rev=83fe0769b32c46f58f278bbb97c40325&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2103099FA_1FS_MCU_APP_S05.00.03_H1.11_20210310.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=BAPF8E11442; ecu_name=MCU; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-2103010FA; serial_number=null; software_ass=S30-2103999FA; software_version=05.00.03; supplier_id=1FS; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=9f7c08185b1aee29551e9a16063642a0; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177252254&rev=dcefdc7b3842475290aca2f8e7a3429a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EV_MPC5606B_prod_2021_11_08_1539_D.05.06.22_DisCcp.s19; app_file_version=null; app_file_version_latest=05.06.22; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=VCU; eol_config_code=0D1D86; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-2108010; serial_number=null; software_ass=S30-2108999; software_version=05.06.22; supplier_id=1FY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=f9a5b5062114139c693ca4080760b49e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29154919&rev=06ce86ed73414887a695ae869c8002e0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115099.s19; app_file_version=null; app_file_version_latest=05.02.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=08a4d3f22f4a3d128f4070bd03b33fdd; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29155000&rev=0fc15dde843541cf992cebc1b335fd8e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115098.s19; driver_file_version=null; driver_file_version_latest=05.02.02; ecode=null; ecu_name=PTC; eol_config_code=-; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-8115010; serial_number=null; software_ass=S30-8115999; software_version=05.02.02; supplier_id=9AF; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=a5de5932e50dde8154f8474404168981; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184227857&rev=13d659267b724bc89af2ade7e7e3546b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2F30_7925010_9AV_TBOX_05.02.49_H1.11_20211210.zip; app_file_version=null; app_file_version_latest=05.02.49; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=TBOX; eol_config_code=01; error_code=1; hardware_version=H1.11; iccid_code=89860921770027188270; manufacture_date=null; part_number=S30-7925010; serial_number=null; software_ass=S30-7925999; software_version=05.02.49; supplier_id=9AV; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=65a407da52739463c028017eb0c799ae; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=207311000&rev=aaf2ddf904254f5fbfa59e77fad5952e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600099_9AT_BDCM_APP_S05.06.10_H1.12_20220425.s19; app_file_version=null; app_file_version_latest=05.06.10; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=71d44d7e82e055c735001fd608fa7365; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=194808122&rev=371c94cf742e40b99f8af82720701765&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600098_9AT_BDCM_DR_S05.06.09_H1.12_20220312.s19; driver_file_version=null; driver_file_version_latest=05.06.09; ecode=null; ecu_name=BDCM; eol_config_code=09A5A4A120000000; error_code=1; hardware_version=H1.12; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3600999; software_version=05.06.10; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=92f35bb725a3e394bb411a589d95fc90; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=171284186&rev=9f86112d73ea40e2b65889ef3295daad&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FHZ00.03.00_Sq13_DFLASH.srec; app_file_version=null; app_file_version_latest=05.00.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ACU; eol_config_code=33000C0301040000; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3658010BB; serial_number=null; software_ass=S30-3658999BB; software_version=05.00.00; supplier_id=8JH; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=F4A2D451A12F2CC0F2A7329284F7FAAB; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154977610&rev=4e12085fcae04082b8226051b1062475&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_05.13.01_01.02.0E__202106301429_processed.S19; app_file_version=null; app_file_version_latest=05.13.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=09SPE007NA0111C540000276; ecu_name=BMS; eol_config_code=-; error_code=1; hardware_version=H3.10; iccid_code=null; manufacture_date=null; part_number=S30-2101010NA; serial_number=null; software_ass=S30-2101999NA; software_version=05.13.01; supplier_id=1HA; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b69aa3d1be98dd068d67aa643442ba7f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409932&rev=745041a8846c9073&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604099_BDCM_S_APP.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=5670419043b96fd1d9414b1665fe3157; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409935&rev=39110adc4c874061bd03bff39f5a3b14&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604098_BDCM_S_Driver.s19; driver_file_version=null; driver_file_version_latest=05.00.03; ecode=null; ecu_name=BDCM_S; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3604999; software_version=05.00.03; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=FBD256AFF37A7639FF5428F0DCE6E331; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203330&rev=4dc86e3c547042f2b2c3967c2be0e4b5&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103099BB.s19; app_file_version=null; app_file_version_latest=05.00.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=99785D729171D63D2D705A4894331FA4; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203333&rev=c84f682a6ec046b4a2ccef04d07fa1c0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103098BB.s19; driver_file_version=null; driver_file_version_latest=05.00.00; ecode=null; ecu_name=EACP; eol_config_code=-; error_code=1; hardware_version=H0.00; iccid_code=null; manufacture_date=null; part_number=S30-8103010BB; serial_number=null; software_ass=S30-8103999BB; software_version=05.00.00; supplier_id=9CY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b88257c99ddb0bd1a60b243c6f048abc; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169207917&rev=51303ee0151f4558af5fb377e7fe3987&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112099-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E5%BA%94%E7%94%A8%E8%BD%AF%E4%BB%B6.s19; app_file_version=null; app_file_version_latest=05.04.04; cal_MD5=28f8697e661aca7b4eba0c0551a2fd4e; cal_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169255031&rev=60e6a5630b204651a30c19b4ef80bfe3&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112097-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E6%A0%87%E5%AE%9A%E8%BD%AF%E4%BB%B6.s19; cal_file_version=null; cal_file_version_latest=05.04.04; driver_MD5=b558277911aa2085c33fedd82fb79edb; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169241933&rev=54221083dd914a72a90482ea1f5122ac&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112098-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E9%A9%B1%E5%8A%A8%E8%BD%AF%E4%BB%B6.s19; driver_file_version=null; driver_file_version_latest=05.04.04; ecode=null; ecu_name=CLM; eol_config_code=890F000000000000; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-8112010; serial_number=null; software_ass=S30-8112999; software_version=05.04.04; supplier_id=9RY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=8af30a1dc3c89d75ea183a9edd809003; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=11894836&rev=aedf0330c0f3486db42aecd3e409997e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30-EPS-V06.hex; app_file_version=null; app_file_version_latest=05.00.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EPS; eol_config_code=00; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3404010; serial_number=null; software_ass=S30-3404999; software_version=05.00.01; supplier_id=6YB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b567b6ecb74e968980f618ba545a5a63; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=168457441&rev=bb9bf176cc984158bbbdd302660d5787&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EGSM_BD_SW_05.00.08H1.01+0818_DiPei.s19; app_file_version=null; app_file_version_latest=05.00.08; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EGSM; eol_config_code=-; error_code=1; hardware_version=H1.01; iccid_code=null; manufacture_date=null; part_number=S30-3774020BD; serial_number=null; software_ass=S30-3774999BD; software_version=05.00.08; supplier_id=6DP; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=fbf4a8f209dc03c504f686056340110f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=54263846&rev=49e4e690d0eb4c37875f3b2d995f7c44&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3601099_9ML_GW_APP_S05.03.12_H1.10_20210204.s19; app_file_version=null; app_file_version_latest=05.03.12; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=GW; eol_config_code=0800; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3600525; serial_number=null; software_ass=S30-3601999; software_version=05.03.12; supplier_id=9ML; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ESC; eol_config_code=00000003; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3570010BA; serial_number=null; software_ass=S30-3570999BA; software_version=05.01.01; supplier_id=6WD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1c7693635ea640e071085f779a02acb7; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=183133061&rev=14bb23b88a76425e9cb655ffeba18fc7&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7912099BC_9CJ_IHU_APP_S05.00.20_H2.01_20211020.zip; app_file_version=null; app_file_version_latest=05.00.20; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=IHU; eol_config_code=0886010080224000; error_code=1; hardware_version=H2.02; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=HEP302020D524A0008; software_ass=S30-7912999BC; software_version=05.00.20; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=7efd08ece0acacc77b628b7898140982; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=170454338&rev=be4fc58e0fd140e0bd51a87d20a0ee4a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FHZEP30_9CJ_ICU_S30-7912020_05.01.00_H2.01_2021.09.18.S19; app_file_version=null; app_file_version_latest=05.01.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ICU; eol_config_code=A008C40000000000; error_code=1; hardware_version=H2.01; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=null; software_ass=S30-7912999BB; software_version=05.01.00; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=65a407da52739463c028017eb0c799ae; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=207311000&rev=aaf2ddf904254f5fbfa59e77fad5952e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600099_9AT_BDCM_APP_S05.06.10_H1.12_20220425.s19; app_file_version=null; app_file_version_latest=05.06.10; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=71d44d7e82e055c735001fd608fa7365; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=194808122&rev=371c94cf742e40b99f8af82720701765&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600098_9AT_BDCM_DR_S05.06.09_H1.12_20220312.s19; driver_file_version=null; driver_file_version_latest=05.06.09; ecode=null; ecu_name=BDCM; eol_config_code=09A5A4A120000000; error_code=1; hardware_version=H1.12; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3600999; software_version=05.06.10; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b69aa3d1be98dd068d67aa643442ba7f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409932&rev=745041a8846c9073&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604099_BDCM_S_APP.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=5670419043b96fd1d9414b1665fe3157; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409935&rev=39110adc4c874061bd03bff39f5a3b14&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604098_BDCM_S_Driver.s19; driver_file_version=null; driver_file_version_latest=05.00.03; ecode=null; ecu_name=BDCM_S; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3604999; software_version=05.00.03; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=F4A2D451A12F2CC0F2A7329284F7FAAB; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154977610&rev=4e12085fcae04082b8226051b1062475&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_05.13.01_01.02.0E__202106301429_processed.S19; app_file_version=null; app_file_version_latest=05.13.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=09SPE007NA0111C540000276; ecu_name=BMS; eol_config_code=-; error_code=1; hardware_version=H3.10; iccid_code=null; manufacture_date=null; part_number=S30-2101010NA; serial_number=null; software_ass=S30-2101999NA; software_version=05.13.01; supplier_id=1HA; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b88257c99ddb0bd1a60b243c6f048abc; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169207917&rev=51303ee0151f4558af5fb377e7fe3987&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112099-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E5%BA%94%E7%94%A8%E8%BD%AF%E4%BB%B6.s19; app_file_version=null; app_file_version_latest=05.04.04; cal_MD5=28f8697e661aca7b4eba0c0551a2fd4e; cal_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169255031&rev=60e6a5630b204651a30c19b4ef80bfe3&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112097-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E6%A0%87%E5%AE%9A%E8%BD%AF%E4%BB%B6.s19; cal_file_version=null; cal_file_version_latest=05.04.04; driver_MD5=b558277911aa2085c33fedd82fb79edb; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169241933&rev=54221083dd914a72a90482ea1f5122ac&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112098-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E9%A9%B1%E5%8A%A8%E8%BD%AF%E4%BB%B6.s19; driver_file_version=null; driver_file_version_latest=05.04.04; ecode=null; ecu_name=CLM; eol_config_code=890F000000000000; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-8112010; serial_number=null; software_ass=S30-8112999; software_version=05.04.04; supplier_id=9RY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=FBD256AFF37A7639FF5428F0DCE6E331; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203330&rev=4dc86e3c547042f2b2c3967c2be0e4b5&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103099BB.s19; app_file_version=null; app_file_version_latest=05.00.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=99785D729171D63D2D705A4894331FA4; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203333&rev=c84f682a6ec046b4a2ccef04d07fa1c0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103098BB.s19; driver_file_version=null; driver_file_version_latest=05.00.00; ecode=null; ecu_name=EACP; eol_config_code=-; error_code=1; hardware_version=H0.00; iccid_code=null; manufacture_date=null; part_number=S30-8103010BB; serial_number=null; software_ass=S30-8103999BB; software_version=05.00.00; supplier_id=9CY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b567b6ecb74e968980f618ba545a5a63; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=168457441&rev=bb9bf176cc984158bbbdd302660d5787&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EGSM_BD_SW_05.00.08H1.01+0818_DiPei.s19; app_file_version=null; app_file_version_latest=05.00.08; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EGSM; eol_config_code=-; error_code=1; hardware_version=H1.01; iccid_code=null; manufacture_date=null; part_number=S30-3774020BD; serial_number=null; software_ass=S30-3774999BD; software_version=05.00.08; supplier_id=6DP; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=8af30a1dc3c89d75ea183a9edd809003; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=11894836&rev=aedf0330c0f3486db42aecd3e409997e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30-EPS-V06.hex; app_file_version=null; app_file_version_latest=05.00.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EPS; eol_config_code=00; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3404010; serial_number=null; software_ass=S30-3404999; software_version=05.00.01; supplier_id=6YB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ESC; eol_config_code=00000003; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3570010BA; serial_number=null; software_ass=S30-3570999BA; software_version=05.01.01; supplier_id=6WD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=fbf4a8f209dc03c504f686056340110f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=54263846&rev=49e4e690d0eb4c37875f3b2d995f7c44&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3601099_9ML_GW_APP_S05.03.12_H1.10_20210204.s19; app_file_version=null; app_file_version_latest=05.03.12; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=GW; eol_config_code=0800; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3600525; serial_number=null; software_ass=S30-3601999; software_version=05.03.12; supplier_id=9ML; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=7efd08ece0acacc77b628b7898140982; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=170454338&rev=be4fc58e0fd140e0bd51a87d20a0ee4a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FHZEP30_9CJ_ICU_S30-7912020_05.01.00_H2.01_2021.09.18.S19; app_file_version=null; app_file_version_latest=05.01.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ICU; eol_config_code=A008C40000000000; error_code=1; hardware_version=H2.01; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=null; software_ass=S30-7912999BB; software_version=05.01.00; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1c7693635ea640e071085f779a02acb7; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=183133061&rev=14bb23b88a76425e9cb655ffeba18fc7&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7912099BC_9CJ_IHU_APP_S05.00.20_H2.01_20211020.zip; app_file_version=null; app_file_version_latest=05.00.20; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=IHU; eol_config_code=0886010080224000; error_code=1; hardware_version=H2.02; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=HEP302020D524A0008; software_ass=S30-7912999BC; software_version=05.00.20; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=78dcdadc72e33f2d38852dfd77fb6fa1; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=197035253&rev=2b726a19b81c45739efa37f4c54ba32b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7922099_9TB_LSA_APP_S05.03.01_H1.02_20220320.s19; app_file_version=null; app_file_version_latest=05.03.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=LSA; eol_config_code=00; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-7922010; serial_number=null; software_ass=S30-7922999; software_version=05.03.01; supplier_id=9TB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=5617210ea3be7d2044c1c57623165668; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=118205864&rev=83fe0769b32c46f58f278bbb97c40325&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2103099FA_1FS_MCU_APP_S05.00.03_H1.11_20210310.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=BAPF8E11442; ecu_name=MCU; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-2103010FA; serial_number=null; software_ass=S30-2103999FA; software_version=05.00.03; supplier_id=1FS; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1f58bf3a84fcdd16407b5e96937e7d2e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154990951&rev=624da02425fa453596e8d6d38cd4af58&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7950010BC_9AD_MFCP_APP_S05.00.11_H1.12_20210715.s19; app_file_version=null; app_file_version_latest=05.00.11; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=MFCP; eol_config_code=-; error_code=1; hardware_version=H1.13; iccid_code=null; manufacture_date=null; part_number=S30-7950010BC; serial_number=null; software_ass=S30-7950999BC; software_version=05.00.11; supplier_id=9AD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=f9a5b5062114139c693ca4080760b49e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29154919&rev=06ce86ed73414887a695ae869c8002e0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115099.s19; app_file_version=null; app_file_version_latest=05.02.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=08a4d3f22f4a3d128f4070bd03b33fdd; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29155000&rev=0fc15dde843541cf992cebc1b335fd8e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115098.s19; driver_file_version=null; driver_file_version_latest=05.02.02; ecode=null; ecu_name=PTC; eol_config_code=-; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-8115010; serial_number=null; software_ass=S30-8115999; software_version=05.02.02; supplier_id=9AF; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=6971c116d126ea40d792c157b47529a4; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184133417&rev=ce0e2815efe54af18b7bb12ac82b0253&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2107099_1VM_OBC_APP_S05.01.02_H1.20_20211113.s19; app_file_version=null; app_file_version_latest=05.01.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=OBC; eol_config_code=-; error_code=1; hardware_version=H1.20; iccid_code=null; manufacture_date=null; part_number=S30-2107010; serial_number=null; software_ass=S30-2107999; software_version=05.01.02; supplier_id=1VM; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=9f7c08185b1aee29551e9a16063642a0; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177252254&rev=dcefdc7b3842475290aca2f8e7a3429a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EV_MPC5606B_prod_2021_11_08_1539_D.05.06.22_DisCcp.s19; app_file_version=null; app_file_version_latest=05.06.22; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=VCU; eol_config_code=0D1D86; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-2108010; serial_number=null; software_ass=S30-2108999; software_version=05.06.22; supplier_id=1FY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=65a407da52739463c028017eb0c799ae; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=207311000&rev=aaf2ddf904254f5fbfa59e77fad5952e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600099_9AT_BDCM_APP_S05.06.10_H1.12_20220425.s19; app_file_version=null; app_file_version_latest=05.06.10; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=71d44d7e82e055c735001fd608fa7365; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=194808122&rev=371c94cf742e40b99f8af82720701765&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3600098_9AT_BDCM_DR_S05.06.09_H1.12_20220312.s19; driver_file_version=null; driver_file_version_latest=05.06.09; ecode=null; ecu_name=BDCM; eol_config_code=09A5A4A120000000; error_code=1; hardware_version=H1.12; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3600999; software_version=05.06.10; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b69aa3d1be98dd068d67aa643442ba7f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409932&rev=745041a8846c9073&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604099_BDCM_S_APP.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=5670419043b96fd1d9414b1665fe3157; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=3409935&rev=39110adc4c874061bd03bff39f5a3b14&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3604098_BDCM_S_Driver.s19; driver_file_version=null; driver_file_version_latest=05.00.03; ecode=null; ecu_name=BDCM_S; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3600010; serial_number=null; software_ass=S30-3604999; software_version=05.00.03; supplier_id=9AT; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=F4A2D451A12F2CC0F2A7329284F7FAAB; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154977610&rev=4e12085fcae04082b8226051b1062475&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_05.13.01_01.02.0E__202106301429_processed.S19; app_file_version=null; app_file_version_latest=05.13.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=09SPE007NA0111C540000276; ecu_name=BMS; eol_config_code=-; error_code=1; hardware_version=H3.10; iccid_code=null; manufacture_date=null; part_number=S30-2101010NA; serial_number=null; software_ass=S30-2101999NA; software_version=05.13.01; supplier_id=1HA; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b88257c99ddb0bd1a60b243c6f048abc; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169207917&rev=51303ee0151f4558af5fb377e7fe3987&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112099-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E5%BA%94%E7%94%A8%E8%BD%AF%E4%BB%B6.s19; app_file_version=null; app_file_version_latest=05.04.04; cal_MD5=28f8697e661aca7b4eba0c0551a2fd4e; cal_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169255031&rev=60e6a5630b204651a30c19b4ef80bfe3&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112097-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E6%A0%87%E5%AE%9A%E8%BD%AF%E4%BB%B6.s19; cal_file_version=null; cal_file_version_latest=05.04.04; driver_MD5=b558277911aa2085c33fedd82fb79edb; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=169241933&rev=54221083dd914a72a90482ea1f5122ac&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8112098-%E7%A9%BA%E8%B0%83%E6%8E%A7%E5%88%B6%E5%99%A8%E9%A9%B1%E5%8A%A8%E8%BD%AF%E4%BB%B6.s19; driver_file_version=null; driver_file_version_latest=05.04.04; ecode=null; ecu_name=CLM; eol_config_code=890F000000000000; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-8112010; serial_number=null; software_ass=S30-8112999; software_version=05.04.04; supplier_id=9RY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=FBD256AFF37A7639FF5428F0DCE6E331; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203330&rev=4dc86e3c547042f2b2c3967c2be0e4b5&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103099BB.s19; app_file_version=null; app_file_version_latest=05.00.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=99785D729171D63D2D705A4894331FA4; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=42203333&rev=c84f682a6ec046b4a2ccef04d07fa1c0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8103098BB.s19; driver_file_version=null; driver_file_version_latest=05.00.00; ecode=null; ecu_name=EACP; eol_config_code=-; error_code=1; hardware_version=H0.00; iccid_code=null; manufacture_date=null; part_number=S30-8103010BB; serial_number=null; software_ass=S30-8103999BB; software_version=05.00.00; supplier_id=9CY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=b567b6ecb74e968980f618ba545a5a63; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=168457441&rev=bb9bf176cc984158bbbdd302660d5787&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EGSM_BD_SW_05.00.08H1.01+0818_DiPei.s19; app_file_version=null; app_file_version_latest=05.00.08; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EGSM; eol_config_code=-; error_code=1; hardware_version=H1.01; iccid_code=null; manufacture_date=null; part_number=S30-3774020BD; serial_number=null; software_ass=S30-3774999BD; software_version=05.00.08; supplier_id=6DP; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=8af30a1dc3c89d75ea183a9edd809003; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=11894836&rev=aedf0330c0f3486db42aecd3e409997e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30-EPS-V06.hex; app_file_version=null; app_file_version_latest=05.00.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EPS; eol_config_code=00; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3404010; serial_number=null; software_ass=S30-3404999; software_version=05.00.01; supplier_id=6YB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ESC; eol_config_code=00000003; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-3570010BA; serial_number=null; software_ass=S30-3570999BA; software_version=05.01.01; supplier_id=6WD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=fbf4a8f209dc03c504f686056340110f; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=54263846&rev=49e4e690d0eb4c37875f3b2d995f7c44&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3601099_9ML_GW_APP_S05.03.12_H1.10_20210204.s19; app_file_version=null; app_file_version_latest=05.03.12; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=GW; eol_config_code=0800; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-3600525; serial_number=null; software_ass=S30-3601999; software_version=05.03.12; supplier_id=9ML; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=7efd08ece0acacc77b628b7898140982; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=170454338&rev=be4fc58e0fd140e0bd51a87d20a0ee4a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FHZEP30_9CJ_ICU_S30-7912020_05.01.00_H2.01_2021.09.18.S19; app_file_version=null; app_file_version_latest=05.01.00; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=ICU; eol_config_code=A008C40000000000; error_code=1; hardware_version=H2.01; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=null; software_ass=S30-7912999BB; software_version=05.01.00; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1c7693635ea640e071085f779a02acb7; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=183133061&rev=14bb23b88a76425e9cb655ffeba18fc7&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7912099BC_9CJ_IHU_APP_S05.00.20_H2.01_20211020.zip; app_file_version=null; app_file_version_latest=05.00.20; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=IHU; eol_config_code=0886010080224000; error_code=1; hardware_version=H2.02; iccid_code=null; manufacture_date=null; part_number=S30-7912020; serial_number=HEP302020D524A0008; software_ass=S30-7912999BC; software_version=05.00.20; supplier_id=9CJ; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=78dcdadc72e33f2d38852dfd77fb6fa1; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=197035253&rev=2b726a19b81c45739efa37f4c54ba32b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7922099_9TB_LSA_APP_S05.03.01_H1.02_20220320.s19; app_file_version=null; app_file_version_latest=05.03.01; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=LSA; eol_config_code=00; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-7922010; serial_number=null; software_ass=S30-7922999; software_version=05.03.01; supplier_id=9TB; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=5617210ea3be7d2044c1c57623165668; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=118205864&rev=83fe0769b32c46f58f278bbb97c40325&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2103099FA_1FS_MCU_APP_S05.00.03_H1.11_20210310.s19; app_file_version=null; app_file_version_latest=05.00.03; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=BAPF8E11442; ecu_name=MCU; eol_config_code=-; error_code=1; hardware_version=H1.11; iccid_code=null; manufacture_date=null; part_number=S30-2103010FA; serial_number=null; software_ass=S30-2103999FA; software_version=05.00.03; supplier_id=1FS; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=1f58bf3a84fcdd16407b5e96937e7d2e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=154990951&rev=624da02425fa453596e8d6d38cd4af58&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-7950010BC_9AD_MFCP_APP_S05.00.11_H1.12_20210715.s19; app_file_version=null; app_file_version_latest=05.00.11; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=MFCP; eol_config_code=-; error_code=1; hardware_version=H1.13; iccid_code=null; manufacture_date=null; part_number=S30-7950010BC; serial_number=null; software_ass=S30-7950999BC; software_version=05.00.11; supplier_id=9AD; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=f9a5b5062114139c693ca4080760b49e; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29154919&rev=06ce86ed73414887a695ae869c8002e0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115099.s19; app_file_version=null; app_file_version_latest=05.02.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=08a4d3f22f4a3d128f4070bd03b33fdd; driver_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=29155000&rev=0fc15dde843541cf992cebc1b335fd8e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-8115098.s19; driver_file_version=null; driver_file_version_latest=05.02.02; ecode=null; ecu_name=PTC; eol_config_code=-; error_code=1; hardware_version=H1.02; iccid_code=null; manufacture_date=null; part_number=S30-8115010; serial_number=null; software_ass=S30-8115999; software_version=05.02.02; supplier_id=9AF; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=6971c116d126ea40d792c157b47529a4; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184133417&rev=ce0e2815efe54af18b7bb12ac82b0253&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-2107099_1VM_OBC_APP_S05.01.02_H1.20_20211113.s19; app_file_version=null; app_file_version_latest=05.01.02; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=OBC; eol_config_code=-; error_code=1; hardware_version=H1.20; iccid_code=null; manufacture_date=null; part_number=S30-2107010; serial_number=null; software_ass=S30-2107999; software_version=05.01.02; supplier_id=1VM; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=9f7c08185b1aee29551e9a16063642a0; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177252254&rev=dcefdc7b3842475290aca2f8e7a3429a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FEP30_EV_MPC5606B_prod_2021_11_08_1539_D.05.06.22_DisCcp.s19; app_file_version=null; app_file_version_latest=05.06.22; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=VCU; eol_config_code=0D1D86; error_code=1; hardware_version=H1.10; iccid_code=null; manufacture_date=null; part_number=S30-2108010; serial_number=null; software_ass=S30-2108999; software_version=05.06.22; supplier_id=1FY; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=a5de5932e50dde8154f8474404168981; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184227857&rev=13d659267b724bc89af2ade7e7e3546b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2F30_7925010_9AV_TBOX_05.02.49_H1.11_20211210.zip; app_file_version=null; app_file_version_latest=05.02.49; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=TBOX; eol_config_code=01; error_code=1; hardware_version=H1.11; iccid_code=89860921770027188270; manufacture_date=null; part_number=S30-7925010; serial_number=null; software_ass=S30-7925999; software_version=05.02.49; supplier_id=9AV; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=a5de5932e50dde8154f8474404168981; app_file_path=https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=184227857&rev=13d659267b724bc89af2ade7e7e3546b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2F30_7925010_9AV_TBOX_05.02.49_H1.11_20211210.zip; app_file_version=null; app_file_version_latest=05.02.49; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=TBOX; eol_config_code=01; error_code=1; hardware_version=H1.11; iccid_code=89860921770027188270; manufacture_date=null; part_number=S30-7925010; serial_number=null; software_ass=S30-7925999; software_version=05.02.49; supplier_id=9AV; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=EACP; eol_config_code=-; error_code=1; hardware_version=null; iccid_code=null; manufacture_date=null; part_number=null; serial_number=null; software_ass=null; software_version=null; supplier_id=null; vin=LUZBGAFB1NA018786; }; ecuArray=ECU{app_MD5=null; app_file_path=null; app_file_version=null; app_file_version_latest=null; cal_MD5=null; cal_file_path=null; cal_file_version=null; cal_file_version_latest=null; driver_MD5=null; driver_file_path=null; driver_file_version=null; driver_file_version_latest=null; ecode=null; ecu_name=OBC; eol_config_code= -; error_code=1; hardware_version=null; iccid_code=null; manufacture_date=null; part_number=null; serial_number=null; software_ass=null; software_version=null; supplier_id=null; vin=LUZBGAFB1NA018786; }; egsm_isconfig=false; ehb_isconfig=false; eps_isconfig=false; error_code=0; esc_isconfig=false; esk=7c; esk=64; esk=62; esk=6; esk=76; esk=77; esk=cf; esk=2d; esk=36; esk=a7; esk=36; esk=c6; esk=fa; esk=f9; esk=f8; esk=fb; flc_isconfig=false; flr_isconfig=false; gw_isconfig=false; icu_isconfig=false; ihu_isconfig=false; info=null; irs_isconfig=false; lsa_isconfig=false; mcu_isconfig=false; mfcp_isconfig=false; model_name=S30; obc_isconfig=false; pin=4917; plg_isconfig=false; ptc_isconfig=false; tap_isconfig=false; tbox_isconfig=false; variant_coding=null; vcu_isconfig=false; vin=LUZBGAFB1NA018786; wpc_isconfig=false; }";


        if (result == null) return false;
        if (result.length() < 20) return false;
        m_blueservice.PrintLog(result);

        //解析
        DomXmlService domxml = new DomXmlService();
        try {
            domxml.getPersonsMesJson(result);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //String vin = domxml.getvin();

        Common.cartype.ecus = domxml.getEcus();

        //分析
        //String v_partname = Common.cartype.getModuleStr("OBC", "partNumber");
        // String v_ivnname = Common.cartype.getModuleStr("OBC", "hardwareVersion");


        byte[] v_pin = new byte[2];
        String pinstr = domxml.getpin();
        if (pinstr != null)
            Commonfunc.StringToBytes(pinstr, v_pin, 2);
        Common.cartype.pin[2] = v_pin[0];
        Common.cartype.pin[3] = v_pin[1];
        Common.cartype.car = domxml.getmodelName();
        Common.cartype.system = domxml.getmodelName();
        if (Common.cartype.car.equals("S11")) {
            Common.cartype.car = "EP12";
        }
        Common.cartype.name = domxml.getbodyColor();
        Commonfunc.StringToBytes(domxml.getesk(), Common.cartype.esk, 16);
        //检查车型
//		if(Common.cartype.car.equals("S30") == false)
//			return false;
        if (Common.cartype.car.equals("S30")) {
            //获取TPMS信息
            methodName = "getEolTireByVin";
            SoapObject requesttpms = new SoapObject(namespace, methodName);
            requesttpms.addProperty("vin", Pvin);

            envelope.bodyOut = requesttpms;
            //创建SoapSerializationEnvelope 对象，同时指定soap版本号(之前在wsdl中看到的)
            try {
                httpTransportSE.call(null, envelope);
            } catch (XmlPullParserException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }//调用

            // 获取返回的数据
            SoapObject objecttpms = (SoapObject) envelope.bodyIn;
            // 获取返回的结果
            String result_tpms = objecttpms.getProperty(0).toString();
            if (result_tpms == null) return false;
            if (result_tpms.length() < 20) return false;
            //查找左前
            int v_start = result_tpms.indexOf("lf_id=", 0);
            int v_end = 0;
            String t_id = "";
            if (v_start > 0) {
                v_end = result_tpms.indexOf(";", v_start);
                if (v_end > 0) {
                    t_id = result_tpms.substring(v_start + 6, v_end);
                    if ("null".equals(t_id))
                        t_id = "";
                    if (t_id.length() > 0)
                        Commonfunc.StringToBytes(t_id, Common.cartype.flid, 4);
                }
            }
            //左后
            v_start = result_tpms.indexOf("lr_id=", 0);
            if (v_start > 0) {
                v_end = result_tpms.indexOf(";", v_start);
                if (v_end > 0) {
                    t_id = result_tpms.substring(v_start + 6, v_end);
                    if ("null".equals(t_id))
                        t_id = "";
                    if (t_id.length() > 0)
                        Commonfunc.StringToBytes(t_id, Common.cartype.blid, 4);
                }
            }
            //右前
            v_start = result_tpms.indexOf("rf_id=", 0);
            if (v_start > 0) {
                v_end = result_tpms.indexOf(";", v_start);
                if (v_end > 0) {
                    t_id = result_tpms.substring(v_start + 6, v_end);
                    if ("null".equals(t_id))
                        t_id = "";
                    if (t_id.length() > 0)
                        Commonfunc.StringToBytes(t_id, Common.cartype.frid, 4);
                }
            }
            //右后
            v_start = result_tpms.indexOf("rr_id=", 0);
            if (v_start > 0) {
                v_end = result_tpms.indexOf(";", v_start);
                if (v_end > 0) {
                    t_id = result_tpms.substring(v_start + 6, v_end);
                    if ("null".equals(t_id))
                        t_id = "";
                    if (t_id.length() > 0)
                        Commonfunc.StringToBytes(t_id, Common.cartype.brid, 4);
                }
            }
        }

        if (Common.cartype.ecus != null)
            return true;
        else
            return false;
        //获取胎压信息
    }

    public boolean getMesVinInfoThai(String Pvin) throws IOException {
        String v_carinfo = "";
        String URL = Common.mes_url;
        String para = "{\n" +
                "    \"Inputs\": {\n" +
                "        \"Header\": {\n" +
                "            \"TaskId\": \"bb01a3cd-987f-469d-9e0f-04dffa2d3278\",\n" +
                "            \"InterfaceCode\": \"IF0002\"\n" +
                "        },\n" +
                "        \"Data\": {\n" +
                "            \"ElementList\": [\n" +
                "                {\n" +
                "                    \"VIN\": \"" + Pvin + "\"\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    }\n" +
                "}";

        String result = "";
        for (int k = 0; k < 3; k++) {
            result = HttpRequest.sendPostToMyMes(URL, para);
			/*result = "{\n" +
					"    \"Outputs\": {\n" +
					"        \"Message\": \"响应成功\",\n" +
					"        \"VIN\": \"MRTAGBGAXPA000025\",\n" +
					"        \"variantCoding\": \"0\",\n" +
					"        \"bodyColor\": \"สีเขียว\",\n" +
					"        \"errorCode\": \"0\",\n" +
					"        \"TaskId\": \"bb01a3cd-987f-469d-9e0f-04dffa2d3278\",\n" +
					"        \"esk\": \"7D6264686274CF2E2E2E2CC9F9FAFAF9\",\n" +
					"        \"__Routing__\": false,\n" +
					"        \"modelName\": \"S12\",\n" +
					"        \"TABLEMODULEECUARRAYS\": [\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"MCU\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2103999BB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.07\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=244950686&rev=df58992389c646b5bd362a49a2acd5e6&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2103099BB_1AB_MCU_APP_S05.00.07_H1.10_20220901.s19\",\n" +
					"                \"APP_MD5\": \"2220436cac3b2c44568f5ebfe89a8b61\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1AB\",\n" +
					"                \"PART_NUMBER\": \"S11-2103010BB\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.07\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"\",\n" +
					"                \"SERIAL_NUMBER\": \"\",\n" +
					"                \"ICCID_CODE\": \"\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.07\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"MCU\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2103999BB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"05.00.07\",\n" +
					"                \"DRIVER_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=244951013&rev=9d489aa85e06435f9198860558199c05&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2103098BB_1AB_MCU_flashdriver_S05.00.07_H1.10_20201016.s19\",\n" +
					"                \"DRIVER_MD5\": \"21aa74bc1e2ff409092f2600aa96c8f3\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"APP_FILE_PATH\": \"\",\n" +
					"                \"APP_MD5\": \"\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1AB\",\n" +
					"                \"PART_NUMBER\": \"S11-2103010BB\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.07\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"\",\n" +
					"                \"SERIAL_NUMBER\": \"\",\n" +
					"                \"ICCID_CODE\": \"\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"05.00.07\",\n" +
					"                \"APP_FILE_VERSION\": \"\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"BDCM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3600999AJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.01\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=317003516&rev=591073b295d04fa6b46028750344ffec&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-3600099AJ_9AT_BDCM_OTS_05.00.01_H1.10_20230615.s19\",\n" +
					"                \"APP_MD5\": \"a2e1047097ead113e204fb8624f0a142\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9AT\",\n" +
					"                \"PART_NUMBER\": \"S11-3600010AJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.01\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"BDCM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3600999AJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"05.00.01\",\n" +
					"                \"DRIVER_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=317003519&rev=05bc60bd998f4a6c82553ca80b60d9d0&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-3600098AJ_9AT_BDCM_OTS_05.00.01_H1.10_20230615.s19\",\n" +
					"                \"DRIVER_MD5\": \"1452c9b8f105186bb56f7c4bdb8fdfda\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"APP_FILE_PATH\": \"\",\n" +
					"                \"APP_MD5\": \"\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9AT\",\n" +
					"                \"PART_NUMBER\": \"S11-3600010AJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"05.00.01\",\n" +
					"                \"APP_FILE_VERSION\": \"\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999AD\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.03\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=252234335&rev=705061e02a434b4e8f9e1b19572525e2&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107099AD_1YB_OBC_APP_S05.00.03_H1.11_20221016.s19\",\n" +
					"                \"APP_MD5\": \"1ac650781c60570a72d1bcca43c05cf8\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1YB\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010AD\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.11\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.03\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.03\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EACP\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8103999CA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"05.00.03\",\n" +
					"                \"DRIVER_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=252704069&rev=3ee5b558942249e7a5e4765bf71072a6&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8103098CA_9WQ_EACP_DR_S05.00.03_H0.01_20221111.s19\",\n" +
					"                \"DRIVER_MD5\": \"99785d729171d63d2d705a4894331fa4\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"APP_FILE_PATH\": \"\",\n" +
					"                \"APP_MD5\": \"\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9WQ\",\n" +
					"                \"PART_NUMBER\": \"S11-8103010CA\",\n" +
					"                \"HARD_WAREVERSION\": \"H0.01\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.03\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"05.00.03\",\n" +
					"                \"APP_FILE_VERSION\": \"\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EACP\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8103999BA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.01\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=15322282&rev=55ebbec276fe488bb25b5f02dec06263&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3774099_9CY_EGSM_APP_S05.00.01_H0.01_20200831.s19\",\n" +
					"                \"APP_MD5\": \"40045f9e0b8d14bb461b51f15ed47b14\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9CY\",\n" +
					"                \"PART_NUMBER\": \"S11-8103010BA\",\n" +
					"                \"HARD_WAREVERSION\": \"H0.01\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.01\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EPBi\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3540999CA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.09\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=248193323&rev=0364212d593e4ea79b74c26c910c3dec&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-3540099CA_6AL_EPBi_APP_S05.00.09_H1.02H1.03_20220915.s19\",\n" +
					"                \"APP_MD5\": \"8e9f2e91a72267646d8c41906d5163c8\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"6AL\",\n" +
					"                \"PART_NUMBER\": \"S11-3540010CA\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.02/H1.03\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.09\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.09\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EACP\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8103999CA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.03\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=252702956&rev=47a36c68ecb54c0dae875c72adf0867f&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8103099CA_9WQ_EACP_APP_05.00.03_H0.01_20221123.s19\",\n" +
					"                \"APP_MD5\": \"b6903fc4ace32caec8c4f02106271ac7\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9WQ\",\n" +
					"                \"PART_NUMBER\": \"S11-8103010CA\",\n" +
					"                \"HARD_WAREVERSION\": \"H0.01\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.03\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.03\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999BD\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.12\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=240112241&rev=8d55de851187439192bf25f8fab52a8c&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099BD_9GG_CLM_APP_05.00.12_H1.10_20220720.s19\",\n" +
					"                \"APP_MD5\": \"7804eb93e3a831319c11e8dbc828002c\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9GG\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010BD\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.12\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.12\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999AB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.01.02\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=220458296&rev=fd58178cd4d342d3a5668adb4689826b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099AB_9SD_CLM_APP_05.01.02_H1.10_20220702.S19\",\n" +
					"                \"APP_MD5\": \"222c7733ed3a6be0f10ef63a66f7baea\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9SD\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010AB\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.01.02\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.01.02\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999EA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.00\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=316604492&rev=b1d70e116c534674b317c0f9c4a0bc6c&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099EA_9SD_CLM_APP_05.00.00_H2.10_20230602.S19\",\n" +
					"                \"APP_MD5\": \"eef3b0322f3514cfdc10bf9960b03614\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9SD\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010EA\",\n" +
					"                \"HARD_WAREVERSION\": \"H2.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.00\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.00\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999EB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.00\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=316604489&rev=7fba418f9f75b279&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099EB_9GG_CLM_APP_05.00.00_H1.10_20230602.s19\",\n" +
					"                \"APP_MD5\": \"873663f6c2d75fd51d8803fdea544f00\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9GG\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010EB\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.00\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.00\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"ESC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3570999\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.06\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=320080873&rev=8f8185f06d7940b89929066f8a03d302&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-3570099_6LC_ESC_ApplicationSoftware_05.00.06_H1.10_20230828.S19\",\n" +
					"                \"APP_MD5\": \"5e1fb9d02eaeecbcbe041dfb1730fa0f\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"6LC\",\n" +
					"                \"PART_NUMBER\": \"S11-3570010\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.06\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.06\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"IHU\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-7999999AC\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"04.00.09\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=320939452&rev=54caa22d3f854801a51da197bb4bdb21&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-7999099AC_9BC_IHU_ApplicationSoftware_04.00.09_H1.12_20230922.zip\",\n" +
					"                \"APP_MD5\": \"aaf1f3ae4a7a2d0a904ba3de211fe9af\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9BC\",\n" +
					"                \"PART_NUMBER\": \"S11-7900050AC\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.12\",\n" +
					"                \"SOFT_WAREVERSION\": \"04.00.09\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"\",\n" +
					"                \"SERIAL_NUMBER\": \"HEP1250ACE822C0006\",\n" +
					"                \"ICCID_CODE\": \"\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"04.00.09\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"WPC\",\n" +
					"                \"SOFTWARE_ASS\": \"S30-3730999BA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.06\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=248124998&rev=fb58ea13bc964918a65392200079f14b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS30-3730011BA_9JE_WPC_APP_S05.00.06_H1.20_20220816.S19\",\n" +
					"                \"APP_MD5\": \"5879688086e1c32db65a01cccf20fe79\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9JE\",\n" +
					"                \"PART_NUMBER\": \"S30-3730011BA\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.20\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.06\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.06\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999CD\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.02.04\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=238447472&rev=95c06a2dd1ba47fcb10133d461937ab1&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099CD_9SD_CLM_APP_05.02.04_H2.10_20220725.S19\",\n" +
					"                \"APP_MD5\": \"b6558203f79848b7632ea20a7c86ad47\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9SD\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010CD\",\n" +
					"                \"HARD_WAREVERSION\": \"H2.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.02.04\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.02.04\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EVCC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2161999DW\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.10.01\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=288552071&rev=875d9ea2db9b4c7f83f26dfe820c460c&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2161099_EVCC_APP_05.10.01_H1.10_1VM_20230218+.s19\",\n" +
					"                \"APP_MD5\": \"38ff5eb6d836c17d34a48348528b3afb\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2101080DW\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.10.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.10.01\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EACP\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8103999BA\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.04\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=260213144&rev=0d4924d0750f4894adb33759da25e03d&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8103099BA_9CY_EACP_APP_S05.00.04_H0.01_20221121.s19\",\n" +
					"                \"APP_MD5\": \"acc9f957f351bc03d76cbf9876a24b2c\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9CY\",\n" +
					"                \"PART_NUMBER\": \"S11-8103010BC\",\n" +
					"                \"HARD_WAREVERSION\": \"H0.01\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.04\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.04\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"TBOX\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-7925999BC\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.10\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=318008410&rev=f0c22b1e479b415bb5d08e656def1c39&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-7925099BC_9YD_TBOX_ApplicationSoftware_05.00.10_H1.11_20230711.bin\",\n" +
					"                \"APP_MD5\": \"4a4faad03a1f1c5f60f79b6c5d292c79\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9YD\",\n" +
					"                \"PART_NUMBER\": \"S11-7925010BC\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.11\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.10\",\n" +
					"                \"EOL_CONFIG_CODE\": \"010031001011A600097330AF14E91841F04989B811804ADFFFC4031420323330840300000000014004610204AE000001000818000001218042B8\",\n" +
					"                \"ECODE\": \"\",\n" +
					"                \"SERIAL_NUMBER\": \"\",\n" +
					"                \"ICCID_CODE\": \"精确追溯件条码\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.10\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999BJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.02.01\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=144389891&rev=717be5668a8c431482b5097aad5ff985&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107099BJ_1VM_OBC_APP_S05.02.01_H1.20_20210605.s19\",\n" +
					"                \"APP_MD5\": \"bac11f54b84c4d6a6a7bca369fc78223\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010BJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.20\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.02.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.02.01\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999BJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.01.02\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=129569318&rev=49563b773b6c45b7b7dfc4f656276e51&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107010BJ_1VM_OBC_APP_S05.01.02_H1.10_20210425.s19\",\n" +
					"                \"APP_MD5\": \"7f872523cdc4701108b4f925e016c1f0\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010BJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.01.02\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.01.02\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EPS\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3404999\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.00.05\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177204170&rev=526130f1aa7c4e08856f3cd539b7b6a7&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-3404099_6HL_EPS_APP_S05.00.02_H1.10_20211109.hex\",\n" +
					"                \"APP_MD5\": \"5468bc6375f2c5a8c52c6c4028146551\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"6HL\",\n" +
					"                \"PART_NUMBER\": \"S11-3404010\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.00.05\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.00.05\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999BJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.01.01\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=95812511&rev=af4046a499755c9b&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107010BJ_1VM_OBC_APP_S05.01.01_H1.10_20210324.s19\",\n" +
					"                \"APP_MD5\": \"79436a25793ae3fadd9f402ac9aafa57\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010BJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.01.01\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.01.01\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"ACU\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-3658999\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.01.00\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=15276073&rev=3f8bc79dad9941888814bcd303fb8e0e&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FACU+%E8%BD%AF%E4%BB%B6%E5%8C%85.txt\",\n" +
					"                \"APP_MD5\": \"/\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"8DF\",\n" +
					"                \"PART_NUMBER\": \"S11-3658010\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.05\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.01.00\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.01.00\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999BJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.02.05\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=171244778&rev=3db3ea429e82425e91f024e292392b70&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107099BJ_1VM_OBC_APP_S05.02.05_H1.20_20211013.s19\",\n" +
					"                \"APP_MD5\": \"245d995f32dc9845f2becfa997d44989\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010BJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.20\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.02.05\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.02.05\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"OBC\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-2107999BJ\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.02.02\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=152870461&rev=4c1e44594b2545568e560e3378691860&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-2107010BJ_1VM_OBC_APP_S05.02.02_H1.20_20210701.s19\",\n" +
					"                \"APP_MD5\": \"4e485d49c9eb66b3f8a0837cf1bfc2fd\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"1VM\",\n" +
					"                \"PART_NUMBER\": \"S11-2107010BJ\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.20\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.02.02\",\n" +
					"                \"EOL_CONFIG_CODE\": \"01\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.02.02\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"EACP\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8103999BB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"05.01.02\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=134999264&rev=545494709b684dda91b386dce56d912a&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FCV100_320Vs_20210104_ForTest%281%29.bin\",\n" +
					"                \"APP_MD5\": \"/\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9QD\",\n" +
					"                \"PART_NUMBER\": \"S11-8103010BB\",\n" +
					"                \"HARD_WAREVERSION\": \"H0.01\",\n" +
					"                \"SOFT_WAREVERSION\": \"05.01.02\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"05.01.02\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"CAR_ERROR_CODE\": \"\",\n" +
					"                \"ECU_NAME\": \"CLM\",\n" +
					"                \"SOFTWARE_ASS\": \"S11-8112999AB\",\n" +
					"                \"DRIVER_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"DRIVER_FILE_PATH\": \"\",\n" +
					"                \"DRIVER_MD5\": \"\",\n" +
					"                \"APP_FILE_VERSION_LATEST\": \"04.00.00\",\n" +
					"                \"APP_FILE_PATH\": \"https://pan.hozonauto.com/custom-interfaces/file/content?path_type=self&neid=177204239&rev=3272844e78cc4f51b14d786401f372ee&path=%2F%E8%BD%AF%E4%BB%B6%E6%95%B0%E6%8D%AE%E4%B8%8B%E5%8F%91%E9%A1%B9%E7%9B%AE%E6%8E%A5%E5%8F%A3%E6%96%87%E4%BB%B6%2FS11-8112099AB_9SD_CLM_APP_04.00.00_H1.10_20211012+%281%29.S19\",\n" +
					"                \"APP_MD5\": \"8e12250b0d51ab68a139a3d28bfbe5a1\",\n" +
					"                \"CAL_FILE_VERSION_LATEST\": \"\",\n" +
					"                \"CAL_FILE_PATH\": \"\",\n" +
					"                \"CAL_MD5\": \"\",\n" +
					"                \"MANUFACTURE_DATE\": \"\",\n" +
					"                \"SUPPLIERID\": \"9SD\",\n" +
					"                \"PART_NUMBER\": \"S11-8112010AB\",\n" +
					"                \"HARD_WAREVERSION\": \"H1.10\",\n" +
					"                \"SOFT_WAREVERSION\": \"04.00.00\",\n" +
					"                \"EOL_CONFIG_CODE\": \"-\",\n" +
					"                \"ECODE\": \"-\",\n" +
					"                \"SERIAL_NUMBER\": \"-\",\n" +
					"                \"ICCID_CODE\": \"-\",\n" +
					"                \"DRIVER_FILE_VERSION\": \"\",\n" +
					"                \"APP_FILE_VERSION\": \"04.00.00\",\n" +
					"                \"CAL_FILE_VERSION\": \"\"\n" +
					"            }\n" +
					"        ],\n" +
					"        \"Code\": \"S\",\n" +
					"        \"pin\": \"5142\"\n" +
					"    }\n" +
					"}\n";*/
            if (result.indexOf("响应成功") > 0) //ok
            {
                break;
            }
        }


        if (result == null) return false;
        if (result.length() < 20) return false;
        m_blueservice.PrintLog(result);

        //解析
        DomXmlService domxml = new DomXmlService();
        try {
            domxml.getDataFromThaiMesJson(result);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //String vin = domxml.getvin();

        Common.cartype.ecus = domxml.getEcus();

        //分析
        //String v_partname = Common.cartype.getModuleStr("OBC", "partNumber");
        // String v_ivnname = Common.cartype.getModuleStr("OBC", "hardwareVersion");


        byte[] v_pin = new byte[2];
        String pinstr = domxml.getpin();
        if (pinstr != null)
            Commonfunc.StringToBytes(pinstr, v_pin, 2);
        Common.cartype.pin[2] = v_pin[0];
        Common.cartype.pin[3] = v_pin[1];
        Common.cartype.car = domxml.getmodelName();
        Common.cartype.system = domxml.getmodelName();
        if (Common.cartype.car.equals("S11") || Common.cartype.car.equals("S12")) {
            Common.cartype.car = "EP12";
        }
        Common.cartype.name = domxml.getbodyColor();
        Commonfunc.StringToBytes(domxml.getesk(), Common.cartype.esk, 16);
        //检查车型
//		if(Common.cartype.car.equals("S30") == false)
//			return false;
        if (Common.cartype.car.equals("S30")) {
            //获取TPMS信息

        }

        if (Common.cartype.ecus != null)
            return true;
        else
            return false;
        //获取胎压信息
    }

    public boolean getMesVinInfofromEol(String Pvin) throws IOException, JSONException {
        String v_carinfo = "";
        JSONObject jsonget = new JSONObject();
        jsonget.put("vin", Pvin);

        String v_getInfo = "";
        for (int k = 0; k < 3; k++) {
            m_blueservice.PrintLog("Request Connect time:" + (k + 1));
            v_getInfo = HttpRequest.sendGetJson(Common.mes_eol, jsonget.toString());
            //v_getInfo = "{\"msg\":\"操作成功\",\"code\":200,\"data\":{\"id\":5,\"vin\":\"0x56-PA000021\",\"modelName\":\"S12\",\"bodyColor\":\"灰\",\"esk\":\"7D6264026270CF2E2EAE2CCDF9FAFAF9\",\"info\":null,\"pin\":\"D945\",\"variantCoding\":null,\"errorCode\":null,\"tableModuleEcuarrays\":[{\"id\":144,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-3570099_6LC_ESC_ApplicationSoftware_05.00.07_H1.10_20230828.S19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.07\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"ESC\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3570010\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3570999\",\"softwareVersion\":\"05.00.07\",\"supplierId\":\"6LC\"},{\"id\":145,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.01.00\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"ACU\",\"eolConfigCode\":\"-\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.05\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3658010\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3658999\",\"softwareVersion\":\"05.01.00\",\"supplierId\":\"8DF\"},{\"id\":146,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.01\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"BDCM\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3600010AJ\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3600999AJ\",\"softwareVersion\":\"05.00.01\",\"supplierId\":\"9AT\"},{\"id\":147,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.10.01\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"EVCC\",\"eolConfigCode\":\"\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-2101080DW\",\"serialNumber\":\"\",\"softwareAss\":\"S11-2161999DW\",\"softwareVersion\":\"05.10.01\",\"supplierId\":\"1VM\"},{\"id\":148,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.23.01\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"03HPB0B50001A4DB20000003\",\"ecuName\":\"HVM\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H3.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-2101010GB\",\"serialNumber\":\"\",\"softwareAss\":\"S11-2101999GB\",\"softwareVersion\":\"05.23.01\",\"supplierId\":\"1GX\"},{\"id\":149,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.02.04\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"CLM\",\"eolConfigCode\":\"\",\"errorCode\":\"\",\"hardwareVersion\":\"H2.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-8112010CD\",\"serialNumber\":\"\",\"softwareAss\":\"S11-8112999CD\",\"softwareVersion\":\"05.02.04\",\"supplierId\":\"9SD\"},{\"id\":150,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.04\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"EACP\",\"eolConfigCode\":\"\",\"errorCode\":\"\",\"hardwareVersion\":\"H0.01\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-8103010BC\",\"serialNumber\":\"\",\"softwareAss\":\"S11-8103999BA\",\"softwareVersion\":\"05.00.04\",\"supplierId\":\"9CY\"},{\"id\":151,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-3540099CA_6AL_EPBi_ApplicationSoftware_05.00.14_H1.03_20231108.s19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.09\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"EPBi\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.03\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3540010CA\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3540999CA\",\"softwareVersion\":\"05.00.14\",\"supplierId\":\"6AL\"},{\"id\":152,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-3404099BA_6HL_EPS_ApplicationSoftware_05.00.05_H1.10_20231116.S19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.05\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"S11-3404098BA_6HL_EPS_DR_S05.00.04_H1.10_20211125.S19\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"05.00.05\",\"ecode\":\"\",\"ecuName\":\"EPS1\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3404010BA\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3404999BA\",\"softwareVersion\":\"05.00.05\",\"supplierId\":\"6HL\"},{\"id\":153,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-3820099AG_9XD_ICU_ApplicationSoftware_05.00.09_H1.02_20231109.s19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.09\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"ICU\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.02\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-3820010AG\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3820999AG\",\"softwareVersion\":\"05.00.09\",\"supplierId\":\"9XD\"},{\"id\":154,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.31\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"IHU\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.12\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-7900050AC\",\"serialNumber\":\"HEP1250ACE822C0008\",\"softwareAss\":\"S11-7999999AC\",\"softwareVersion\":\"05.00.31\",\"supplierId\":\"9BC\"},{\"id\":155,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.07\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"FAPZ9H00018\",\"ecuName\":\"MCU\",\"eolConfigCode\":\"\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.10\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-2103010BB\",\"serialNumber\":\"\",\"softwareAss\":\"S11-2103999BB\",\"softwareVersion\":\"05.00.07\",\"supplierId\":\"1AB\"},{\"id\":156,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-2107099AD_1YB_CDU_ApplicationSoftware_05.01.03_H1.11_20230926.s19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.01.03\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"OBCe\",\"eolConfigCode\":\"01\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.11\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-2107010AD\",\"serialNumber\":\"\",\"softwareAss\":\"S11-2107999AD\",\"softwareVersion\":\"05.01.03\",\"supplierId\":\"1YB\"},{\"id\":157,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.10\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"TBOX1\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.11\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-7925010BC\",\"serialNumber\":\"\",\"softwareAss\":\"S11-7925999BC\",\"softwareVersion\":\"05.00.10\",\"supplierId\":\"9YD\"},{\"id\":158,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"S11-2108099AJ_9BY_PDCS_ApplicationSoftware_05.70.02_H2.40_20231116.s19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.70.02\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"S11-2108098AJ_9BY_PDCS_DriverSoftware_05.02.05_H2.40_20200928 .s19\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"05.02.05\",\"ecode\":\"\",\"ecuName\":\"PDCS\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8050570181E6C020000000000000000000000000000000000000000000000\",\"errorCode\":\"\",\"hardwareVersion\":\"H2.40\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-2108050AB\",\"serialNumber\":\"\",\"softwareAss\":\"S11-2108999AJ\",\"softwareVersion\":\"05.70.02\",\"supplierId\":\"9BY\"},{\"id\":159,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.02.29\",\"calMd5\":\"\",\"calFilePath\":\"\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"\",\"driverMd5\":\"\",\"driverFilePath\":\"\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"\",\"ecode\":\"\",\"ecuName\":\"WPC\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.11\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S30-3730011\",\"serialNumber\":\"\",\"softwareAss\":\"S30-3730999\",\"softwareVersion\":\"05.02.29\",\"supplierId\":\"9BB\"},{\"id\":160,\"moduleId\":5,\"appMd5\":\"\",\"appFilePath\":\"Hozon_EP12_internal_FLASH.s19\",\"appFileVersion\":\"\",\"appFileVersionLatest\":\"05.00.01\",\"calMd5\":\"\",\"calFilePath\":\"Hozon_EP12_Cal.s19\",\"calFileVersion\":\"\",\"calFileVersionLatest\":\"05.00.01\",\"driverMd5\":\"\",\"driverFilePath\":\"Flashdriver.s19\",\"driverFileVersion\":\"\",\"driverFileVersionLatest\":\"05.00.01\",\"ecode\":\"\",\"ecuName\":\"FLC1\",\"eolConfigCode\":\"010031001011A600096830AF14A91841F04989B811804FDFFFC4031420323330840300000000014004610204AE00000100040FE60009218042B8\",\"errorCode\":\"\",\"hardwareVersion\":\"H1.11\",\"iccidCode\":\"\",\"manufactureDate\":\"\",\"partNumber\":\"S11-7900041AB\",\"serialNumber\":\"\",\"softwareAss\":\"S11-3692999AC\",\"softwareVersion\":\"05.00.01\",\"supplierId\":\"NBAT\"}]}}";
            if (v_getInfo != null && v_getInfo.length() > 10)
                break;
        }
        // 获取返回的结果
        String result = v_getInfo;

        if (result == null) return false;
        if (result.length() < 20) return false;
        m_blueservice.PrintLog(result);
        //JSONObject obj = new JSONObject(result);

        //解析
        DomXmlService domxml = new DomXmlService();
        try {
            domxml.getDataFromEolJson(result);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //String vin = domxml.getvin();

        Common.cartype.ecus = domxml.getEcus();

        //分析
        //String v_partname = Common.cartype.getModuleStr("OBC", "partNumber");
        // String v_ivnname = Common.cartype.getModuleStr("OBC", "hardwareVersion");


        byte[] v_pin = new byte[2];
        String pinstr = domxml.getpin();
        if (pinstr != null)
            Commonfunc.StringToBytes(pinstr, v_pin, 2);
        String esk = domxml.getesk();
        if (esk != null)
            Commonfunc.StringToBytes(esk, Common.cartype.esk, 16);
        //EP12
        Common.cartype.pin[0] = v_pin[0];
        Common.cartype.pin[1] = v_pin[1];
        Common.cartype.pin[2] = (byte) 0xAA;
        Common.cartype.pin[3] = 0x55;
        Common.cartype.system = domxml.getmodelName();
        if (Common.cartype.system.equals("S12")) {
            Common.cartype.car = "EP12";
        }
        Common.cartype.name = domxml.getbodyColor();
        Commonfunc.StringToBytes(domxml.getesk(), Common.cartype.esk, 16);


        if (Common.cartype.ecus != null)
            return true;
        else
            return false;
        //获取胎压信息
    }

    public boolean GetVinInfoFromLocal(String Pvin) throws Exception {
        String v_xml = Pvin + "_ModelConfig.xml";
//		String v_xml = "LUZBGAFB0NA015782_ModelConfig.xml";
        String v_Mxml = Common.Dir + Common.GuestName + "/XML/" + v_xml;
        InputStream inputStream = new FileInputStream(new File(v_Mxml));
        //SAXParserFactory spf = SAXParserFactory.newInstance();
        //SAXParser saxParser = spf.newSAXParser(); // 创建解析器
        //XmlContentHandler handler = new XmlContentHandler();
        //saxParser.parse(inputStream, handler);
        DomXmlService domxml = new DomXmlService();
//		domxml.getPersons(inputStream);
        domxml.getPersonsNew(inputStream);
        //String vin = domxml.getvin();
        inputStream.close();
        Common.cartype.ecus = domxml.getEcus();
        //Commonfunc.StringToBytes(handler.getpin(),Common.cartype.pin,2);
        //String v_pin = handler.getpin();
        String pinstr = domxml.getpin();
        //新的PIN码算法
        byte[] v_pin = new byte[2];
        if (pinstr != null)
            Commonfunc.StringToBytes(pinstr, v_pin, 2);
        Common.cartype.pin[2] = v_pin[0];
        Common.cartype.pin[3] = v_pin[1];
        //旧的PIN码算法
        //System.arraycopy(pinstr.getBytes(), 0, Common.cartype.pin, 0, 4);

        //int len =  Common.cartype.pin.length;
        //String v_s = new String( Common.cartype.pin);
        Common.cartype.car = domxml.getmodelName();
        if (Common.cartype.car.equals("S12")) {
            Common.cartype.car = "EP12";
        }
        Common.cartype.name = domxml.getbodyColor();
        Commonfunc.StringToBytes(domxml.getesk(), Common.cartype.esk, 16);
        //tpms
        if (domxml.getlfId().length() == 8) {
            Commonfunc.StringToBytes(domxml.getlfId(), Common.cartype.flid, 4);
            Commonfunc.StringToBytes(domxml.getrfId(), Common.cartype.frid, 4);
            Commonfunc.StringToBytes(domxml.getlrId(), Common.cartype.blid, 4);
            Commonfunc.StringToBytes(domxml.getrrId(), Common.cartype.brid, 4);
        }
        //检查车型
//		if(Common.cartype.car.equals("S30") == false)
//			return false;

        if (Common.cartype.ecus != null)
            return true;
        else
            return false;
    }

    public class ListColorAdapter extends BaseAdapter {
        private ArrayList<String> listdata = null;
        private ListColor listlayout = null;
        private View view = null;
        private LayoutInflater inflater = null;
        private int Selected = -1;

        public ListColorAdapter(ArrayList<String> Pdata) {
            // TODO Auto-generated constructor stub
            listdata = Pdata;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (listdata != null)
                return listdata.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (listdata != null && listdata.size() > position)
                return listdata.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public void setSelected(int pos) {
            Selected = pos;
        }

        public int getSelected() {
            return Selected;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listcoloritem, null, false);
            listlayout = (ListColor) view.getTag();
            if (listlayout == null) {
                listlayout = new ListColor();
                listlayout.text = (TextView) view.findViewById(R.id.eolfunction_liststep_context);
                view.setTag(listlayout);
            }
            if (position == Selected) {
                //listlayout.text.setBackgroundColor(Color.GREEN);
                view.setBackgroundColor(Color.GREEN);
                listlayout.text.setSelected(true);
                listlayout.text.setPressed(true);
            } else if (position % 2 == 1) {
                listlayout.text.setSelected(false);
                listlayout.text.setPressed(false);
                view.setBackgroundColor(0xFFC0C0C0);
            } else {
                listlayout.text.setSelected(false);
                listlayout.text.setPressed(false);
                view.setBackgroundColor(Color.WHITE);
            }
            listlayout.text.setText(listdata.get(position));
            return view;
        }
    }

    public class ListColorMainAdapter extends BaseAdapter {
        private ArrayList<String> listdata = null;
        private ListColor listlayout = null;
        private View view = null;
        private LayoutInflater inflater = null;
        private int Selected = -1;

        public ListColorMainAdapter(ArrayList<String> Pdata) {
            // TODO Auto-generated constructor stub
            listdata = Pdata;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            if (listdata != null)
                return listdata.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            if (listdata != null && listdata.size() > position)
                return listdata.get(position);
            else
                return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public void setSelected(int pos) {
            Selected = pos;
        }

        public int getSelected() {
            return Selected;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listcoloritemmain, null, false);
            listlayout = (ListColor) view.getTag();
            if (listlayout == null) {
                listlayout = new ListColor();
                listlayout.text = (TextView) view.findViewById(R.id.eolfunction_liststep_main);
                view.setTag(listlayout);
            }
            if (position == Selected) {
                //listlayout.text.setBackgroundColor(Color.GREEN);
                view.setBackgroundColor(Color.GREEN);
                listlayout.text.setSelected(true);
                listlayout.text.setPressed(true);
            } else if (position % 2 == 1) {
                listlayout.text.setSelected(false);
                listlayout.text.setPressed(false);
                view.setBackgroundColor(0xFFC0C0C0);
            } else {
                listlayout.text.setSelected(false);
                listlayout.text.setPressed(false);
                view.setBackgroundColor(Color.WHITE);
            }
            listlayout.text.setText(listdata.get(position));
            return view;
        }
    }

    class ListColor {
        TextView text;
    }

    private void getAppVersion() {
        String web = "projectName=" + Common.ProjectName + "&"
                + "terminalSerial=" + Common.BoxName;
        String res = null;
        for (int i = 0; i < 3; i++) {
            res = HttpRequest.sendGet(Common.web_API_SelectUpdate, web); //调用查询版本接口
            System.out.println(res);
            if (res.indexOf("200") > 0) {
                break;
            }
        }
        if (!res.isEmpty() && res.indexOf("200") > 0) {
            try {
                JSONObject jsonObject = new JSONObject(res);
                JSONObject result = jsonObject.getJSONObject("data");
                //获取返回的版本以及路径以及版本信息
                Common.appVersion = result.getString("appVersion");
                String filePath = result.getString("filePath");
                Common.appNote = result.getString("appNote");
                String fileName = result.getString("fileName");
                Common.FileName = fileName;
                Common.resource = "resource=" + filePath;
                // todo 查询成功处理
                String version = getVersionName();
                float newVersion = Float.parseFloat(Common.appVersion.replace("V", "").replace(".", ""));
                float oldVersion = Float.parseFloat(version.replace("V", "").replace(".", ""));
                if (newVersion > oldVersion) {
                    // 软件版本存在更新
                    mhander.obtainMessage(com.src.xyzk_personal.config.Message.MSG_Updata_Version, 1, 0).sendToTarget();
                } else {
                    //已是最新版本
                    mhander.obtainMessage(com.src.xyzk_personal.config.Message.MSG_Updata_Version, 2, 0).sendToTarget();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //查询失败
            mhander.obtainMessage(com.src.xyzk_personal.config.Message.MSG_Updata_Version, 3, 0).sendToTarget();
        }
    }

    //获取软件版本号
    private String getVersionName() {
        String versionName = "";
        try {
            PackageInfo packageInfo = context.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("", e.getMessage());
        }
//        Ini inifile;
//        try {
//            String configfile = Common.Dir + Common.GuestName + "/mes.ini";
//            inifile = new Ini(new File(configfile));
//            Profile.Section sec = null;
//            //获取系统配置信息
//            sec = inifile.get("SETTING");
//            if (sec == null) return versionName;
//            versionName = sec.get("App_Version");
//
//        } catch (InvalidFileFormatException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (IOException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (NullPointerException e) {
//            m_blueservice.PrintLog(e.toString());
//        }
        return versionName;
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == PERMISSION_REQUEST_CODE) {
//            LoadConfig();
//        } else if (requestCode == PERMISSION_REQUEST_CODE2) {
//            Common.BoxName = getSN();
//        }
//    }
}

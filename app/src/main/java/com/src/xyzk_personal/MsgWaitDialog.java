package com.src.xyzk_personal;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.src.tsdl_personal.R;
import com.src.xyzk_personal.config.Common;

public class MsgWaitDialog {
	private final static String TAG = "MsgWaitDialog";
	private Dialog alertDialog;
	private ViewGroup alertViewGroup;
	private Activity myactivity = null;
	private int width = 0;
	private Button m_onok = null;
	private Button m_oncancel = null;
	private int m_wait_time = 0;	//等待时间
	private boolean IsShow = false;
	public MsgWaitDialog(Activity activity) {
		// TODO Auto-generated constructor stub
		this.myactivity = activity;
		WindowManager manager = activity.getWindowManager();
		Display display = manager.getDefaultDisplay();
		width = display.getWidth() - 30;
		LayoutInflater inflater = activity.getLayoutInflater();
		alertViewGroup = (ViewGroup) inflater.inflate(R.layout.msg_wait_dialog, null);
		//alertDialog = new Dialog.Builder(activity).create();
		//alertDialog = new AlertDialog.Builder(activity).create();
		alertDialog = new Dialog(activity);
		alertDialog.setTitle(R.string.msg_wait_title);
		if(Common.Debug) Log.i(TAG,"创建窗口!");
		if(alertDialog != null)
			alertDialog.show();
		//处理确定按钮
		m_onok = (Button)alertViewGroup.findViewById(R.id.msg_wait_but_ok);
		m_onok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SetAutoClose();
			}
		});
		m_oncancel = (Button)alertViewGroup.findViewById(R.id.msg_wait_but_cancel);
		m_oncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});
		//屏蔽返回
		alertDialog.setCancelable(false);
	}
	public void setOnKeyListener(DialogInterface.OnKeyListener keyListener)
	{
		alertDialog.setOnKeyListener(keyListener);
	}
	public MsgWaitDialog setPositiveButton(String buttonText,OnClickListener l){
		Button positiveButton = (Button)alertViewGroup.findViewById(R.id.msg_wait_but_ok);
		positiveButton.setOnClickListener(l);
		//positiveButton.setText(buttonText);
		//alertViewGroup.findViewById(R.id.input_vin_but_ok).setVisibility(View.VISIBLE);
		return this;
	}
	public MsgWaitDialog setNagitaveButton(String buttontext,OnClickListener l)
	{
		Button NagitaveButton = (Button)alertViewGroup.findViewById(R.id.msg_wait_but_cancel);
		NagitaveButton.setOnClickListener(l);
		return this;
	}
	//设置自动关闭
	public void SetAutoClose()
	{
		//禁用按钮
		m_onok.setClickable(false);
		//开始倒计时
		new Thread()
		{
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(int i = m_wait_time; i >= 0; i --)
				{
					if(Common.wait == false) break;
					mhandler.obtainMessage(MSG_WaitTime, i, 0).sendToTarget();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}//等待
				}
				mhandler.obtainMessage(MSG_exit, 0, 0).sendToTarget();
			}
		}.start();
	}

	public MsgWaitDialog setOnOKButton(OnClickListener l){
		//return setPositiveButton(activity.getResources().getString(resId),l);
		return setPositiveButton("",l);
	}
	public MsgWaitDialog setOnCancelButton(OnClickListener l){
		//return setPositiveButton(activity.getResources().getString(resId),l);
		return setNagitaveButton("",l);
	}
	public void cancel(){
		alertDialog.cancel();
		IsShow = false;
	}

	public void dismiss(){
		alertDialog.dismiss();
		IsShow = false;
	}
	public void show(){
		alertDialog.getWindow().setLayout(width, LayoutParams.WRAP_CONTENT);
		alertDialog.getWindow().setContentView(alertViewGroup);
		IsShow = true;
	}
	//set title
	public MsgWaitDialog setTitle(String title){
		//TextView titleView = (TextView)alertViewGroup.findViewById(R.id.msg_wait_title);
		//titleView.setText(title);
		alertDialog.setTitle(title);
		return this;
	}

	public MsgWaitDialog setTitle(int resId){
		return setTitle(myactivity.getResources().getString(resId));
	}
	public void setWaitTime(int itime)
	{
		m_wait_time = itime;
	}
	public boolean IsShowing()
	{
		return IsShow;
	}
	public void setText(String str)
	{
		TextView textView = (TextView)alertViewGroup.findViewById(R.id.msg_wait_context);
		textView.setText(str);
	}
	public void setText(int resid)
	{
		TextView textView = (TextView)alertViewGroup.findViewById(R.id.msg_wait_context);
		textView.setText(myactivity.getResources().getString(resid) );
	}
	public void ShowCancel()
	{
		m_oncancel.setVisibility(View.VISIBLE);
	}

	public void ShowYesOrNo()
	{
		m_oncancel.setText(myactivity.getString(R.string.but_no));
		m_onok.setText(myactivity.getString(R.string.but_yes));
	}
	private final static int MSG_WaitTime = 101;	//弹出等待提示对话框
	private final static int MSG_exit	= 102;		//退出
	private final Handler mhandler = new Handler()
	{
		public void handleMessage(android.os.Message msg)
		{
			switch (msg.what) {
				case MSG_WaitTime:
					m_onok.setText(myactivity.getResources().getString(R.string.msg_wait_dialog_text1) + msg.arg1 +
							myactivity.getResources().getString(R.string.msg_wait_dialog_text2));
					break;
				case MSG_exit:
					Common.wait = false;
					IsShow = false;
					dismiss();
					break;
				default:
					break;
			}
		};
	};
}

package com.src.xyzk_personal;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.src.tsdl_personal.R;
import com.src.xyzk_personal.config.Common;

public class InputVinDialog {
	private final static String TAG = "InputVinDialog";
	private Dialog alertDialog;
	private ViewGroup alertViewGroup;
	private Activity activity = null;
	private int width = 0;
	private Button m_onok = null;
	private Button m_onclose = null;
	public InputVinDialog(Activity activity) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		WindowManager manager = activity.getWindowManager();
		Display display = manager.getDefaultDisplay();
		width = display.getWidth() - 30;
		LayoutInflater inflater = activity.getLayoutInflater();
		alertViewGroup = (ViewGroup) inflater.inflate(R.layout.input_vin_dialog, null);
		//alertDialog = new Dialog.Builder(activity).create();
		alertDialog = new Dialog(activity);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.show();
		if(Common.Debug) Log.i(TAG,"创建窗口!");
		//处理确定按钮
		m_onok = (Button)alertViewGroup.findViewById(R.id.input_vin_but_ok);
		m_onok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		//close
		m_onclose = (Button)alertViewGroup.findViewById(R.id.input_vin_close);
		m_onclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});
		//屏蔽返回
		alertDialog.setCancelable(false);
	}
	public void setOnKeyListener(DialogInterface.OnKeyListener keyListener)
	{
		alertDialog.setOnKeyListener(keyListener);
	}
	public InputVinDialog setPositiveButton(String buttonText,OnClickListener l){
		Button positiveButton = (Button)alertViewGroup.findViewById(R.id.input_vin_but_ok);
		positiveButton.setOnClickListener(l);
		//positiveButton.setText(buttonText);
		//alertViewGroup.findViewById(R.id.input_vin_but_ok).setVisibility(View.VISIBLE);
		return this;
	}

	public InputVinDialog setOnOKButton(OnClickListener l){
		//return setPositiveButton(activity.getResources().getString(resId),l);
		return setPositiveButton("",l);
	}
	public void cancel(){
		alertDialog.cancel();
	}

	public void dismiss(){
		alertDialog.dismiss();
	}
	public void show(){
		alertDialog.getWindow().setLayout(width, LayoutParams.WRAP_CONTENT);
		alertDialog.getWindow().setContentView(alertViewGroup);
	}
	//set title
	public InputVinDialog setTitle(String title){
		TextView titleView = (TextView)alertViewGroup.findViewById(R.id.input_vin_title);
		titleView.setText(title);
		return this;
	}

	public InputVinDialog setTitle(int resId){
		return setTitle(activity.getResources().getString(resId));
	}
	public String GetEditString()
	{
		EditText edit = (EditText)alertViewGroup.findViewById(R.id.input_vin_edit);
		return edit.getText().toString();
	}
	public void setEditString(String data)
	{
		EditText edit = (EditText)alertViewGroup.findViewById(R.id.input_vin_edit);
		edit.setText(data);
	}
}

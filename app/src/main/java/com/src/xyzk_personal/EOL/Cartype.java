package com.src.xyzk_personal.EOL;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;

import com.src.xyzk.bluetooth.service.BluetoothAdapterService;
import com.src.xyzk_personal.config.Common;
import com.src.xyzk_personal.config.Commonfunc;
import com.src.xyzk_personal.config.Ecu;
import com.src.xyzk_personal.config.UrlUtil;

import android.R.string;
import android.util.Log;

public class Cartype {
	private static String TAG = "Cartype";
	BluetoothAdapterService m_ble = BluetoothAdapterService.getInstance();
	//其他变量
	public List<Ecu> ecus = null;
	public byte [] vin = null;	//vin码
	public byte [] pin = null;	//pin码
	public byte [] esk = null;	//ESK码
	public byte [] time = null; //配置时间
	public byte [] flid = null;
	public byte [] frid = null;
	public byte [] blid = null;
	public byte [] brid = null;
	public int station = -1;	//工位
	public String car = null;	//车型名称
	public Ini idtc = null;	//dtc路径
	public Ini ilang = null;	//语言文件路径
	public Ini ican = null;		//can设置
	public Ini istep = null;
	public int OptMode = 0;	//返修模式是否启用
	public String name = null;		//车辆颜色
	public String system = null;	//车型代码
	HashMap<String, Integer> map = null; //系统模块配置
	HashMap<String, String> map_ver = null; //版本信息
	HashMap<String, byte[]> map_config = null;	//写入数据
	public String box_serialnum = null;	//box序列号
	public String box_version = null;	//box版本号
	public String start_time = null;	//起始时间
	public String end_time = null;		//结束时间
	public int Result = 0;		//检测结果
	public Cartype() {
		// TODO Auto-generated constructor stub
		if(Common.Debug) Log.i(TAG,"cartype初始化");
		vin = new byte[17];
		pin = new byte[4];
		esk = new byte[16];
		time = new byte[4];
		flid = new byte[4];
		frid = new byte[4];
		blid = new byte[4];
		brid = new byte[4];
		map = new HashMap<String, Integer>();
		map_ver = new HashMap<String, String>();
		map_config = new HashMap<String, byte[]>();
	}
	public void clearall()
	{
		if(vin == null) return;
		Result = 0;
		map.clear();
		map_ver.clear();
		map_config.clear();
		//Arrays.fill(vin, (byte) 0);
		Arrays.fill(pin, (byte) 0);
		Arrays.fill(esk, (byte) 0);
		Arrays.fill(time, (byte) 0);
		Arrays.fill(flid, (byte) 0);
		Arrays.fill(frid, (byte) 0);
		Arrays.fill(blid, (byte) 0);
		Arrays.fill(brid, (byte) 0);
		if(Common.Debug) Log.i(TAG,"clear all data is ok!");
	}

	public void SetCartype(int Pstation) {
		// TODO Auto-generated constructor stub
		//先清除
		station = Pstation;
		if(station == 4)
			OptMode = 1; //启用返修模式
		else
			OptMode = 0; //正常模式
		//clearall();
		try {
			istep = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "STEP_" + station + ".ini"));
			ican = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "can.ini"));
			if(Common.Lang.equals("en"))
			{
				idtc = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "DTC_EN.ini"));
				ilang = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "LANG_EN.ini"));
			}
			else
			{
				idtc = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "DTC_CN.ini"));
				ilang = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "LANG_CN.ini"));
			}

			//时间
			//time
			SimpleDateFormat tempDate = new SimpleDateFormat("yyyyMMdd");
			Date v_datetime = new java.util.Date();
			this.start_time = tempDate.format(v_datetime).toString();
			Commonfunc.StringToBytes(this.start_time, this.time, 4);
			//这里区分车型
			//load module
			Profile.Section sec = null;
			for(Ecu ecu : ecus){
				//module select
				putMap(ecu.getecuName(), 1);
				//load can id
				sec = ican.get(ecu.getecuName());
				if(sec == null)
				{
					continue;
				}
				String v_send = sec.get("ID_SEND");
				if(v_send != null && v_send.length() > 0)
					ecu.setreqid(Commonfunc.HexStringtoInt(v_send));
				String v_recv = sec.get("ID_RECV");
				if(v_recv != null && v_recv.length() > 0)
					ecu.setresid(Commonfunc.HexStringtoInt(v_recv));
			}

			if(car.equals("S30"))
			{
			}
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			m_ble.PrintLog(e1.toString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			m_ble.PrintLog(e1.toString());
		}
	}
	public String SetRepairIstep(String Pfunction) //返修模式设置文件,返回函数
	{
		String funcname = "";
		if(OptMode != 1) return funcname;
		try {
			if(Pfunction.indexOf("下线检测") > 0 || Pfunction.indexOf("Detection") > 0)  //
				station = 0;
			else if(Pfunction.indexOf("刷写") > 0 || Pfunction.indexOf("Refresh") > 0)
				station = 1;
			else if(Pfunction.indexOf("定位检测") > 0 || Pfunction.indexOf("arcate") > 0)
				station = 2;
			else if(Pfunction.indexOf("出厂检测") > 0 || Pfunction.indexOf("inspection") > 0)
				station = 3;
			else
				return funcname;
			//解析名字
			int adr = Pfunction.indexOf("->");
			funcname = Pfunction.substring(adr + 2);
			istep = new Ini(new File(Common.Dir + Common.GuestName + "/" + car + "/" + "STEP_" + station + ".ini"));

		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return funcname;
	}
	public void setVin(String Pvin)
	{
		vin = Pvin.getBytes();
	}
	public void setTime(byte[] Ptime)
	{
		System.arraycopy(Ptime, 0, time, 0, 4);
	}
	public void setPin(byte[] Ppin)
	{
		pin = Ppin;
	}
	public int getMap(String Pdata)
	{
		if(map.get(Pdata) == null)
			return 0;
		else
			return map.get(Pdata);
	}
	public void putMap(String str,int val)
	{
		map.put(str, val);
	}
	public String getMapStr(String Pdata)
	{
		return map_ver.get(Pdata);
	}
	public void putMapStr(String str,String value)
	{
		map_ver.put(str, value);
	}
	public void putMapbyte(String str,byte [] value)
	{
		map_config.put(str, value);
	}
	public byte[] getMapByte(String str)
	{
		return map_config.get(str);
	}
	public int getModuleIdReq(String Pname)
	{
		Pname = Pname.substring(0, Pname.length());
		for(Ecu ecu : ecus){
			if(ecu.getecuName().equals(Pname))
				return ecu.getreqid();
		}
		return 0;
	}
	public int getModuleIdRes(String Pname)
	{
		Pname = Pname.substring(0, Pname.length());
		for(Ecu ecu : ecus){
			if(ecu.getecuName().equals(Pname))
				return ecu.getresid();
		}
		return 0;
	}
	public int getModuleNum(String Pname)
	{
		int ecu_num = 0;
		if(ecus.size() <= 0) return 0;
		for(Ecu ecu : ecus){
			if(ecu.getecuName().equals(Pname))
			{
				ecu_num ++;
			}
		}
		return ecu_num;
	}
	public String getModuleStr(String Pname,String Pvalue)
	{
		//Pname = Pname.substring(0, Pname.length());  
		String v_data = "";
		if(ecus.size() <= 0) return v_data;
		for(Ecu ecu : ecus){
			if(ecu.getecuName().equals(Pname))
			{
				if(Pvalue.equals("manufactureDate"))
					v_data = ecu.getmanufactureDate();
				else if(Pvalue.equals("supplierId"))
					v_data = ecu.getsupplierId();
				else if(Pvalue.equals("partNumber"))
					v_data = ecu.getpartNumber();
				else if(Pvalue.equals("softwareAss"))
					v_data = ecu.getsoftwareAss();
				else if(Pvalue.equals("hardwareVersion"))
					v_data = ecu.gethardwareVersion();
				else if(Pvalue.equals("softwareVersion"))
					v_data = ecu.getsoftwareVersion();
				else if(Pvalue.equals("eolConfigCode"))
					v_data = ecu.geteolConfigCode();
				else if(Pvalue.equals("ecode"))
					v_data = ecu.getecode();
				else if(Pvalue.equals("serialNumber"))
					v_data = ecu.getserialNumber();
				else if(Pvalue.equals("driverFileVersion"))
					v_data = ecu.getdriverFileVersion();
				else if(Pvalue.equals("driverFileVersionLatest"))
					v_data = ecu.getdriverFileVersionLatest();
				else if(Pvalue.equals("driverFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getdriverFileName());
				else if(Pvalue.equals("driverServerPath"))
					v_data = ecu.getdriverServerPath();
				else if(Pvalue.equals("appFileVersion"))
					v_data = ecu.getappFileVersion();
				else if(Pvalue.equals("appFileVersionLatest"))
					v_data = ecu.getappFileVersionLatest();
				else if(Pvalue.equals("appFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getappFileName());
				else if(Pvalue.equals("appServerPath"))
					v_data = ecu.getappServerPath();
				else if(Pvalue.equals("calFileVersion"))
					v_data = ecu.getcalFileVersion();
				else if(Pvalue.equals("calFileVersionLatest"))
					v_data = ecu.getcalFileVersionLatest();
				else if(Pvalue.equals("calFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getcalFileName());
				else if(Pvalue.equals("calServerPath"))
					v_data = ecu.getcalServerPath();
				else if(Pvalue.equals("iccid_code"))
					v_data = ecu.geticcid();
				break;
			}
		}
		return v_data;
	}
	public List<String> getModuleStrALL(String Pname,String Pvalue)
	{
		//Pname = Pname.substring(0, Pname.length());
		List<String> v_datas=new ArrayList<>();
		String v_data = "";
		if(ecus.size() <= 0) return v_datas;
		for(Ecu ecu : ecus){
			if(ecu.getecuName().equals(Pname))
			{
				if(Pvalue.equals("manufactureDate"))
					v_data = ecu.getmanufactureDate();
				else if(Pvalue.equals("supplierId"))
					v_data = ecu.getsupplierId();
				else if(Pvalue.equals("partNumber"))
					v_data = ecu.getpartNumber();
				else if(Pvalue.equals("softwareAss"))
					v_data = ecu.getsoftwareAss();
				else if(Pvalue.equals("hardwareVersion"))
					v_data = ecu.gethardwareVersion();
				else if(Pvalue.equals("softwareVersion"))
					v_data = ecu.getsoftwareVersion();
				else if(Pvalue.equals("eolConfigCode"))
					v_data = ecu.geteolConfigCode();
				else if(Pvalue.equals("ecode"))
					v_data = ecu.getecode();
				else if(Pvalue.equals("serialNumber"))
					v_data = ecu.getserialNumber();
				else if(Pvalue.equals("driverFileVersion"))
					v_data = ecu.getdriverFileVersion();
				else if(Pvalue.equals("driverFileVersionLatest"))
					v_data = ecu.getdriverFileVersionLatest();
				else if(Pvalue.equals("driverFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getdriverFileName());
				else if(Pvalue.equals("driverServerPath"))
					v_data = ecu.getdriverServerPath();
				else if(Pvalue.equals("appFileVersion"))
					v_data = ecu.getappFileVersion();
				else if(Pvalue.equals("appFileVersionLatest"))
					v_data = ecu.getappFileVersionLatest();
				else if(Pvalue.equals("appFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getappFileName());
				else if(Pvalue.equals("appServerPath"))
					v_data = ecu.getappServerPath();
				else if(Pvalue.equals("calFileVersion"))
					v_data = ecu.getcalFileVersion();
				else if(Pvalue.equals("calFileVersionLatest"))
					v_data = ecu.getcalFileVersionLatest();
				else if(Pvalue.equals("calFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getcalFileName());
				else if(Pvalue.equals("calServerPath"))
					v_data = ecu.getcalServerPath();
				else if(Pvalue.equals("iccid_code"))
					v_data = ecu.geticcid();

				v_datas.add(v_data);
			}
		}
		return v_datas;
	}
	public String getModuleStr(String Pname,String Partnumber,String Pvalue)
	{
		//Pname = Pname.substring(0, Pname.length());  
		String v_data = "";
		if(ecus.size() <= 0) return v_data;
		for(Ecu ecu : ecus)
		{
			if(ecu.getecuName().equals(Pname))
			{
				if(Partnumber.length() > 0)
				{
					if((ecu.getpartNumber().equals(Partnumber)||ecu.getsoftwareAss().equals(Partnumber)||ecu.gethardwareVersion().equals(Partnumber)) == false) continue;
				}
				if(Pvalue.equals("manufactureDate"))
					v_data = ecu.getmanufactureDate();
				else if(Pvalue.equals("supplierId"))
					v_data = ecu.getsupplierId();
				else if(Pvalue.equals("partNumber"))
					v_data = ecu.getpartNumber();
				else if(Pvalue.equals("softwareAss"))
					v_data = ecu.getsoftwareAss();
				else if(Pvalue.equals("hardwareVersion"))
					v_data = ecu.gethardwareVersion();
				else if(Pvalue.equals("softwareVersion"))
					v_data = ecu.getsoftwareVersion();
				else if(Pvalue.equals("eolConfigCode"))
					v_data = ecu.geteolConfigCode();
				else if(Pvalue.equals("ecode"))
					v_data = ecu.getecode();
				else if(Pvalue.equals("serialNumber"))
					v_data = ecu.getserialNumber();
				else if(Pvalue.equals("driverFileVersion"))
					v_data = ecu.getdriverFileVersion();
				else if(Pvalue.equals("driverFileVersionLatest"))
					v_data = ecu.getdriverFileVersionLatest();
				else if(Pvalue.equals("driverFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getdriverFileName());
				else if(Pvalue.equals("driverServerPath"))
					v_data = ecu.getdriverServerPath();
				else if(Pvalue.equals("appFileVersion"))
					v_data = ecu.getappFileVersion();
				else if(Pvalue.equals("appFileVersionLatest"))
					v_data = ecu.getappFileVersionLatest();
				else if(Pvalue.equals("appFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getappFileName());
				else if(Pvalue.equals("appServerPath"))
					v_data = ecu.getappServerPath();
				else if(Pvalue.equals("calFileVersion"))
					v_data = ecu.getcalFileVersion();
				else if(Pvalue.equals("calFileVersionLatest"))
					v_data = ecu.getcalFileVersionLatest();
				else if(Pvalue.equals("calFileName"))
					v_data = UrlUtil.getURLDecodeHozon(ecu.getcalFileName());
				else if(Pvalue.equals("calServerPath"))
					v_data = ecu.getcalServerPath();
				else if(Pvalue.equals("iccid_code"))
					v_data = ecu.geticcid();
			}
		}
		return v_data;
	}
}

package com.src.xyzk_personal.Flash;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.src.tsdl_personal.R;
import com.src.xyzk_personal.config.Common;

import java.util.ArrayList;

public class SelectPartNumberDialog {
	private final static String TAG = "SelectPartNumberDialog";
	private Dialog alertDialog;
	private ViewGroup alertViewGroup;
	private Activity activity = null;
	private int width = 0;
	private Button m_onok = null;
	private Button m_onclose = null;

	private int m_current = 0;
	private Spinner m_spinner = null;
	private ArrayList<String> m_spinnerarray = null;
	private ArrayAdapter<String> m_spinneradapter = null;

	public SelectPartNumberDialog(Activity activity) {
		this.activity = activity;
		WindowManager manager = activity.getWindowManager();
		Display display = manager.getDefaultDisplay();
		width = display.getWidth() - 30;
		LayoutInflater inflater = activity.getLayoutInflater();
		alertViewGroup = (ViewGroup) inflater.inflate(R.layout.select_part_dialog, null);
		//alertDialog = new Dialog.Builder(activity).create();
		alertDialog = new Dialog(activity);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.show();
		if(Common.Debug) Log.i(TAG,"创建窗口!");
		//处理确定按钮
		m_onok = (Button)alertViewGroup.findViewById(R.id.select_part_but_ok);
		m_onok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		//close
		m_onclose = (Button)alertViewGroup.findViewById(R.id.select_part_close);
		m_onclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});
		m_spinner = (Spinner)alertViewGroup.findViewById(R.id.select_part_spinner);
		m_spinnerarray = new ArrayList<String>();
		//先添加列表

		m_spinneradapter = new ArrayAdapter<String>(this.activity, R.layout.my_spinner_select_item,m_spinnerarray);
		//设置下拉风格
		m_spinneradapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
		m_spinner.setAdapter(m_spinneradapter);
		m_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				m_current = position;
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		//屏蔽返回
		alertDialog.setCancelable(false);
	}
	public void setOnKeyListener(DialogInterface.OnKeyListener keyListener)
	{
		alertDialog.setOnKeyListener(keyListener);
	}
	public SelectPartNumberDialog setPositiveButton(String buttonText, OnClickListener l){
		Button positiveButton = (Button)alertViewGroup.findViewById(R.id.select_part_but_ok);
		positiveButton.setOnClickListener(l);
		//positiveButton.setText(buttonText);
		//alertViewGroup.findViewById(R.id.input_vin_but_ok).setVisibility(View.VISIBLE);
		return this;
	}

	public SelectPartNumberDialog setOnOKButton(OnClickListener l){
		//return setPositiveButton(activity.getResources().getString(resId),l);
		return setPositiveButton("",l);
	}
	public void cancel(){
		alertDialog.cancel();
	}

	public void dismiss(){
		alertDialog.dismiss();
	}
	public void show(){
		alertDialog.getWindow().setLayout(width, LayoutParams.WRAP_CONTENT);
		alertDialog.getWindow().setContentView(alertViewGroup);
	}
	//set title
	public SelectPartNumberDialog setTitle(String title){
		TextView titleView = (TextView)alertViewGroup.findViewById(R.id.select_part_title);
		titleView.setText(title);
		return this;
	}

	public SelectPartNumberDialog setTitle(int resId){
		return setTitle(activity.getResources().getString(resId));
	}
	public String GetSelectString()
	{
		return m_spinnerarray.get(m_current);
	}
	public int GetSelected()
	{
		return m_current;
	}
	public void AddSelectString(String Pitem)
	{
		m_spinnerarray.add(Pitem);
		m_spinneradapter.notifyDataSetChanged();
	}
	public void SetDefaultSelect(int Pselect)
	{
		if(Pselect < m_spinnerarray.size())
		{
			m_spinner.setSelection(Pselect, true);
			m_spinneradapter.notifyDataSetChanged();
		}
	}
}

package com.src.xyzk_personal.Service;

import android.app.Application;



public class TApplication extends Application {
	private static TApplication app;

	@Override
	public void onCreate() {
		super.onCreate();
		app = this;
		CrashHandler.init();
	}

	/**
	 * 获取上下文
	 * @return
	 */
	public static TApplication getContext(){
		return app;
	}
}

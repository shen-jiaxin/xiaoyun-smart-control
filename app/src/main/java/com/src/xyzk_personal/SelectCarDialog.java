package com.src.xyzk_personal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile;
import org.ini4j.Profile.Section;

import com.src.tsdl_personal.R;
import com.src.xyzk_personal.config.Common;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class SelectCarDialog {
	private final static String TAG = "SelectPartNumberDialog";
	private Dialog alertDialog;
	private ViewGroup alertViewGroup;
	private Activity activity = null;
	private int width = 0;
	private Button m_onok = null;
	private Button m_onclose = null;

	//选择
	private int m_select_car = 0;
	private int m_select_eng = 0;
	private int m_select_config = 0;
	private int m_select_color = 0;
	private Spinner m_spinner = null;
	private ArrayList<String> m_spinnerarray = null;
	private ArrayAdapter<String> m_spinneradapter = null;
	//engine
	private Spinner m_eng_spinner = null;
	private ArrayList<String> m_eng_spinnerarray = null;
	private ArrayAdapter<String> m_eng_spinneradapter = null;
	//config
	private Spinner m_config_spinner = null;
	private ArrayList<String> m_config_spinnerarray = null;
	private ArrayAdapter<String> m_config_spinneradapter = null;
	//color
	private Spinner m_color_spinner = null;
	private ArrayList<String> m_color_spinnerarray = null;
	private ArrayAdapter<String> m_color_spinneradapter = null;

	public SelectCarDialog(Activity activity) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		WindowManager manager = activity.getWindowManager();
		Display display = manager.getDefaultDisplay();
		width = display.getWidth() - 30;
		LayoutInflater inflater = activity.getLayoutInflater();
		alertViewGroup = (ViewGroup) inflater.inflate(R.layout.select_car_dialog, null);
		//alertDialog = new Dialog.Builder(activity).create();
		alertDialog = new Dialog(activity);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.show();
		if(Common.Debug) Log.i(TAG,"创建窗口!");
		//处理确定按钮
		m_onok = (Button)alertViewGroup.findViewById(R.id.select_car_but_ok);
		m_onok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		//close
		m_onclose = (Button)alertViewGroup.findViewById(R.id.select_car_close);
		m_onclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
			}
		});
		//先添加列表
		m_spinner = (Spinner)alertViewGroup.findViewById(R.id.select_car_spinner);
		m_spinnerarray = new ArrayList<String>();
		m_spinneradapter = new ArrayAdapter<String>(this.activity, R.layout.my_spinner_select_item,m_spinnerarray);
		//设置下拉风格
		m_spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_spinner.setAdapter(m_spinneradapter);
		m_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				if(m_select_car != position)
				{
					m_select_car = position;
					LoadDefaultSelect(m_select_car,0,0,0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		//engine list
		m_eng_spinner = (Spinner)alertViewGroup.findViewById(R.id.select_engine_spinner);
		m_eng_spinnerarray = new ArrayList<String>();
		m_eng_spinneradapter = new ArrayAdapter<String>(this.activity, R.layout.my_spinner_select_item,m_eng_spinnerarray);
		//设置下拉风格
		m_eng_spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_eng_spinner.setAdapter(m_eng_spinneradapter);
		m_eng_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				if(m_select_eng != position)
				{
					m_select_eng = position;
					LoadConfig(position);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		//config
		m_config_spinner = (Spinner)alertViewGroup.findViewById(R.id.select_config_spinner);
		m_config_spinnerarray = new ArrayList<String>();
		m_config_spinneradapter = new ArrayAdapter<String>(this.activity, R.layout.my_spinner_select_item,m_config_spinnerarray);
		//设置下拉风格
		m_config_spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_config_spinner.setAdapter(m_config_spinneradapter);
		m_config_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				if(m_select_config != position)
				{
					m_select_config = position;
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		//color
		m_color_spinner = (Spinner)alertViewGroup.findViewById(R.id.select_color_spinner);
		m_color_spinnerarray = new ArrayList<String>();
		m_color_spinneradapter = new ArrayAdapter<String>(this.activity, R.layout.my_spinner_select_item,m_color_spinnerarray);
		//设置下拉风格
		m_color_spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_color_spinner.setAdapter(m_color_spinneradapter);
		m_color_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				if(m_select_color != position)
				{
					m_select_color = position;
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		//屏蔽返回
		alertDialog.setCancelable(false);
	}
	public void setOnKeyListener(DialogInterface.OnKeyListener keyListener)
	{
		alertDialog.setOnKeyListener(keyListener);
	}
	public SelectCarDialog setPositiveButton(String buttonText,OnClickListener l){
		Button positiveButton = (Button)alertViewGroup.findViewById(R.id.select_car_but_ok);
		positiveButton.setOnClickListener(l);
		//positiveButton.setText(buttonText);
		//alertViewGroup.findViewById(R.id.input_vin_but_ok).setVisibility(View.VISIBLE);
		return this;
	}

	public SelectCarDialog setOnOKButton(OnClickListener l){
		//return setPositiveButton(activity.getResources().getString(resId),l);
		return setPositiveButton("",l);
	}
	public void cancel(){
		alertDialog.cancel();
	}

	public void dismiss(){
		alertDialog.dismiss();
	}
	public void show(){
		alertDialog.getWindow().setLayout(width, LayoutParams.WRAP_CONTENT);
		alertDialog.getWindow().setContentView(alertViewGroup);
	}
	//set title
	public SelectCarDialog setTitle(String title){
		TextView titleView = (TextView)alertViewGroup.findViewById(R.id.select_car_title);
		titleView.setText(title);
		return this;
	}

	public SelectCarDialog setTitle(int resId){
		return setTitle(activity.getResources().getString(resId));
	}
	public void AddSelectString(String Pitem)
	{
		m_spinnerarray.add(Pitem);
		m_spinneradapter.notifyDataSetChanged();
	}
	public void LoadDefaultSelect(int Pcar,int Peng,int Pconfig,int Pcolor)
	{
		//设置CAR
		if(Pcar >= 0)
			m_select_car = Pcar;
		if(Peng >= 0)
			m_select_eng = Peng;
		if(Pconfig >= 0)
			m_select_config = Pconfig;
		m_spinner.setSelection(Pcar);
		//获取eng list
		String v_car = m_spinnerarray.get(Pcar);
		if(m_eng_spinnerarray.size() > 0)
			m_eng_spinnerarray.clear();
		///////////TEST//////////////
/*		String t_carfile = Common.Dir + Common.GuestName + "/CAR.ini";
		Ini tinifile;
		try {
			tinifile = new Ini(new File(t_carfile));
			//Profile.Section sec = inifile.get(v_modename);

			//String v_tit;
			String v_item;
			Set<Entry<String, Section>> set = tinifile.entrySet();
            for (Entry<String, Section> entry : set)
            {
                String sectionName = entry.getKey();
                if(sectionName.contains(v_car))
				{
					v_item = sectionName.substring(v_car.length() + 1);
					m_eng_spinnerarray.add(v_item);
				}
            }
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
*/

		//config
		if(m_config_spinnerarray.size() > 0)
			m_config_spinnerarray.clear();

		if(v_car.equals("VIN"))
		{
			//m_eng_spinner.setSelection(Peng);

			addVinConfiglist("MODE");
		}
		else
		{

			String v_carfile = Common.Dir + Common.GuestName + "/CAR.ini";
			Ini inifile;
			try {
				inifile = new Ini(new File(v_carfile));
				//String v_tit;
/*				String v_item;
				Set<Entry<String, Section>> set = inifile.entrySet();
	            for (Entry<String, Section> entry : set)
	            {
	                String sectionName = entry.getKey();
	                if(sectionName.contains(v_car))
					{
						v_item = sectionName.substring(v_car.length() + 1);
						m_eng_spinnerarray.add(v_item);
					}
	            }
	            m_eng_spinner.setSelection(Peng);
*/
				//config
				String v_modename = v_car + "_" + m_eng_spinnerarray.get(Peng);
				Profile.Section sec = inifile.get(v_modename);
				if(sec == null) return;
				//数量
				int num = Integer.valueOf(sec.get("NUM"));
				String id = null;
				String v_name = null;
				for(int i = 0; i < num; i ++)
				{
					id = "T" + i;
					v_name = sec.get(id);
					m_config_spinnerarray.add(v_name);
				}
			} catch (InvalidFileFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		m_eng_spinneradapter.notifyDataSetChanged();
		//Sleep(20);
		m_config_spinner.setSelection(Pconfig);
		m_config_spinneradapter.notifyDataSetChanged();

		m_color_spinner.setVisibility(View.GONE);
	}
	private void Sleep(int time)
	{
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}
	private void addVinlist(String Pcar)
	{
		String v_carfile = Common.Dir + Common.GuestName + "/CAR.ini";
		Ini inifile;
		try {
			inifile = new Ini(new File(v_carfile));
			Profile.Section sec = inifile.get(Pcar);
			if(sec == null) return;
			//数量
			int num = Integer.valueOf(sec.get("NUM"));
			String id = null;
			String v_name = null;
			for(int i = 0; i < num; i ++)
			{
				id = "T" + i;
				v_name = sec.get(id);
				m_eng_spinnerarray.add(v_name);
			}
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	private void addVinConfiglist(String Pmode)
	{
		String v_carfile = Common.Dir + Common.GuestName + "/VIN/cartype.ini";
		Ini inifile;
		try {
			inifile = new Ini(new File(v_carfile));
			Profile.Section sec = inifile.get(Pmode);
			if(sec == null) return;
			//数量
			int num = Integer.valueOf(sec.get("NUM"));
			String id = null;
			String v_name = null;
			for(int i = 1; i <= num; i ++)
			{
				id = "T" + i;
				v_name = sec.get(id);
				m_config_spinnerarray.add(v_name);
			}
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	private void LoadConfig(int Peng)
	{
		//config
		if(m_config_spinnerarray.size() > 0)
			m_config_spinnerarray.clear();
		String v_modename = m_spinnerarray.get(m_select_car) + "_" + m_eng_spinnerarray.get(Peng);
		String v_carfile = Common.Dir + Common.GuestName + "/CAR.ini";
		Ini inifile;
		try {
			inifile = new Ini(new File(v_carfile));
			Profile.Section sec = inifile.get(v_modename);
			if(sec == null) return;
			//数量
			int num = Integer.valueOf(sec.get("NUM"));
			String id = null;
			String v_name = null;
			for(int i = 0; i < num; i ++)
			{
				id = "T" + i;
				v_name = sec.get(id);
				m_config_spinnerarray.add(v_name);
			}
		} catch (InvalidFileFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		m_config_spinner.setSelection(0);
		m_config_spinneradapter.notifyDataSetChanged();
	}
	public int GetCarSelect()
	{
		return m_select_car;
	}
	public int GetEngSelect()
	{
		return m_select_eng;
	}
	public int GetConfigSelect()
	{
		return m_select_config;
	}
	public int GetColorSelect()
	{
		return m_select_color;
	}
	public String GetCarString()
	{
		return m_spinnerarray.get(m_select_car);
	}
	public String GetEngString()
	{
		return m_eng_spinnerarray.get(m_select_eng);
	}
	public String GetConfigString()
	{
		return m_config_spinnerarray.get(m_select_config);
	}
	public String GetSelectString()
	{
		String v_data;
		if(GetCarString().equals("S402"))
			v_data = m_spinnerarray.get(m_select_car) + "_" + m_eng_spinnerarray.get(m_select_eng) + m_config_spinnerarray.get(m_select_config) + "(" + m_color_spinnerarray.get(m_select_color) + ")";
		else
			v_data = m_spinnerarray.get(m_select_car) + "_" + m_eng_spinnerarray.get(m_select_eng) + m_config_spinnerarray.get(m_select_config);
		return v_data;
	}
}
